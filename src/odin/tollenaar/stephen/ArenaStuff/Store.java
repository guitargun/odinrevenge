package odin.tollenaar.stephen.ArenaStuff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import odin.tollenaar.stephen.Abilities.Ability;
import odin.tollenaar.stephen.Items.Actives;
import odin.tollenaar.stephen.Items.Area;
import odin.tollenaar.stephen.Items.Armor;
import odin.tollenaar.stephen.Items.Critters;
import odin.tollenaar.stephen.Items.Potions;
import odin.tollenaar.stephen.Items.Stackers;
import odin.tollenaar.stephen.Items.Weapons;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Store {
	private HashMap<UUID, String> playertopopeninv = new HashMap<UUID, String>();

	private OdinMain plugin;
	private Arena arena;

	public Store(OdinMain instance, Arena arena) {
		this.plugin = instance;
		this.arena = arena;
		plugin.items.LoadAll();
	}

	public void storemain(Player player) {
		Inventory main = Bukkit.createInventory(null, 9, "Store");
		ItemStack armors = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
		{
			ItemMeta meta = armors.getItemMeta();
			meta.setDisplayName("Armor");
			armors.setItemMeta(meta);
			main.addItem(armors);
		}

		ItemStack power = new ItemStack(Material.IRON_SWORD);
		{
			ItemMeta meta = power.getItemMeta();
			meta.setDisplayName("Damage");
			power.setItemMeta(meta);
			main.addItem(power);
		}

		ItemStack stacks = new ItemStack(Material.IRON_SPADE);
		{
			ItemMeta meta = stacks.getItemMeta();
			meta.setDisplayName("Misc");
			stacks.setItemMeta(meta);
			main.addItem(stacks);
		}
		ItemStack abili = new ItemStack(Material.POTION);
		{
			ItemMeta meta = abili.getItemMeta();
			meta.setDisplayName("Abilities");
			abili.setItemMeta(meta);
			main.addItem(abili);
		}
		ItemStack aura = new ItemStack(Material.NETHER_STAR);
		{
			ItemMeta meta = aura.getItemMeta();
			meta.setDisplayName("Area");
			aura.setItemMeta(meta);
			main.addItem(aura);
		}
		ItemStack potion = new ItemStack(Material.POTION);
		{
			ItemMeta meta = potion.getItemMeta();
			meta.setDisplayName("Potions");
			potion.setItemMeta(meta);
			main.addItem(potion);
		}
		ItemStack actives = new ItemStack(Material.FEATHER);
		{
			ItemMeta meta = actives.getItemMeta();
			meta.setDisplayName("Actives");
			actives.setItemMeta(meta);
			main.addItem(actives);
		}

		playertopopeninv.put(player.getUniqueId(), "Main");
		player.openInventory(main);
	}

	private void storeactive(Player player) {
		int invsize = Area.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}
		Inventory inv = Bukkit.createInventory(null, invsize, "Store");
		int playerhas = 0;
		ArrayList<String> playerhasininv = new ArrayList<String>();
		for (int i = 30; i < 36; i++) {
			if (player.getInventory().getItem(i) != null
					&& player.getInventory().getItem(i).getType() != Material.AIR) {
				playerhasininv.add(player.getInventory().getItem(i)
						.getItemMeta().getDisplayName());
			}
		}
		HashMap<Actives, ArrayList<Actives>> tier2 = new HashMap<Actives, ArrayList<Actives>>();

		// mapping the tier2 to tier3
		for (Actives tl : Actives.values()) {
			Actives t = tl;
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				while (t.hasPrereq()) {

					if (t.hasPrereq() && !t.getPrereq().hasPrereq() && t != tl) {
						if (tier2.get(t) == null) {
							tier2.put(t,
									new ArrayList<Actives>(Arrays.asList(tl)));
						} else {
							tier2.get(t).add(tl);
						}

					}
					t = t.getPrereq();

				}
				if (playerhasininv.contains(tl.getName())) {
					playerhas++;
				}
			}
		}

		// counting how much the max is for one subtier
		HashMap<Actives, Integer> tier2c = new HashMap<Actives, Integer>();
		for (Actives tl : Actives.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				if (tier2.get(tl) != null) {
					for (Actives inlist : tier2.get(tl)) {
						if (playerhasininv.contains(inlist.getName())) {
							if (tier2c.get(tl) == null) {
								tier2c.put(tl, 1);
							} else {
								tier2c.put(tl, (tier2c.get(tl) + 1));
							}
						}
					}
				}
			}
		}
		// totalmax is for the whole tier3 including all branches
		int totalmax = 0;
		for (Actives keys : tier2.keySet()) {
			totalmax += tier2.get(keys).size();
		}
		for (Actives tl : Actives.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Actives t = tl;
				// basic standard parameters
				ItemStack in = new ItemStack(Material.FEATHER);
				ItemMeta m = in.getItemMeta();

				ArrayList<String> lore = new ArrayList<String>();
				lore.add(tl.getLore());

				// basic check if the player doesn't have the item and doesn't
				// have
				// the maximum
				if (totalmax != playerhas
						|| (!playerhasininv.contains(tl.getName())
								&& tier2.get(tl.getPrereq()) != null && tier2
								.get(tl.getPrereq()).size() != playerhas)) {
					// check if it is tier 2
					if (tier2.get(tl) != null) {
						// basic check if the player hasn't the maximum of that
						// branch
						if (tier2c.get(tl) != null
								&& tier2.get(tl).size() == tier2c.get(tl)) {
							m.setLore(lore);
							continue;
						}
						// adding the cost
						int cost = 0;
						t = tl;
						while (t.hasPrereq()) {
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							}
							t = t.getPrereq();
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));
					} else {
						int cost = t.getCost();
						t = tl;
						while (t.hasPrereq()) {
							t = t.getPrereq();
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							} else {
								break;
							}
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));

					}
					t = tl;
					while (t.hasPrereq()) {
						t = t.getPrereq();
						if (!playerhasininv.contains(t.getName())) {
							int cost = t.getCost();
							Actives t2 = t;
							while (t2.hasPrereq()) {
								t2 = t2.getPrereq();
								if (!playerhasininv.contains(t2.getName())) {
									cost += t2.getCost();
								} else {
									break;
								}
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));
						} else {
							break;
						}
					}

				}

				m.setLore(lore);
				m.setDisplayName(tl.getName());
				in.setItemMeta(m);
				inv.addItem(in);
			}
		}
		if (inv.getContents()[0] != null && inv.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Actives");
			player.openInventory(inv);
		} else {
			storemain(player);
		}
	}

	private void storepotion(Player player) {
		int invsize = Area.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}
		Inventory potions = Bukkit.createInventory(null, invsize, "Store");
		for (Potions pots : Potions.values()) {
			if (pots.getDef() == arena.players.get(player.getUniqueId())
					.getProf().typeattack()
					|| pots.getDef().equals("all")) {
				ItemStack item = new ItemStack(Material.POTION);
				ItemMeta meta = item.getItemMeta();
				meta.setDisplayName(pots.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.add("Duration: " + pots.getDuration());
				meta.setLore(lore);
				item.setItemMeta(meta);
				potions.addItem(item);
			}
		}
		if (potions.getContents()[0] != null && potions.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Potion");
			player.openInventory(potions);
		} else {
			storemain(player);
		}
	}

	private void storeaura(Player player) {
		int invsize = Area.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}

		Inventory inv = Bukkit.createInventory(null, invsize, "Store");
		int playerhas = 0;
		ArrayList<String> playerhasininv = new ArrayList<String>();
		for (int i = 30; i < 36; i++) {
			if (player.getInventory().getItem(i) != null
					&& player.getInventory().getItem(i).getType() != Material.AIR) {
				playerhasininv.add(player.getInventory().getItem(i)
						.getItemMeta().getDisplayName());
			}
		}
		HashMap<Area, ArrayList<Area>> tier2 = new HashMap<Area, ArrayList<Area>>();

		// mapping the tier2 to tier3
		for (Area tl : Area.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Area t = tl;
				while (t.hasPrereq()) {

					if (t.hasPrereq() && !t.getPrereq().hasPrereq() && t != tl) {
						if (tier2.get(t) == null) {
							tier2.put(t, new ArrayList<Area>(Arrays.asList(tl)));
						} else {
							tier2.get(t).add(tl);
						}

					}
					t = t.getPrereq();

				}
				if (playerhasininv.contains(tl.getName())) {
					playerhas++;
				}
			}
		}
		// counting how much the max is for one subtier
		HashMap<Area, Integer> tier2c = new HashMap<Area, Integer>();
		for (Area tl : Area.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				if (tier2.get(tl) != null) {
					for (Area inlist : tier2.get(tl)) {
						if (playerhasininv.contains(inlist.getName())) {
							if (tier2c.get(tl) == null) {
								tier2c.put(tl, 1);
							} else {
								tier2c.put(tl, (tier2c.get(tl) + 1));
							}
						}
					}
				}
			}
		}
		// totalmax is for the whole tier3 including all branches
		int totalmax = 0;
		for (Area keys : tier2.keySet()) {
			totalmax += tier2.get(keys).size();
		}
		for (Area tl : Area.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Area t = tl;
				// basic standard parameters
				ItemStack in = new ItemStack(tl.getType());
				ItemMeta m = in.getItemMeta();

				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(tl.getModifier());
				if (tl.getAura() != null) {
					lore.add(ChatColor.GREEN + tl.getAura());
				}

				// basic check if the player doesn't have the item and doesn't
				// have
				// the maximum
				if (totalmax != playerhas
						|| (!playerhasininv.contains(tl.getName())
								&& tier2.get(tl.getPrereq()) != null && tier2
								.get(tl.getPrereq()).size() != playerhas)) {
					// check if it is tier 2
					if (tier2.get(tl) != null) {
						// basic check if the player hasn't the maximum of that
						// branch
						if (tier2c.get(tl) != null
								&& tier2.get(tl).size() == tier2c.get(tl)) {
							m.setLore(lore);
							continue;
						}
						// adding the cost
						int cost = 0;
						t = tl;
						while (t.hasPrereq()) {
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							}
							t = t.getPrereq();
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));
					} else {
						int cost = t.getCost();
						t = tl;
						while (t.hasPrereq()) {
							t = t.getPrereq();
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							} else {
								break;
							}
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));

					}
					t = tl;
					while (t.hasPrereq()) {
						t = t.getPrereq();
						if (!playerhasininv.contains(t.getName())) {
							int cost = t.getCost();
							Area t2 = t;
							while (t2.hasPrereq()) {
								t2 = t2.getPrereq();
								if (!playerhasininv.contains(t2.getName())) {
									cost += t2.getCost();
								} else {
									break;
								}
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));
						} else {
							break;
						}
					}

				}

				m.setLore(lore);
				m.setDisplayName(tl.getName());
				in.setItemMeta(m);
				inv.addItem(in);

			}
		}
		if (inv.getContents()[0] != null && inv.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Area");
			player.openInventory(inv);
		} else {
			storemain(player);
		}
	}

	private void storearmor(Player player) {
		/**
		 * to add is a method or statement that shortens the list of prices to
		 * use with the prereq system. now its always the big list without
		 * changes made on it.
		 */
		int invsize = Armor.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}

		Inventory inv = Bukkit.createInventory(null, invsize, "Store");
		int playerhas = 0;
		ArrayList<String> playerhasininv = new ArrayList<String>();
		for (int i = 30; i < 36; i++) {
			if (player.getInventory().getItem(i) != null
					&& player.getInventory().getItem(i).getType() != Material.AIR) {
				playerhasininv.add(player.getInventory().getItem(i)
						.getItemMeta().getDisplayName());
			}
		}
		HashMap<Armor, ArrayList<Armor>> tier2 = new HashMap<Armor, ArrayList<Armor>>();

		// mapping the tier2 to tier3
		for (Armor tl : Armor.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Armor t = tl;
				while (t.hasPrereq()) {

					if (t.hasPrereq() && !t.getPrereq().hasPrereq() && t != tl) {
						if (tier2.get(t) == null) {
							tier2.put(t,
									new ArrayList<Armor>(Arrays.asList(tl)));
						} else {
							tier2.get(t).add(tl);
						}

					}
					t = t.getPrereq();

				}
				if (playerhasininv.contains(tl.getName())) {
					playerhas++;
				}
			}
		}
		// counting how much the max is for one subtier
		HashMap<Armor, Integer> tier2c = new HashMap<Armor, Integer>();
		for (Armor tl : Armor.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				if (tier2.get(tl) != null) {
					for (Armor inlist : tier2.get(tl)) {
						if (playerhasininv.contains(inlist.getName())) {
							if (tier2c.get(tl) == null) {
								tier2c.put(tl, 1);
							} else {
								tier2c.put(tl, (tier2c.get(tl) + 1));
							}
						}
					}
				}
			}
		}
		// totalmax is for the whole tier3 including all branches
		int totalmax = 0;
		for (Armor keys : tier2.keySet()) {
			totalmax += tier2.get(keys).size();
		}
		for (Armor tl : Armor.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Armor t = tl;
				// basic standard parameters
				ItemStack in = new ItemStack(tl.getType());
				ItemMeta m = in.getItemMeta();

				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(tl.getModifier());

				if (tl.getPassive() != null) {
					lore.add(ChatColor.GREEN + tl.getPassive());
				}
				// basic check if the player doesn't have the item and doesn't
				// have
				// the maximum
				if (totalmax != playerhas
						|| (!playerhasininv.contains(tl.getName())
								&& tier2.get(tl.getPrereq()) != null && tier2
								.get(tl.getPrereq()).size() != playerhas)) {
					// check if it is tier 2
					if (tier2.get(tl) != null) {
						// basic check if the player hasn't the maximum of that
						// branch
						if (tier2c.get(tl) != null
								&& tier2.get(tl).size() == tier2c.get(tl)) {
							m.setLore(tl.getModifier());
							continue;
						}
						// adding the cost
						int cost = 0;
						t = tl;
						while (t.hasPrereq()) {
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							}
							t = t.getPrereq();
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));
					} else {
						int cost = t.getCost();
						t = tl;
						while (t.hasPrereq()) {
							t = t.getPrereq();
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							} else {
								break;
							}
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));

					}
					t = tl;
					while (t.hasPrereq()) {
						t = t.getPrereq();
						if (!playerhasininv.contains(t.getName())) {
							int cost = t.getCost();
							Armor t2 = t;
							while (t2.hasPrereq()) {
								t2 = t2.getPrereq();
								if (!playerhasininv.contains(t2.getName())) {
									cost += t2.getCost();
								} else {
									break;
								}
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));
						} else {
							break;
						}
					}

				}

				m.setLore(lore);
				m.setDisplayName(tl.getName());
				in.setItemMeta(m);
				inv.addItem(in);
			}
		}
		if (inv.getContents()[0] != null && inv.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Armor");
			player.openInventory(inv);
		} else {
			storemain(player);
		}
	}

	private void storeweapons(Player player) {
		int invsize = Weapons.values().length + Critters.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}
		Inventory inv = Bukkit.createInventory(null, invsize, "Store");
		int playerhas = 0;
		ArrayList<String> playerhasininv = new ArrayList<String>();
		for (int i = 30; i < 36; i++) {
			if (player.getInventory().getItem(i) != null
					&& player.getInventory().getItem(i).getType() != Material.AIR) {
				playerhasininv.add(player.getInventory().getItem(i)
						.getItemMeta().getDisplayName());
			}
		}

		{
			HashMap<Weapons, ArrayList<Weapons>> tier2 = new HashMap<Weapons, ArrayList<Weapons>>();

			// mapping the tier2 to tier3
			for (Weapons tl : Weapons.values()) {
				if (tl.getDef().equals(arena.players.get(player.getUniqueId())
						.getProf().typeattack())
						|| tl.getDef().equals("all")) {
					Weapons t = tl;
					while (t.hasPrereq()) {

						if (t.hasPrereq() && !t.getPrereq().hasPrereq()
								&& t != tl) {
							if (tier2.get(t) == null) {
								tier2.put(
										t,
										new ArrayList<Weapons>(Arrays
												.asList(tl)));
							} else {
								tier2.get(t).add(tl);
							}

						}
						t = t.getPrereq();

					}
					if (playerhasininv.contains(tl.getName())) {
						playerhas++;
					}
				}
			}
			// counting how much the max is for one subtier
			HashMap<Weapons, Integer> tier2c = new HashMap<Weapons, Integer>();
			for (Weapons tl : Weapons.values()) {
				if (tl.getDef().equals(arena.players.get(player.getUniqueId())
						.getProf().typeattack())
						|| tl.getDef().equals("all")) {
					if (tier2.get(tl) != null) {
						for (Weapons inlist : tier2.get(tl)) {
							if (playerhasininv.contains(inlist.getName())) {
								if (tier2c.get(tl) == null) {
									tier2c.put(tl, 1);
								} else {
									tier2c.put(tl, (tier2c.get(tl) + 1));
								}
							}
						}
					}
				}
			}
			// totalmax is for the whole tier3 including all branches
			int totalmax = 0;
			for (Weapons keys : tier2.keySet()) {
				totalmax += tier2.get(keys).size();
			}
			for (Weapons tl : Weapons.values()) {
				if (tl.getDef().equals(arena.players.get(player.getUniqueId())
						.getProf().typeattack())
						|| tl.getDef().equals("all")) {
					Weapons t = tl;
					// basic standard parameters
					ItemStack in = new ItemStack(tl.getType());
					ItemMeta m = in.getItemMeta();

					ArrayList<String> lore = new ArrayList<String>();
					lore.addAll(tl.getModifier());

					if (tl.getPassive() != null) {
						lore.add(ChatColor.GREEN + tl.getPassive());
					}
					// basic check if the player doesn't have the item and
					// doesn't
					// have
					// the maximum
					if (totalmax != playerhas
							|| (!playerhasininv.contains(tl.getName())
									&& tier2.get(tl.getPrereq()) != null && tier2
									.get(tl.getPrereq()).size() != playerhas)) {
						// check if it is tier 2
						if (tier2.get(tl) != null) {
							// basic check if the player hasn't the maximum of
							// that
							// branch
							if (tier2c.get(tl) != null
									&& tier2.get(tl).size() == tier2c.get(tl)) {
								m.setLore(tl.getModifier());
								continue;
							}
							// adding the cost
							int cost = 0;
							t = tl;
							while (t.hasPrereq()) {
								if (!playerhasininv.contains(t.getName())) {
									cost += t.getCost();
								}
								t = t.getPrereq();
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));
						} else {
							int cost = t.getCost();
							t = tl;
							while (t.hasPrereq()) {
								t = t.getPrereq();
								if (!playerhasininv.contains(t.getName())) {
									cost += t.getCost();
								} else {
									break;
								}
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));

						}
						t = tl;
						while (t.hasPrereq()) {
							t = t.getPrereq();
							if (!playerhasininv.contains(t.getName())) {
								int cost = t.getCost();
								Weapons t2 = t;
								while (t2.hasPrereq()) {
									t2 = t2.getPrereq();
									if (!playerhasininv.contains(t2.getName())) {
										cost += t2.getCost();
									} else {
										break;
									}
								}
								lore.add(ChatColor.GOLD
										+ Integer.toString(cost));
							} else {
								break;
							}
						}

					}

					m.setLore(lore);
					m.setDisplayName(tl.getName());
					in.setItemMeta(m);
					inv.addItem(in);
				}
			}
		}

		{
			HashMap<Critters, ArrayList<Critters>> tier2 = new HashMap<Critters, ArrayList<Critters>>();

			// mapping the tier2 to tier3
			for (Critters tl : Critters.values()) {
				if (tl.getDef().equals(arena.players.get(player.getUniqueId())
						.getProf().typeattack())
						|| tl.getDef().equals("all")) {
					Critters t = tl;
					while (t.hasPrereq()) {

						if (t.hasPrereq() && !t.getPrereq().hasPrereq()
								&& t != tl) {
							if (tier2.get(t) == null) {
								tier2.put(
										t,
										new ArrayList<Critters>(Arrays
												.asList(tl)));
							} else {
								tier2.get(t).add(tl);
							}

						}
						t = t.getPrereq();

					}
					if (playerhasininv.contains(tl.getName())) {
						playerhas++;
					}
				}
			}
			// counting how much the max is for one subtier
			HashMap<Critters, Integer> tier2c = new HashMap<Critters, Integer>();
			for (Critters tl : Critters.values()) {
				if (tl.getDef().equals(arena.players.get(player.getUniqueId())
						.getProf().typeattack())
						|| tl.getDef().equals("all")) {
					if (tier2.get(tl) != null) {
						for (Critters inlist : tier2.get(tl)) {
							if (playerhasininv.contains(inlist.getName())) {
								if (tier2c.get(tl) == null) {
									tier2c.put(tl, 1);
								} else {
									tier2c.put(tl, (tier2c.get(tl) + 1));
								}
							}
						}
					}
				}
			}
			// totalmax is for the whole tier3 including all branches
			int totalmax = 0;
			for (Critters keys : tier2.keySet()) {
				totalmax += tier2.get(keys).size();
			}
			for (Critters tl : Critters.values()) {
				if (tl.getDef().equals(arena.players.get(player.getUniqueId())
						.getProf().typeattack())
						|| tl.getDef().equals("all")) {
					Critters t = tl;
					// basic standard parameters
					ItemStack in = new ItemStack(tl.getType());
					ItemMeta m = in.getItemMeta();

					ArrayList<String> lore = new ArrayList<String>();
					lore.addAll(tl.getModifier());

					if (tl.getPassive() != null) {
						lore.add(ChatColor.GREEN + tl.getPassive());
					}
					// basic check if the player doesn't have the item and
					// doesn't
					// have
					// the maximum
					if (totalmax != playerhas
							|| (!playerhasininv.contains(tl.getName())
									&& tier2.get(tl.getPrereq()) != null && tier2
									.get(tl.getPrereq()).size() != playerhas)) {
						// check if it is tier 2
						if (tier2.get(tl) != null) {
							// basic check if the player hasn't the maximum of
							// that
							// branch
							if (tier2c.get(tl) != null
									&& tier2.get(tl).size() == tier2c.get(tl)) {
								m.setLore(lore);
								continue;
							}
							// adding the cost
							int cost = 0;
							t = tl;
							while (t.hasPrereq()) {
								if (!playerhasininv.contains(t.getName())) {
									cost += t.getCost();
								}
								t = t.getPrereq();
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));
						} else {
							int cost = t.getCost();
							t = tl;
							while (t.hasPrereq()) {
								t = t.getPrereq();
								if (!playerhasininv.contains(t.getName())) {
									cost += t.getCost();
								} else {
									break;
								}
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));

						}
						t = tl;
						while (t.hasPrereq()) {
							t = t.getPrereq();
							if (!playerhasininv.contains(t.getName())) {
								int cost = t.getCost();
								Critters t2 = t;
								while (t2.hasPrereq()) {
									t2 = t2.getPrereq();
									if (!playerhasininv.contains(t2.getName())) {
										cost += t2.getCost();
									} else {
										break;
									}
								}
								lore.add(ChatColor.GOLD
										+ Integer.toString(cost));
							} else {
								break;
							}
						}

					}

					m.setLore(lore);
					m.setDisplayName(tl.getName());
					in.setItemMeta(m);
					inv.addItem(in);
				}
			}
		}
		if (inv.getContents()[0] != null && inv.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Weapons");
			player.openInventory(inv);
		} else {
			storemain(player);
		}
	}

	private void storemisc(Player player) {
		int invsize = Stackers.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}

		Inventory inv = Bukkit.createInventory(null, invsize, "Store");
		int playerhas = 0;
		ArrayList<String> playerhasininv = new ArrayList<String>();
		for (int i = 30; i < 36; i++) {
			if (player.getInventory().getItem(i) != null
					&& player.getInventory().getItem(i).getType() != Material.AIR) {
				playerhasininv.add(player.getInventory().getItem(i)
						.getItemMeta().getDisplayName());
			}
		}
		HashMap<Stackers, ArrayList<Stackers>> tier2 = new HashMap<Stackers, ArrayList<Stackers>>();

		// mapping the tier2 to tier3
		for (Stackers tl : Stackers.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Stackers t = tl;
				while (t.hasPrereq()) {

					if (t.hasPrereq() && !t.getPrereq().hasPrereq() && t != tl) {
						if (tier2.get(t) == null) {
							tier2.put(t,
									new ArrayList<Stackers>(Arrays.asList(tl)));
						} else {
							tier2.get(t).add(tl);
						}

					}
					t = t.getPrereq();

				}
				if (playerhasininv.contains(tl.getName())) {
					playerhas++;
				}
			}
		}
		// counting how much the max is for one subtier
		HashMap<Stackers, Integer> tier2c = new HashMap<Stackers, Integer>();
		for (Stackers tl : Stackers.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				if (tier2.get(tl) != null) {
					for (Stackers inlist : tier2.get(tl)) {
						if (playerhasininv.contains(inlist.getName())) {
							if (tier2c.get(tl) == null) {
								tier2c.put(tl, 1);
							} else {
								tier2c.put(tl, (tier2c.get(tl) + 1));
							}
						}
					}
				}
			}
		}
		// totalmax is for the whole tier3 including all branches
		int totalmax = 0;
		for (Stackers keys : tier2.keySet()) {
			totalmax += tier2.get(keys).size();
		}
		for (Stackers tl : Stackers.values()) {
			if (tl.getDef().equals(arena.players.get(player.getUniqueId())
					.getProf().typeattack())
					|| tl.getDef().equals("all")) {
				Stackers t = tl;
				// basic standard parameters
				ItemStack in = new ItemStack(tl.getType());
				ItemMeta m = in.getItemMeta();

				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(tl.getModifier());

				if (tl.getPassive() != null) {
					lore.add(ChatColor.GREEN + tl.getPassive());
				}
				// basic check if the player doesn't have the item and doesn't
				// have
				// the maximum
				if (totalmax != playerhas
						|| (!playerhasininv.contains(tl.getName())
								&& tier2.get(tl.getPrereq()) != null && tier2
								.get(tl.getPrereq()).size() != playerhas)) {
					// check if it is tier 2
					if (tier2.get(tl) != null) {
						// basic check if the player hasn't the maximum of that
						// branch
						if (tier2c.get(tl) != null
								&& tier2.get(tl).size() == tier2c.get(tl)) {
							m.setLore(tl.getModifier());
							continue;
						}
						// adding the cost
						int cost = 0;
						t = tl;
						while (t.hasPrereq()) {
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							}
							t = t.getPrereq();
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));
					} else {
						int cost = t.getCost();
						t = tl;
						while (t.hasPrereq()) {
							t = t.getPrereq();
							if (!playerhasininv.contains(t.getName())) {
								cost += t.getCost();
							} else {
								break;
							}
						}
						lore.add(ChatColor.GOLD + Integer.toString(cost));

					}
					t = tl;
					while (t.hasPrereq()) {
						t = t.getPrereq();
						if (!playerhasininv.contains(t.getName())) {
							int cost = t.getCost();
							Stackers t2 = t;
							while (t2.hasPrereq()) {
								t2 = t2.getPrereq();
								if (!playerhasininv.contains(t2.getName())) {
									cost += t2.getCost();
								} else {
									break;
								}
							}
							lore.add(ChatColor.GOLD + Integer.toString(cost));
						} else {
							break;
						}
					}

				}

				m.setLore(lore);
				m.setDisplayName(tl.getName());
				in.setItemMeta(m);
				inv.addItem(in);
			}
		}
		if (inv.getContents()[0] != null && inv.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Stackers");
			player.openInventory(inv);
		} else {
			storemain(player);
		}
	}

	private void storeability(Player player) {
		int invsize = Ability.values().length;
		if (invsize % 9 == 0) {
			invsize++;
		}
		while (invsize % 9 != 0) {
			invsize++;
		}

		if (invsize == 0) {
			invsize = 9;
		}

		ArrayList<String> contained = new ArrayList<String>();
		if (player.getInventory().getItem(4) != null
				&& player.getInventory().getItem(4).getType() != Material.AIR) {
			contained.add(player.getInventory().getItem(4).getItemMeta()
					.getDisplayName());
		}
		if (player.getInventory().getItem(3) != null
				&& player.getInventory().getItem(3).getType() != Material.AIR) {
			contained.add(player.getInventory().getItem(3).getItemMeta()
					.getDisplayName());
		}

		Inventory inv = Bukkit.createInventory(null, invsize, "Store");
		for (Ability ab : Ability.values()) {

			if (ab != null) {
				if (ab.getDef() == arena.players.get(player.getUniqueId())
						.getProf().typeattack()
						|| ab.getDef().equals("all")) {
					Material mat;
					if (ab.getParticle().equals("line")) {
						mat = Material.ARROW;
					} else {
						mat = Material.POTION;
					}
					ItemStack item = new ItemStack(mat);
					ItemMeta meta = item.getItemMeta();
					meta.setDisplayName(ab.getName());

					ArrayList<String> lore = new ArrayList<String>();
					lore.add(ab.getParticle());
					lore.add("Damage: " + ab.getDam());

					if (!contained.contains(ab.getName())
							&& contained.size() != 2) {
						lore.add(ChatColor.GOLD
								+ Double.toString(ab.getCosts()));
					}
					meta.setLore(lore);
					item.setItemMeta(meta);
					inv.addItem(item);
				}
			}
		}
		if (inv.getContents()[0] != null && inv.getContents()[0].getType() != Material.AIR) {
			playertopopeninv.put(player.getUniqueId(), "Ability");
			player.openInventory(inv);
		} else {
			storemain(player);
		}
	}

	public void oninventoryclick(InventoryClickEvent event, Player player,
			Champions champ) {
		if (!event.isShiftClick()) {
			if (event.getClickedInventory().getName().equals("Store")) {
				if (event.getCurrentItem() != null) {
					ItemStack item = event.getCurrentItem();
					if (item.hasItemMeta()) {
						// switch to different tab
						if (item.getItemMeta().getDisplayName().equals("Armor")) {
							storearmor(player);
							event.setCancelled(true);
							return;
						} else if (item.getItemMeta().getDisplayName()
								.equals("Damage")) {
							storeweapons(player);
							event.setCancelled(true);
							return;
						} else if (item.getItemMeta().getDisplayName()
								.equals("Misc")) {
							storemisc(player);
							event.setCancelled(true);
							return;
						} else if (item.getItemMeta().getDisplayName()
								.equals("Abilities")) {
							storeability(player);
							event.setCancelled(true);
							return;
						} else if (item.getItemMeta().getDisplayName()
								.equals("Area")) {
							storeaura(player);
							event.setCancelled(true);
							return;
						} else if (item.getItemMeta().getDisplayName()
								.equals("Potions")) {
							storepotion(player);
							event.setCancelled(true);
							return;
						} else if (item.getItemMeta().getDisplayName()
								.equals("Actives")) {
							storeactive(player);
							event.setCancelled(true);
							return;
						}
						// buying the item
						else {
							String itemname = item.getItemMeta()
									.getDisplayName();
							// check prereq first

							ArrayList<String> playerhasininv = new ArrayList<String>();
							for (int i = 30; i < 36; i++) {
								if (player.getInventory().getItem(i) != null
										&& player.getInventory().getItem(i)
												.getType() != Material.AIR) {
									playerhasininv.add(player.getInventory()
											.getItem(i).getItemMeta()
											.getDisplayName());
								}
							}

							/**
							 * all armor items bought
							 * 
							 */
							if (Armor.returnarmor(itemname) != null) {
								ItemStack replacement = null;

								Armor crititem = Armor.returnarmor(itemname);
								int cost = 0;
								Armor t = crititem;

								int size = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									size++;
								}

								// checking the prereq and adding the costs if
								// needed
								while (t.hasPrereq()) {
									// player doesn't have the item so adding
									// costs
									if (!playerhasininv.contains(t.getName())) {
										cost += t.getCost();
										// cost if greater than player has to
										// spend to lessing it
										if (cost > champ.getGold()) {
											Armor t2 = crititem;
											while (cost > champ.getGold()) {
												cost -= t2.getCost();
												t2 = t2.getPrereq();
											}
											crititem = t2;
											break;
										}
										size++;
										// player has so breaking
									} else {
										replacement = new ItemStack(t.getType());
										ItemMeta m = replacement.getItemMeta();
										m.setDisplayName(t.getName());
										replacement.setItemMeta(m);
										break;
									}
									t = t.getPrereq();
									// this is the same as for the prereq but
									// now for tier1
									if (!t.hasPrereq()) {
										if (!playerhasininv.contains(t
												.getName())) {
											cost += t.getCost();
											if (cost > champ.getGold()) {
												Armor t2 = crititem;
												while (cost > champ.getGold()) {
													cost -= t2.getCost();
													t2 = t2.getPrereq();
												}
												crititem = t2;
												break;
											}
										}
									}
								}
								int playerhas = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									playerhas++;
								}
								if (playerhas == size
										|| playerhasininv.size() == 6) {
									return;
								}

								ItemStack buying = new ItemStack(
										crititem.getType());
								ItemMeta meta = buying.getItemMeta();
								meta.setDisplayName(crititem.getName());
								List<String> lore = crititem.getModifier();
								if (crititem.getPassive() != null) {
									lore.add(ChatColor.GREEN
											+ crititem.getPassive());
								}
								meta.setLore(lore);
								buying.setItemMeta(meta);
								champ.setGold(champ.getGold() - cost);
								additem(player, item, replacement);

							} else
							/**
							 * all weapons
							 */
							if (Weapons.returnweapon(itemname) != null) {
								ItemStack replacement = null;

								Weapons crititem = Weapons
										.returnweapon(itemname);
								int cost = 0;
								Weapons t = crititem;

								int size = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									size++;
								}

								// checking the prereq and adding the costs if
								// needed
								while (t.hasPrereq()) {
									// player doesn't have the item so adding
									// costs
									if (!playerhasininv.contains(t.getName())) {
										cost += t.getCost();
										// cost if greater than player has to
										// spend to lessing it
										if (cost > champ.getGold()) {
											Weapons t2 = crititem;
											while (cost > champ.getGold()) {
												cost -= t2.getCost();
												t2 = t2.getPrereq();
											}
											crititem = t2;
											break;
										}
										size++;
										// player has so breaking
									} else {
										replacement = new ItemStack(t.getType());
										ItemMeta m = replacement.getItemMeta();
										m.setDisplayName(t.getName());
										replacement.setItemMeta(m);
										break;
									}
									t = t.getPrereq();
									// this is the same as for the prereq but
									// now for tier1
									if (!t.hasPrereq()) {
										if (!playerhasininv.contains(t
												.getName())) {
											cost += t.getCost();
											if (cost > champ.getGold()) {
												Weapons t2 = crititem;
												while (cost > champ.getGold()) {
													cost -= t2.getCost();
													t2 = t2.getPrereq();
												}
												crititem = t2;
												break;
											}
										}
									}
								}
								int playerhas = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									playerhas++;
								}
								if (playerhas == size
										|| playerhasininv.size() == 6) {
									return;
								}

								ItemStack buying = new ItemStack(
										crititem.getType());
								ItemMeta meta = buying.getItemMeta();
								meta.setDisplayName(crititem.getName());
								List<String> lore = crititem.getModifier();
								if (crititem.getPassive() != null) {
									lore.add(ChatColor.GREEN
											+ crititem.getPassive());
								}
								meta.setLore(lore);
								buying.setItemMeta(meta);
								champ.setGold(champ.getGold() - cost);
								additem(player, item, replacement);

							} else

							/**
							 * all stackers
							 */
							if (Stackers.returnstacker(itemname) != null) {
								ItemStack replacement = null;

								Stackers crititem = Stackers
										.returnstacker(itemname);
								int cost = 0;
								Stackers t = crititem;

								int size = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									size++;
								}

								// checking the prereq and adding the costs if
								// needed
								while (t.hasPrereq()) {
									// player doesn't have the item so adding
									// costs
									if (!playerhasininv.contains(t.getName())) {
										cost += t.getCost();
										// cost if greater than player has to
										// spend to lessing it
										if (cost > champ.getGold()) {
											Stackers t2 = crititem;
											while (cost > champ.getGold()) {
												cost -= t2.getCost();
												t2 = t2.getPrereq();
											}
											crititem = t2;
											break;
										}
										size++;
										// player has so breaking
									} else {
										replacement = new ItemStack(t.getType());
										ItemMeta m = replacement.getItemMeta();
										m.setDisplayName(t.getName());
										replacement.setItemMeta(m);
										break;
									}
									t = t.getPrereq();
									// this is the same as for the prereq but
									// now for tier1
									if (!t.hasPrereq()) {
										if (!playerhasininv.contains(t
												.getName())) {
											cost += t.getCost();
											if (cost > champ.getGold()) {
												Stackers t2 = crititem;
												while (cost > champ.getGold()) {
													cost -= t2.getCost();
													t2 = t2.getPrereq();
												}
												crititem = t2;
												break;
											}
										}
									}
								}
								int playerhas = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									playerhas++;
								}
								if (playerhas == size
										|| playerhasininv.size() == 6) {
									return;
								}

								ItemStack buying = new ItemStack(
										crititem.getType());
								ItemMeta meta = buying.getItemMeta();
								meta.setDisplayName(crititem.getName());
								List<String> lore = crititem.getModifier();
								if (crititem.getPassive() != null) {
									lore.add(ChatColor.GREEN
											+ crititem.getPassive());
								}
								meta.setLore(lore);
								buying.setItemMeta(meta);
								champ.setGold(champ.getGold() - cost);
								additem(player, item, replacement);

							} else
							/**
							 * all critters
							 */
							if (Critters.returncritter(itemname) != null) {
								ItemStack replacement = null;

								Critters crititem = Critters
										.returncritter(itemname);
								int cost = 0;
								Critters t = crititem;

								int size = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									size++;
								}

								// checking the prereq and adding the costs if
								// needed
								while (t.hasPrereq()) {
									// player doesn't have the item so adding
									// costs
									if (!playerhasininv.contains(t.getName())) {
										cost += t.getCost();
										// cost if greater than player has to
										// spend to lessing it
										if (cost > champ.getGold()) {
											Critters t2 = crititem;
											while (cost > champ.getGold()) {
												cost -= t2.getCost();
												t2 = t2.getPrereq();
											}
											crititem = t2;
											break;
										}
										size++;
										// player has so breaking
									} else {
										replacement = new ItemStack(t.getType());
										ItemMeta m = replacement.getItemMeta();
										m.setDisplayName(t.getName());
										replacement.setItemMeta(m);
										break;
									}
									t = t.getPrereq();
									// this is the same as for the prereq but
									// now for tier1
									if (!t.hasPrereq()) {
										if (!playerhasininv.contains(t
												.getName())) {
											cost += t.getCost();
											if (cost > champ.getGold()) {
												Critters t2 = crititem;
												while (cost > champ.getGold()) {
													cost -= t2.getCost();
													t2 = t2.getPrereq();
												}
												crititem = t2;
												break;
											}
										}
									}
								}
								int playerhas = crititem.getModifier().size();
								if (crititem.getPassive() != null) {
									playerhas++;
								}
								if (playerhas == size
										|| playerhasininv.size() == 6) {
									return;
								}

								ItemStack buying = new ItemStack(
										crititem.getType());
								ItemMeta meta = buying.getItemMeta();
								meta.setDisplayName(crititem.getName());
								List<String> lore = crititem
										.getModifier();
								if (crititem.getPassive() != null) {
									lore.add(ChatColor.GREEN
											+ crititem.getPassive());
								}
								meta.setLore(lore);
								buying.setItemMeta(meta);
								champ.setGold(champ.getGold() - cost);
								additem(player, item, replacement);

							} else
							/**
							 * All abilities
							 */
							if (Ability.returnability(itemname) != null) {
								if (item.getItemMeta().getLore().size() == 2) {
									return;
								}
								Ability ab = Ability.returnability(itemname);
								double needed = ab.getCosts();
								double gold = champ.getGold();
								if (gold >= needed) {
									champ.setGold((int) (gold - needed));
									additem(player, item, null);
								}
							} else

							/**
							 * all area item
							 */
							if (Area.returnAura(itemname) != null) {
								ItemStack replacement = null;

								Area crititem = Area.returnAura(itemname);
								int cost = 0;
								Area t = crititem;

								int size = crititem.getModifier().size();

								// checking the prereq and adding the costs if
								// needed
								while (t.hasPrereq()) {
									// player doesn't have the item so adding
									// costs
									if (!playerhasininv.contains(t.getName())) {
										cost += t.getCost();
										// cost if greater than player has to
										// spend to lessing it
										if (cost > champ.getGold()) {
											Area t2 = crititem;
											while (cost > champ.getGold()) {
												cost -= t2.getCost();
												t2 = t2.getPrereq();
											}
											crititem = t2;
											break;
										}
										size++;
										// player has so breaking
									} else {
										replacement = new ItemStack(t.getType());
										ItemMeta m = replacement.getItemMeta();
										m.setDisplayName(t.getName());
										replacement.setItemMeta(m);
										break;
									}
									t = t.getPrereq();
									// this is the same as for the prereq but
									// now for tier1
									if (!t.hasPrereq()) {
										if (!playerhasininv.contains(t
												.getName())) {
											cost += t.getCost();
											if (cost > champ.getGold()) {
												Area t2 = crititem;
												while (cost > champ.getGold()) {
													cost -= t2.getCost();
													t2 = t2.getPrereq();
												}
												crititem = t2;
												break;
											}
										}
									}
								}
								int playerhas = crititem.getModifier().size();
								if (playerhas == size
										|| playerhasininv.size() == 6) {
									return;
								}

								ItemStack buying = new ItemStack(
										crititem.getType());
								ItemMeta meta = buying.getItemMeta();
								meta.setDisplayName(crititem.getName());
								List<String> lore = crititem
										.getModifier();
								if (crititem.getAura() != null) {
									lore.add(ChatColor.GREEN
											+ crititem.getAura());
								}
								meta.setLore(lore);
								buying.setItemMeta(meta);
								champ.setGold(champ.getGold() - cost);
								additem(player, item, replacement);

							} else
							/**
							 * all potions
							 */
							if (Potions.returnpotion(itemname) != null) {
								Potions pot = Potions.returnpotion(itemname);
								if (champ.getGold() >= pot.getCosts()) {
									if (player.getInventory().getItem(7) != null
											&& player.getInventory().getItem(7)
													.getType() != Material.AIR) {
										if (player.getInventory().getItem(7)
												.getItemMeta().getDisplayName()
												.equals(pot.getName())) {
											champ.setGold(champ.getGold()
													- pot.getCosts());
											player.getInventory()
													.getItem(7)
													.setAmount(
															player.getInventory()
																	.getItem(7)
																	.getAmount() + 1);
										} else {
											if (player.getInventory()
													.getItem(8) != null
													&& player.getInventory()
															.getItem(8)
															.getType() != Material.AIR) {
												if (player.getInventory()
														.getItem(8)
														.getItemMeta()
														.getDisplayName()
														.equals(pot.getName())) {
													champ.setGold(champ
															.getGold()
															- pot.getCosts());
													player.getInventory()
															.getItem(8)
															.setAmount(
																	player.getInventory()
																			.getItem(
																					8)
																			.getAmount() + 1);
												}
											} else {
												champ.setGold(champ.getGold()
														- pot.getCosts());
												ItemStack potion = new ItemStack(
														Material.POTION);
												ItemMeta meta = potion
														.getItemMeta();
												meta.setDisplayName(itemname);
												potion.setItemMeta(meta);
												potion.setAmount(1);
												player.getInventory().setItem(
														8, potion);
											}
										}
									} else {
										champ.setGold(champ.getGold()
												- pot.getCosts());
										ItemStack potion = new ItemStack(
												Material.POTION);
										ItemMeta meta = potion.getItemMeta();
										meta.setDisplayName(itemname);
										potion.setItemMeta(meta);
										potion.setAmount(1);
										player.getInventory()
												.setItem(7, potion);
									}
								}
							} else
							/**
							 * all actives
							 */
							if (Actives.returnactive(itemname) != null) {
								ItemStack replacement = null;

								Actives crititem = Actives
										.returnactive(itemname);
								int cost = 0;
								Actives t = crititem;

								int size = 1;

								// checking the prereq and adding the costs if
								// needed
								while (t.hasPrereq()) {
									// player doesn't have the item so adding
									// costs
									if (!playerhasininv.contains(t.getName())) {
										cost += t.getCost();
										// cost if greater than player has to
										// spend to lessing it
										if (cost > champ.getGold()) {
											Actives t2 = crititem;
											while (cost > champ.getGold()) {
												cost -= t2.getCost();
												t2 = t2.getPrereq();
											}
											crititem = t2;
											break;
										}
										size++;
										// player has so breaking
									} else {
										replacement = new ItemStack(
												Material.FEATHER);
										ItemMeta m = replacement.getItemMeta();
										m.setDisplayName(t.getName());
										replacement.setItemMeta(m);
										break;
									}
									t = t.getPrereq();
									// this is the same as for the prereq but
									// now for tier1
									if (!t.hasPrereq()) {
										if (!playerhasininv.contains(t
												.getName())) {
											cost += t.getCost();
											if (cost > champ.getGold()) {
												Actives t2 = crititem;
												while (cost > champ.getGold()) {
													cost -= t2.getCost();
													t2 = t2.getPrereq();
												}
												crititem = t2;
												break;
											}
										}
									}
								}
								int playerhas = 1;

								if (playerhas == size
										|| playerhasininv.size() == 6) {
									return;
								}

								champ.setGold(champ.getGold() - cost);
								additem(player, item, replacement);

							}
						}
					}
					event.setCancelled(true);
					player.closeInventory();

					if (playertopopeninv.get(player.getUniqueId()) != null) {
						if (!playertopopeninv.get(player.getUniqueId()).equals(
								"Main")) {
							if (playertopopeninv.get(player.getUniqueId())
									.equals("Armor")) {
								playertopopeninv.remove(player.getUniqueId());
								storearmor(player);
							} else if (playertopopeninv.get(
									player.getUniqueId()).equals("Weapons")) {
								playertopopeninv.remove(player.getUniqueId());
								storeweapons(player);
							} else if (playertopopeninv.get(
									player.getUniqueId()).equals("Stackers")) {
								playertopopeninv.remove(player.getUniqueId());
								storemisc(player);
							} else if (playertopopeninv.get(
									player.getUniqueId()).equals("Ability")) {
								playertopopeninv.remove(player.getUniqueId());
								storeability(player);
							} else if (playertopopeninv.get(
									player.getUniqueId()).equals("Potion")) {
								playertopopeninv.remove(player.getUniqueId());
								storepotion(player);
							} else if (playertopopeninv.get(
									player.getUniqueId()).equals("Actives")) {
								playertopopeninv.remove(player.getUniqueId());
								storeactive(player);
							}

							else if (playertopopeninv.get(player.getUniqueId())
									.equals("Area")) {
								playertopopeninv.remove(player.getUniqueId());
								storeaura(player);
							}
						} else {
							playertopopeninv.remove(player.getUniqueId());
						}
					}

				}

			} else {
				if (event.getClickedInventory().getName().contains("Select")) {
					if (event.getCurrentItem() != null
							&& event.getCurrentItem().getType() != Material.AIR) {
						ItemStack item = event.getCurrentItem();
						Profession prof = Profession.profreturn(item
								.getItemMeta().getDisplayName());
						champ.setProf(prof);
						champ.setAttackspeed(prof.getAttackspeed());
						player.closeInventory();
						event.setCancelled(true);
					}
				}
			}
		}
	}

	// all works nothing to change about
	private void additem(Player player, ItemStack itemtoadd,
			ItemStack itemtoreplace) {
		for (Armor i : Armor.values()) {
			if (itemtoadd.getItemMeta().getDisplayName().equals(i.getName())) {
				itemtoadd = new ItemStack(i.getType());
				ItemMeta meta = itemtoadd.getItemMeta();
				meta.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(i.getModifier());
				if (i.getPassive() != null) {
					lore.add(ChatColor.GREEN + i.getPassive());
				}
				meta.setLore(lore);
				itemtoadd.setItemMeta(meta);
				break;
			}
		}
		for (Critters i : Critters.values()) {
			if (itemtoadd.getItemMeta().getDisplayName().equals(i.getName())) {
				itemtoadd = new ItemStack(i.getType());
				ItemMeta meta = itemtoadd.getItemMeta();
				meta.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(i.getModifier());
				if (i.getPassive() != null) {
					lore.add(ChatColor.GREEN + i.getPassive());
				}
				meta.setLore(lore);
				itemtoadd.setItemMeta(meta);
				break;
			}
		}
		for (Weapons i : Weapons.values()) {
			if (itemtoadd.getItemMeta().getDisplayName().equals(i.getName())) {
				itemtoadd = new ItemStack(i.getType());
				ItemMeta meta = itemtoadd.getItemMeta();
				meta.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(i.getModifier());
				if (i.getPassive() != null) {
					lore.add(ChatColor.GREEN + i.getPassive());
				}
				meta.setLore(lore);
				itemtoadd.setItemMeta(meta);
				break;
			}
		}
		for (Stackers i : Stackers.values()) {
			if (itemtoadd.getItemMeta().getDisplayName().equals(i.getName())) {
				itemtoadd = new ItemStack(i.getType());
				ItemMeta meta = itemtoadd.getItemMeta();
				meta.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(i.getModifier());
				if (i.getPassive() != null) {
					lore.add(ChatColor.GREEN + i.getPassive());
				}
				meta.setLore(lore);
				itemtoadd.setItemMeta(meta);
				break;
			}
		}
		for (Ability i : Ability.values()) {
			if (itemtoadd.getItemMeta().getDisplayName()
					.equals(i.getName())) {
				itemtoadd = new ItemStack(Material.NETHER_STAR);
				ItemMeta meta = itemtoadd.getItemMeta();
				meta.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.add(i.getParticle());
				lore.add("Damage: " + i.getDam());
				meta.setLore(lore);
				itemtoadd.setItemMeta(meta);
				break;
			}
		}
		for (Area i : Area.values()) {
			if (itemtoadd.getItemMeta().getDisplayName().equals(i.getName())) {
				itemtoadd = new ItemStack(i.getType());
				ItemMeta meta = itemtoadd.getItemMeta();
				meta.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.addAll(i.getModifier());
				if (i.getAura() != null) {
					lore.add(ChatColor.GREEN + i.getAura());
				}
				meta.setLore(lore);
				itemtoadd.setItemMeta(meta);
				break;
			}
		}

		for (Actives i : Actives.values()) {
			if (itemtoadd.getItemMeta().getDisplayName().equals(i.getName())) {
				itemtoadd = new ItemStack(Material.FEATHER);
				ItemMeta im = itemtoadd.getItemMeta();
				im.setDisplayName(i.getName());
				ArrayList<String> lore = new ArrayList<String>();
				lore.add(i.getLore());
				im.setLore(lore);
				itemtoadd.setItemMeta(im);
				break;
			}
		}

		if (Ability.returnability(itemtoadd.getItemMeta().getDisplayName()) == null
				&& Actives.returnactive(itemtoadd.getItemMeta()
						.getDisplayName()) == null) {

			if (itemtoreplace == null) {
				for (int i = 30; i < 36; i++) {
					if (player.getInventory().getItem(i) == null) {
						player.getInventory().setItem(i, itemtoadd);
						break;
					}
				}
			} else {
				for (int i = 30; i < 36; i++) {
					if (player.getInventory().getItem(i) != null
							&& player
									.getInventory()
									.getItem(i)
									.getItemMeta()
									.getDisplayName()
									.equals(itemtoreplace.getItemMeta()
											.getDisplayName())) {
						player.getInventory().setItem(i, itemtoadd);
						break;
					}
				}
			}
			if (Area.returnAura(itemtoadd.getItemMeta().getDisplayName()) != null) {
				plugin.aura.InitAuras(player);
			}
			for (String lore : itemtoadd.getItemMeta().getLore()) {
				if (lore.contains("Speed")) {
					for (String in : lore.split(" ")) {
						if (in.contains("+")) {
							try {
								double percent = Double.parseDouble(in.replace(
										"+", "").replace("%", ""));
								Champions champ = arena.players.get(player
										.getUniqueId());
								double newAttack = champ.getAttackspeed()
										- champ.getAttackspeed() / percent;
								if (newAttack < 0.79) {
									newAttack = 79;
								}
								champ.setAttackspeed(newAttack);
								break;
							} catch (NumberFormatException ex) {
								continue;
							}
						}
					}
				}
			}
		} else {
			if (Ability.returnability(itemtoadd.getItemMeta().getDisplayName()) != null) {
				for (int i = 3; i < 5; i++) {
					if (player.getInventory().getItem(i) == null) {
						player.getInventory().setItem(i, itemtoadd);
						break;
					}
				}
			} else {
				for (int i = 5; i < 7; i++) {
					if (player.getInventory().getItem(i) == null) {
						player.getInventory().setItem(i, itemtoadd);
						break;
					}
				}
			}
		}
	}

}
