package odin.tollenaar.stephen.ArenaStuff;

import java.util.ArrayList;
import java.util.UUID;

import odin.tollenaar.stephen.OdinsRevenge.OdinMain;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Champions {

	private OdinMain plugin;

	private int deaths;
	private int kills;
	private int assists;

	private int gold;

	private String playername;
	private UUID playeruuid;
	private String team;
	private Profession prof;
	
	private double attackspeed;
	
	

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getPlayername() {
		return playername;
	}

	public void setPlayername(String playername) {
		this.playername = playername;
	}

	public UUID getPlayeruuid() {
		return playeruuid;
	}

	public void setPlayeruuid(UUID playeruuid) {
		this.playeruuid = playeruuid;
	}

	public int getDeaths() {
		return deaths;
	}

	public void setDeaths(Player player) {
		this.deaths = deaths+1;
		player.getInventory().getItem(28).setAmount(deaths);
	}

	public int getKills() {
		return kills;
	}

	public void setKills(Player player) {
		this.kills = kills+1;
		player.getInventory().getItem(27).setAmount(kills);
	}

	public int getAssists() {
		return assists;
	}

	public void setAssists(int assists) {
		this.assists = assists;
	}

	public int getGold() {
		return gold;
	}

	public void setGold(int gold) {
		this.gold = gold;
		Player player = Bukkit.getPlayer(playeruuid);
		player.setLevel(gold);

	}

	public Champions(OdinMain instance, final Player player) {
		this.plugin = instance;
		this.deaths = 0;
		this.kills = 0;
		this.assists = 0;
		this.gold = 0;
		this.playername = player.getName();
		this.playeruuid = player.getUniqueId();
		this.team = "none";
		this.prof = Profession.NONE;
		this.attackspeed = 0;
		
		ItemStack kills = new ItemStack(Material.DIAMOND_SWORD);
		{
			ItemMeta meta = kills.getItemMeta();
			meta.setDisplayName("Number of kills");
			kills.setItemMeta(meta);
			kills.setAmount(0);
		}
		
		ItemStack deaths = new ItemStack(Material.SKULL_ITEM);
		{
			ItemMeta meta = deaths.getItemMeta();
			meta.setDisplayName("Number of deaths");
			deaths.setItemMeta(meta);
			deaths.setAmount(0);
		}
		player.getInventory().setItem(27, kills);
		player.getInventory().setItem(28, deaths);
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				proffessioninv(player);
			}

		}, 10L);

	}

	private void proffessioninv(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "Select Profession");
		for (Profession prof : Profession.values()) {
			if (prof != Profession.NONE) {
				ItemStack item = new ItemStack(prof.sym());
				ArrayList<String> lore = new ArrayList<String>();
				ItemMeta im = item.getItemMeta();
				im.setDisplayName(prof.name().toLowerCase());
				lore.add(prof.typeattack());
				lore.add(prof.getAttacksort());
				im.setLore(lore);
				item.setItemMeta(im);
				inv.addItem(item);
			}
		}
		player.openInventory(inv);
	}

	public Profession getProf() {
		return prof;
	}

	public void setProf(Profession prof) {
		this.prof = prof;
	}

	public double getAttackspeed() {
		return attackspeed;
	}

	public void setAttackspeed(double attackspeed) {
		this.attackspeed = attackspeed;
	}

}
