package odin.tollenaar.stephen.ArenaStuff;

import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.utils.Scheduler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import MoY.tollenaar.stephen.MistsOfYsir.MoY;
import MoY.tollenaar.stephen.MistsOfYsir.Party;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

public class ArenaInformations implements Listener {
	private Arenacheck archeck;
	private OdinMain plugin;


	public ArenaInformations(OdinMain instance) {
		this.archeck = instance.mov;
		this.plugin = instance;
		load();
	}

	private HashSet<Block> allsigns = new HashSet<Block>();

	@EventHandler
	public void onSignPlace(SignChangeEvent event) {
		Player player = event.getPlayer();
		PermissionUser user = PermissionsEx.getUser(player);
		if (user.has("Odin.signs")) {
			if (ArenaType.returnarena(event.getLine(0).replace("[", "")
					.replace("]", "")) != null) {
				ArenaType artype = ArenaType.returnarena(event.getLine(0)
						.replace("[", "").replace("]", ""));
				String line1 = ChatColor.BLUE + "Type: " + artype.getType();
				String line2;
				if (archeck.typestoid.get(artype) == null
						|| archeck.typestoid.get(artype).size() == 0) {
					line2 = ChatColor.RED + "Avaible Arenas: 0";
				} else {
					line2 = ChatColor.GREEN + "Avaible Arenas: "
							+ archeck.typestoid.get(artype).size();
				}

				String line3;
				if (archeck.que.get(artype) == null) {
					line3 = ChatColor.GOLD + "Qued: 0";
				} else {
					line3 = ChatColor.GOLD + "Qued: "
							+ archeck.que.get(artype).size();
				}

				String line4 = null;
				switch (artype) {
				case ARENA:
					if (archeck.acarena == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acarena;
					}
					break;
				case JOUST:
					if (archeck.acjoust == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acjoust;
					}
					break;
				case RANKED:
					if (archeck.acrank == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acrank;
					}
					break;
				case V1:
					if (archeck.ac1v1 == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.ac1v1;
					}
					break;
				case SIEGE:
					if (archeck.acsiege == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acsiege;
					}
					break;
				default:
					break;
				}

				event.setLine(0, line1);
				event.setLine(1, line2);
				event.setLine(2, line3);
				event.setLine(3, line4);

				event.getBlock().getState().update();
				allsigns.add(event.getBlock());
			} else if (event.getLine(0).replace("[", "").replace("]", "")
					.equals("ranklist")) {
				Location l = event.getBlock().getLocation();
				Hologram holo = HologramsAPI.createHologram(plugin, l);

				plugin.database.holofill(holo);
				event.getBlock().setType(Material.CARPET);
				allsigns.add(event.getBlock());
			}
		}
	}

	@EventHandler
	public void onRightClick(PlayerInteractEvent event) {
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if (event.getClickedBlock().getType() == Material.WALL_SIGN
					|| event.getClickedBlock().getType() == Material.SIGN_POST) {
				Player player = event.getPlayer();
				Sign sign = (Sign) event.getClickedBlock().getState();
				ArenaType artype = null;
				for (String in : sign.getLine(0).split(" ")) {
					if (ArenaType.returnarena(in) != null) {

						MoY m = (MoY) Bukkit.getPluginManager().getPlugin(
								"MistsOfYsir");
						Party p = m.party;

						artype = ArenaType.returnarena(in);
						if (p.Partymembers.get(player.getUniqueId()) != null
								&& p.partymatchmaking.get(player.getUniqueId())
								&& p.Partyleaders
										.get(p.Partymembers.get(player
												.getUniqueId())).compareTo(
												player.getUniqueId()) == 0) {
							if (playert(player, artype, p) == null) {
								player.sendMessage(ChatColor.AQUA
										+ "[Arena] "
										+ ChatColor.GRAY
										+ "Your party has to many persons in it. Leave the party or turn party matchmaking off");
								return;
							}
							Set<Player> topeform = playert(player, artype, p);
							if (topeform.size() != 0) {
								for (Player performing : topeform) {
									performing.performCommand("arena join "
											+ artype.getType());
								}
							}
						} else {
							player.performCommand("arena join "
									+ artype.getType());
						}
					}
				}
			}
		}
	}

	private Set<Player> playert(Player player, ArenaType type, Party p) {
		Set<Player> players = new HashSet<Player>();

		Set<UUID> toadd = p.partyfecther(player.getUniqueId());

		if (toadd.size() <= (type.getMaxplayers() / 2)) {
			if (archeck.que.get(type) != null) {
				if (archeck.que.get(type).size() + toadd.size() > type
						.getMaxplayers()) {
					return null;
				}
			}
			for (UUID inp : toadd) {
				Player pl = Bukkit.getPlayer(inp);
				players.add(pl);
			}
		} else {
			return null;
		}
		players.add(player);
		return players;
	}

	private void updater(Sign sign) {
		ArenaType artype = null;
		for (String in : sign.getLine(0).split(" ")) {
			if (ArenaType.returnarena(in) != null) {
				artype = ArenaType.returnarena(in);

				String line1 = ChatColor.BLUE + "Type: " + artype.getType();
				String line2;
				if (archeck.typestoid.get(artype) == null
						|| archeck.typestoid.get(artype).size() == 0) {
					line2 = ChatColor.RED + "Avaible Arenas: 0";
				} else {
					line2 = ChatColor.GREEN + "Avaible Arenas: "
							+ archeck.typestoid.get(artype).size();
				}
				String line3;
				if (archeck.que.get(artype) == null) {
					line3 = ChatColor.GOLD + "Qued: 0";
				} else {
					line3 = ChatColor.GOLD + "Qued: "
							+ archeck.que.get(artype).size();
				}

				String line4 = null;
				switch (artype) {
				case ARENA:
					if (archeck.acarena == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acarena;
					}
					break;
				case JOUST:
					if (archeck.acjoust == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acjoust;
					}
					break;
				case RANKED:
					if (archeck.acrank == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acrank;
					}
					break;
				case V1:
					if (archeck.ac1v1 == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.ac1v1;
					}
					break;
				case SIEGE:
					if (archeck.acsiege == 0) {
						line4 = ChatColor.RED + "Active arenas: 0";
					} else {
						line4 = ChatColor.GREEN + "Active arenas: "
								+ archeck.acsiege;
					}
					break;
				default:
					break;
				}

				sign.setLine(0, line1);
				sign.setLine(1, line2);
				sign.setLine(2, line3);
				sign.setLine(3, line4);

				sign.update();
				allsigns.add(sign.getBlock());
				save1(sign);
				break;
			}
		}
	}

	public void updateall() {
		for (Block sign : allsigns) {
			try{
			updater((Sign) sign.getState());
			}catch(ClassCastException ex){
				continue;
			}
		}
	}

	public void storeit() {
		for (Block blocks : allsigns) {
			try{
			plugin.fw.storesign((Sign) blocks.getState());
			}catch(ClassCastException ex){
				continue;
			}
		}
	}

	private void save1(Sign sign) {
		plugin.fw.storesign(sign);
	}

	public void repeating() {
		Scheduler.runAsyncTask(new Runnable() {
			public void run() {
				updateholos();
				repeating();
			}
		}, 10 * 60 * 20);
	}

	public void updateholos() {
		for (Hologram h : HologramsAPI.getHolograms(plugin)) {
			h.clearLines();
			loadholo(h);
		}
	}

	private void loadholo(Hologram holo) {
		plugin.database.holofill(holo);
	}

	public void load() {
		for (File file : plugin.fw.returnsings()) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Location loc = new Location(Bukkit.getWorld(config
					.getString("World")), config.getDouble("x"),
					config.getDouble("y"), config.getDouble("z"));
			Block block = loc.getBlock();
			if (block.getType() != Material.AIR) {
				Sign sign = (Sign) block.getState();
				allsigns.add(block);
				updater(sign);
			} else {
				file.delete();
			}
		}

	}
	

}
