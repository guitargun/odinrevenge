package odin.tollenaar.stephen.ArenaStuff;

public enum ArenaType {

	ARENA(30, 50, 10, "Arena"),
	JOUST(30, 20, 6, "Joust"),
	RANKED(60, 5, 2, "Ranked"),
	V1(30, 30, 2, "1v1"),
	SIEGE(30, 50, 4, "Siege");
	
	
	private final int maxtime;
	private final int maxplayers;
	private final int maxkill;
	private final String type;
	
	ArenaType(int maxtime, int maxkill, int maxplayers, String type){
		this.maxkill = maxkill;
		this.maxplayers = maxplayers;
		this.maxtime = maxtime;
		this.type = type;
	}

	public int getMaxtime() {
		return maxtime;
	}

	public int getMaxplayers() {
		return maxplayers;
	}

	public int getMaxkill() {
		return maxkill;
	}

	public String getType() {
		return type;
	}
	
	public static ArenaType returnarena(String type){
		for(ArenaType ar : values()){
			if(ar.getType().equals(type)){
				return ar;
			}
		}
		return null;
	}
	
	
}
