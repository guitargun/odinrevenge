

package odin.tollenaar.stephen.ArenaStuff;

import org.bukkit.Material;

public enum Profession {
	MAGE("magical", 1, 1.34, Material.POTION, "ranged attack", 1.07), WARRIOR(
			"physical", 1.34, 1, Material.DIAMOND_SWORD, "close combat", 1), NONE(
			"none", 1, 1, Material.FEATHER, "close combat", 0), HUNTER("physical", 1.34, 1, Material.BOW, "ranged attack", 1.08), 
			GUARDIAN("magical", 1, 1.34, Material.DIAMOND_CHESTPLATE, "close combat", 1.02);

	private final String type; // sort of attacking
	private final double magpen; // penalty damage magical 1 is none
	private final double physpen; // penalty damage physical 1 is none
	private final Material symbol;
	private final String attacksort;
	private final double attackspeed;

	Profession(String sort, double magicalpen, double physicalpen,
			Material sym, String attack, double attackspeed) {
		this.type = sort;
		this.magpen = magicalpen;
		this.physpen = physicalpen;
		this.symbol = sym;
		this.attacksort = attack;
		this.attackspeed = attackspeed;
	}

	public double magicpen() {
		return magpen;
	}

	public double physpen() {
		return physpen;
	}

	public String typeattack() {
		return type;
	}

	public Material sym() {
		return symbol;
	}

	public String getAttacksort() {
		return attacksort;
	}

	public double getAttackspeed() {
		return attackspeed;
	}

	public static Profession profreturn(String name) {
		switch (name) {
		case "mage":
			return Profession.MAGE;
		case "warrior":
			return Profession.WARRIOR;
		default:
			return Profession.NONE;
		}
	}

}
