package odin.tollenaar.stephen.ArenaStuff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.UUID;

import odin.tollenaar.stephen.Abilities.Ability;
import odin.tollenaar.stephen.Items.Actives;
import odin.tollenaar.stephen.Items.Area;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.OdinsRevenge.Playerstats;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import MoY.tollenaar.stephen.MistsOfYsir.MoY;
import MoY.tollenaar.stephen.MistsOfYsir.Party;

@SuppressWarnings("deprecation")
public class Arenacheck implements Listener {
	private OdinMain plugin;

	// saved arena types
	public HashMap<Integer, Arena> arena1v1 = new HashMap<Integer, Arena>();
	public HashMap<Integer, Arena> rankedarenas = new HashMap<Integer, Arena>();
	public HashMap<Integer, Arena> joustarenas = new HashMap<Integer, Arena>();
	public HashMap<Integer, Arena> arenaarenas = new HashMap<Integer, Arena>();
	public HashMap<Integer, Arena> siegearenas = new HashMap<Integer, Arena>();

	protected int ac1v1 = 0;
	protected int acrank = 0;
	protected int acjoust = 0;
	protected int acarena = 0;
	protected int acsiege = 0;

	private HashMap<Integer, Arena> arenaids = new HashMap<Integer, Arena>(); // Temporary
																				// one
																				// till
																				// completion

	// typed and their number
	protected HashMap<ArenaType, HashSet<Integer>> typestoid = new HashMap<ArenaType, HashSet<Integer>>();
	// player to arena
	public HashMap<UUID, Arena> playertoarena = new HashMap<UUID, Arena>();
	// needed before creation
	private HashMap<Integer, ArrayList<Location>> arenaborder = new HashMap<Integer, ArrayList<Location>>();

	public HashMap<Integer, ArenaType> arenatypes = new HashMap<Integer, ArenaType>();

	private HashMap<UUID, ArrayList<String>> tempplayer = new HashMap<UUID, ArrayList<String>>();

	// active arenas
	public HashMap<Integer, Arena> arenas = new HashMap<Integer, Arena>();
	// players in que
	protected HashMap<ArenaType, HashSet<UUID>> que = new HashMap<ArenaType, HashSet<UUID>>();

	// ###START OF ARENA MAKING###
	public void arenamaker(Player player) {
		int index = 0;
		while (arenatypes.get(index) != null) {
			index++;
		}
		Location dummy = new Location(player.getWorld(), 0, -1, 0);
		ArrayList<Location> dum = new ArrayList<Location>();
		dum.add(dummy);
		dum.add(dummy);
		dum.add(dummy);
		dum.add(dummy);
		arenaborder.put(index, dum);
		arenatypes.put(index, ArenaType.ARENA);
		arenaeditor(player, index);
	}

	public void arenaeditor(Player player, int arenaid) {
		Inventory inv = Bukkit.createInventory(null, 18, "Arena");

		ArrayList<Location> usethis = new ArrayList<Location>();
		if (arenaborder.get(arenaid) != null) {
			usethis = arenaborder.get(arenaid);
		} else {
			Arena a = returnarena(arenatypes.get(arenaid), arenaid);
			usethis.addAll(searchminxmax(a.godspawnred));
			usethis.addAll(searchminxmax(a.godspawnblue));
		}

		ItemStack rmin = new ItemStack(Material.COMPASS);
		{
			ItemMeta meta = rmin.getItemMeta();
			meta.setDisplayName("RedSpawn Min Location");
			Location loc = usethis.get(0);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add("X " + loc.getX());
			lore.add("Y " + loc.getY());
			lore.add("Z " + loc.getZ());
			lore.add("World " + loc.getWorld().getName());
			meta.setLore(lore);
			rmin.setItemMeta(meta);
			inv.addItem(rmin);
		}

		ItemStack rmax = new ItemStack(Material.COMPASS);
		{
			ItemMeta meta = rmax.getItemMeta();
			meta.setDisplayName("RedSpawn Max Location");
			Location loc = usethis.get(1);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add("X " + loc.getX());
			lore.add("Y " + loc.getY());
			lore.add("Z " + loc.getZ());
			lore.add("World " + loc.getWorld().getName());
			meta.setLore(lore);
			rmax.setItemMeta(meta);
			inv.addItem(rmax);
		}

		ItemStack bmin = new ItemStack(Material.COMPASS);
		{
			ItemMeta meta = bmin.getItemMeta();
			meta.setDisplayName("BlueSpawn Min Location");
			Location loc = usethis.get(2);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add("X " + loc.getX());
			lore.add("Y " + loc.getY());
			lore.add("Z " + loc.getZ());
			lore.add("World " + loc.getWorld().getName());
			meta.setLore(lore);
			bmin.setItemMeta(meta);
			inv.addItem(bmin);
		}

		ItemStack bmax = new ItemStack(Material.COMPASS);
		{
			ItemMeta meta = bmax.getItemMeta();
			meta.setDisplayName("BlueSpawn Max Location");
			Location loc = usethis.get(3);
			ArrayList<String> lore = new ArrayList<String>();
			lore.add("X " + loc.getX());
			lore.add("Y " + loc.getY());
			lore.add("Z " + loc.getZ());
			lore.add("World " + loc.getWorld().getName());
			meta.setLore(lore);
			bmax.setItemMeta(meta);
			inv.addItem(bmax);
		}

		ItemStack info = new ItemStack(Material.BEDROCK);
		{
			ItemMeta meta = info.getItemMeta();
			meta.setDisplayName("Arena info");
			ArrayList<String> lore = new ArrayList<String>();
			lore.add(Integer.toString(arenaid));
			if (arenaborder.get(arenaid) == null) {
				lore.add("existing");
			}
			meta.setLore(lore);
			info.setItemMeta(meta);
			inv.setItem(inv.getSize() - 1, info);
		}

		ItemStack type = new ItemStack(Material.BOOK);
		{
			ItemMeta meta = type.getItemMeta();
			meta.setDisplayName("Arena type");
			ArrayList<String> lore = new ArrayList<String>();
			if (arenatypes.get(arenaid) != null) {
				lore.add(arenatypes.get(arenaid).getType());
			} else {
				lore.add(arenaids.get(arenaid).getType().getType());
			}
			meta.setLore(lore);
			type.setItemMeta(meta);
			inv.addItem(type);
		}

		ItemStack run = new ItemStack(Material.REDSTONE);
		{
			ItemMeta meta = run.getItemMeta();
			meta.setDisplayName("Create");
			run.setItemMeta(meta);
			inv.addItem(run);
		}
		player.openInventory(inv);
	}

	public void allarenas(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, "All Arena's");
		for (int index = 0; index < arenatypes.size(); index++) {
			ItemStack temp = new ItemStack(Material.COMPASS);
			ItemMeta t = temp.getItemMeta();
			t.setDisplayName("Arena " + index);
			temp.setItemMeta(t);
			inv.addItem(temp);
		}
		player.openInventory(inv);
	}

	@EventHandler
	public void oninventoryclick(InventoryClickEvent event) {
		if (event.getClickedInventory() != null) {
			if (event.getClickedInventory().getName().equals("Arena")) {
				Player player = (Player) event.getWhoClicked();
				if (event.getCurrentItem() != null) {

					ItemStack info = event.getClickedInventory().getItem(
							event.getClickedInventory().getSize() - 1);

					// type
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("Arena type")) {
						event.setCancelled(true);
						player.sendMessage("type the arena type (Arena/Ranked/Joust/1v1/Siege");
						ArrayList<String> temp = new ArrayList<String>();
						temp.add(info.getItemMeta().getLore().get(0));
						temp.add("type");
						if (info.getItemMeta().getLore().size() == 2
								&& info.getItemMeta().getLore().get(1)
										.equals("existing")) {
							temp.add("existing");
						}
						tempplayer.put(player.getUniqueId(), temp);
						player.closeInventory();
						return;
					}

					// REDSPAWN
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("RedSpawn Min Location")) {
						event.setCancelled(true);
						player.sendMessage("Go to 1 corner of the red spawn.");
						ArrayList<String> temp = new ArrayList<String>();
						temp.add(info.getItemMeta().getLore().get(0));
						temp.add("redmin");
						if (info.getItemMeta().getLore().size() == 2
								&& info.getItemMeta().getLore().get(1)
										.equals("existing")) {
							temp.add("existing");
						}
						tempplayer.put(player.getUniqueId(), temp);
						player.closeInventory();
						return;
					}
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("RedSpawn Max Location")) {
						event.setCancelled(true);
						player.sendMessage("Go to the oposite corner of the red spawn");
						ArrayList<String> temp = new ArrayList<String>();
						temp.add(info.getItemMeta().getLore().get(0));
						temp.add("redmax");
						if (info.getItemMeta().getLore().size() == 2
								&& info.getItemMeta().getLore().get(1)
										.equals("existing")) {
							temp.add("existing");
						}
						tempplayer.put(player.getUniqueId(), temp);
						player.closeInventory();
						return;
					}

					// BLUESPAWN
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("BlueSpawn Min Location")) {
						event.setCancelled(true);
						player.sendMessage("Go to 1 corner of the blue spawn");
						ArrayList<String> temp = new ArrayList<String>();
						temp.add(info.getItemMeta().getLore().get(0));
						temp.add("bluemin");
						if (info.getItemMeta().getLore().size() == 2
								&& info.getItemMeta().getLore().get(1)
										.equals("existing")) {
							temp.add("existing");
						}
						tempplayer.put(player.getUniqueId(), temp);
						player.closeInventory();
						return;
					}
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("BlueSpawn Max Location")) {
						event.setCancelled(true);
						player.sendMessage("Go to the oposite corner of the blue spawn");
						ArrayList<String> temp = new ArrayList<String>();
						temp.add(info.getItemMeta().getLore().get(0));
						temp.add("bluemax");
						if (info.getItemMeta().getLore().size() == 2
								&& info.getItemMeta().getLore().get(1)
										.equals("existing")) {
							temp.add("existing");
						}
						tempplayer.put(player.getUniqueId(), temp);
						player.closeInventory();
						return;
					}

					if (event.getCurrentItem().getType() == Material.BEDROCK) {
						event.setCancelled(true);
						return;
					}
					if (event.getCurrentItem().getItemMeta().getDisplayName()
							.equals("Create")) {
						int arnumber = Integer.parseInt(info.getItemMeta()
								.getLore().get(0));
						if (arenaborder.get(arnumber).get(0).getY() != -1
								&& arenaborder.get(arnumber).get(1).getY() != -1
								&& arenaborder.get(arnumber).get(2).getY() != -1
								&& arenaborder.get(arnumber).get(3).getY() != -1) {

							ArrayList<Location> real = premadelocations(arenaborder
									.get(arnumber));

							Location redmin = real.get(0);
							Location redmax = real.get(1);

							Location bluemin = real.get(2);
							Location bluemax = real.get(3);
							Arena arena = new Arena(plugin, redmin, redmax,
									bluemin, bluemax, arenatypes.get(arnumber),
									arnumber);
							switch (arena.getType()) {

							case ARENA:
								arenaarenas.put(arnumber, arena);
								break;
							case JOUST:
								joustarenas.put(arnumber, arena);
								break;
							case RANKED:
								rankedarenas.put(arnumber, arena);
								break;
							case V1:
								arena1v1.put(arnumber, arena);
								break;
							case SIEGE:
								siegearenas.put(arnumber, arena);
								break;
							default:
								break;
							}
							if (typestoid.get(arena.getType()) == null) {
								HashSet<Integer> numbers = new HashSet<Integer>();
								numbers.add(arnumber);
								typestoid.put(arena.getType(), numbers);
							} else {
								typestoid.get(arena.getType()).add(arnumber);
							}

							arenaborder.remove(arnumber);
							arenaids.remove(arnumber);
							player.sendMessage("Arena is made.");
							plugin.info.updateall();
						} else {
							player.sendMessage("This arena is not finished yet.");
						}
						player.closeInventory();
						event.setCancelled(true);
						return;
					}
				}
			} else if (event.getClickedInventory().getName()
					.equals("All Arena's")) {
				Player player = (Player) event.getWhoClicked();
				if (event.getCurrentItem() != null) {
					{
						String arena = event.getCurrentItem().getItemMeta()
								.getDisplayName().split(" ")[1];
						event.setCancelled(true);
						arenaeditor(player, Integer.parseInt(arena));
						return;
					}
				}
			} else {
				if (playertoarena.get(event.getWhoClicked().getUniqueId()) != null) {
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onplayermovev(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (playertoarena.get(player.getUniqueId()) != null) {
			Arena ar = playertoarena.get(player.getUniqueId());
			if (ar.repeater.containsKey(player.getUniqueId())
					|| ar.delayer.containsKey(player.getUniqueId())) {
				if (event.getTo().getX() != event.getFrom().getX()
						|| event.getTo().getZ() != event.getTo().getZ()) {
					Bukkit.getScheduler().cancelTask(
							ar.repeater.get(player.getUniqueId()));
					Bukkit.getScheduler().cancelTask(
							ar.delayer.get(player.getUniqueId()));
					ar.repeater.remove(player.getUniqueId());
					ar.delayer.remove(player.getUniqueId());
				}
			}
		}
	}

	@EventHandler
	public void onitemheld(PlayerItemHeldEvent event) {
		if (playertoarena.get(event.getPlayer().getUniqueId()) != null) {
			Arena arena = playertoarena.get(event.getPlayer().getUniqueId());
			if (event.getNewSlot() == 1) {
				arena.store.storemain(event.getPlayer());
			} else if (event.getNewSlot() == 3 || event.getNewSlot() == 4) {
				if (event.getPlayer().getInventory()
						.getItem(event.getNewSlot()) != null) {
					ItemStack item = event.getPlayer().getInventory()
							.getItem(event.getNewSlot());
					Ability ab = Ability.returnability(item.getItemMeta()
							.getDisplayName());
					arena.CastAbility(event.getPlayer(), ab.getParticle(), ab,
							event.getNewSlot());
					return;
				}
			} else if (event.getNewSlot() == 5 || event.getNewSlot() == 6) {
				if (event.getPlayer().getInventory()
						.getItem(event.getNewSlot()) != null
						&& event.getPlayer().getInventory()
								.getItem(event.getNewSlot()).getType() != Material.AIR) {
					arena.onActive(
							event.getPlayer(),
							Actives.returnactive(event.getPlayer()
									.getInventory().getItem(event.getNewSlot())
									.getItemMeta().getDisplayName()),
							event.getNewSlot());
				}
			} else if (event.getNewSlot() == 2) {
				arena.baseteleport(event.getPlayer());
				event.setCancelled(true);
			} else if ((event.getNewSlot() == 7 || event.getNewSlot() == 8)
					&& event.getPlayer().getInventory()
							.getItem(event.getNewSlot()) != null
					&& event.getPlayer().getInventory()
							.getItem(event.getNewSlot()).getType() != Material.AIR) {
				arena.potionadder(event.getPlayer(), event.getNewSlot());
			}

			event.setCancelled(true);
			event.getPlayer().getInventory().setHeldItemSlot(0);
		}
	}

	@EventHandler
	public void onitemdrop(PlayerDropItemEvent event) {
		if (playertoarena.get(event.getPlayer().getUniqueId()) != null) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onchat(PlayerChatEvent event) {
		if (event.getMessage().equalsIgnoreCase("save")) {
			Player player = event.getPlayer();
			if (tempplayer.keySet().contains(player.getUniqueId())) {
				event.setCancelled(true);
				ArrayList<String> info = tempplayer.get(player.getUniqueId());
				int id = Integer.parseInt(info.get(0));
				Arena ar;
				if (tempplayer.get(player.getUniqueId()).contains("existing")
						&& returnarena(arenatypes.get(id), id) != null) {
					ar = returnarena(arenatypes.get(id), id);
				} else {
					ar = null;
				}

				// REDSPAWN
				if (info.get(1).equals("redmin")) {
					if (ar == null) {
						arenaborder.get(id).set(0, player.getLocation());
					} else {
						ar.godspawnred.add(player.getLocation());
						ArrayList<Location> l = searchminxmax(ar.godspawnred);
						ar.godspawnred.clear();
						ar.godspawnred
								.addAll(ar.makespawns(l.get(0), l.get(1)));
					}
					tempplayer.remove(player.getUniqueId());
					player.sendMessage("Location set.");
				} else if (info.get(1).equals("redmax")) {
					if (ar == null) {
						arenaborder.get(id).set(1, player.getLocation());
					} else {
						ar.godspawnred.add(player.getLocation());
						ArrayList<Location> l = searchminxmax(ar.godspawnred);
						ar.godspawnred.clear();
						ar.godspawnred
								.addAll(ar.makespawns(l.get(0), l.get(1)));
					}
					tempplayer.remove(player.getUniqueId());
					player.sendMessage("Location set.");
				} else

				// BLUESPAWN
				if (info.get(1).equals("bluemin")) {
					if (ar == null) {
						arenaborder.get(id).set(2, player.getLocation());
					} else {
						ar.godspawnblue.add(player.getLocation());
						ArrayList<Location> l = searchminxmax(ar.godspawnblue);
						ar.godspawnblue.clear();
						ar.godspawnblue
								.addAll(ar.makespawns(l.get(0), l.get(1)));
					}
					tempplayer.remove(player.getUniqueId());
					player.sendMessage("Location set.");
				} else if (info.get(1).equals("bluemax")) {
					if (ar == null) {
						arenaborder.get(id).set(3, player.getLocation());
					} else {
						ar.godspawnblue.add(player.getLocation());
						ArrayList<Location> l = searchminxmax(ar.godspawnblue);
						ar.godspawnblue.clear();
						ar.godspawnblue
								.addAll(ar.makespawns(l.get(0), l.get(1)));
					}
					tempplayer.remove(player.getUniqueId());
					player.sendMessage("Location set.");
				}

			}
		} else {
			if (ArenaType.returnarena(event.getMessage()) != null) {
				Player player = event.getPlayer();
				if (tempplayer.containsKey(player.getUniqueId())) {
					ArrayList<String> info = tempplayer.get(player
							.getUniqueId());
					int id = Integer.parseInt(info.get(0));
					boolean changed = false;
					if (tempplayer.get(player.getUniqueId()).size() == 3
							&& tempplayer.get(player.getUniqueId()).get(2)
									.equals("existing")
							&& returnarena(arenatypes.get(id), id) != null) {

						Arena a = returnarena(arenatypes.get(id), id);

						// THIS IS PURE FOR THE ARENA ITSELF
						a.setType(ArenaType.returnarena(event.getMessage()));

						// THIS IS FOR THE LIST SWITCHING
						newtypesetting(a.getType(), arenatypes.get(id), id, a);
						changed = true;

					}
					arenatypes.put(id,
							ArenaType.returnarena(event.getMessage()));

					if (changed) {
						plugin.info.updateall();
					}

					event.setCancelled(true);
					player.sendMessage("type set");
				}
			}
		}
	}

	@EventHandler
	public void playerquic(PlayerQuitEvent event) {
		if (playertoarena.get(event.getPlayer().getUniqueId()) != null) {
			Arena arena = playertoarena.get(event.getPlayer().getUniqueId());
			arena.players.remove(event.getPlayer().getUniqueId());
			Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
					"qtempban " + event.getPlayer().getName()
							+ " 30m [Arena] deserter penalty");
			playertoarena.remove(event.getPlayer().getUniqueId());
		}
	}

	@EventHandler
	public void onplayerjoin(PlayerJoinEvent event) {
		plugin.fw.loadinv(event.getPlayer());
		if (Playerstats.fromto.get(event.getPlayer().getUniqueId()) == null) {
			Playerstats p = new Playerstats(event.getPlayer().getUniqueId(),
					plugin);
			plugin.database.saveplayer(p);
			plugin.info.updateholos();
		}
	}

	// ###END OF ARENA MAKING###

	// ARENA LISTENERS
	@EventHandler
	public void onplayerdeath(PlayerDeathEvent event) {
		Player player = event.getEntity();
		if (playertoarena.get(player.getUniqueId()) != null) {
			Arena arena = playertoarena.get(player.getUniqueId());
			arena.ongoddeath(event);

			String line = ChatColor.AQUA + "[Arena] " + ChatColor.GRAY
					+ event.getEntity().getName();
			if (player.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
				EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) player
						.getLastDamageCause();
				if (e.getDamager() instanceof Projectile) {
					if (e.getDamager() instanceof Snowball) {
						line += " was cursed by "
								+ player.getKiller().getName();
					}
					Projectile pr = (Projectile) e.getDamager();
					if (pr.hasMetadata("aura")) {
						Area au = Area.returnAura(pr.getMetadata("aura").get(0)
								.asString());
						line += " was killed with " + au.getName()
								+ " worn by " + player.getKiller().getName();
					}
					if (pr.hasMetadata("abilitie")) {
						Ability ab = Ability.returnability(pr
								.getMetadata("abilitie").get(0).asString());
						line += " was killed with " + ab.getName()
								+ " used by " + player.getKiller().getName();
					}
				} else {
					line += " was slain by "
							+ event.getEntity().getKiller().getName();
				}
				event.setDeathMessage(line);
			}
		}
	}

	@EventHandler
	public void onstoreclick(InventoryClickEvent event) {
		if (event.getClickedInventory() != null
				&& (event.getClickedInventory().getName().equals("Store") || event
						.getClickedInventory().getName().contains("Select"))) {
			Player player = (Player) event.getWhoClicked();
			if (playertoarena.get(player.getUniqueId()) != null) {
				if (event.getClickedInventory().getName().equals("Store")
						|| event.getClickedInventory().getName()
								.contains("Select")) {
					Arena arena = playertoarena.get(player.getUniqueId());
					arena.onstoreclick(event);
				} else {
					event.setCancelled(true);
				}
			}
		}
	}

	@EventHandler
	public void onrespawn(PlayerRespawnEvent event) {
		Player player = event.getPlayer();
		if (playertoarena.get(player.getUniqueId()) != null) {
			Arena arena = playertoarena.get(player.getUniqueId());
			arena.onrespawn(event);
		}
	}

	// END OF ARENA LISTENERS

	public void leavinque(Player player) {
		for (ArenaType types : que.keySet()) {
			if (que.get(types).contains(player.getUniqueId())) {
				player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "You have left the que.");
				que.get(types).remove(player.getUniqueId());
				plugin.info.updateall();

				return;
			}
		}
		player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
				+ "You can't leave an active game!");

	}

	public void joinarena(Player player, ArenaType type) {
		for (ArenaType types : que.keySet()) {
			if (que.get(types).contains(player.getUniqueId())) {
				player.sendMessage(ChatColor.AQUA
						+ "[Arena] "
						+ ChatColor.GOLD
						+ "You are already in the que. If you want to leave use "
						+ ChatColor.RED + " /arena leave.");
				return;
			}
		}
		switch (type) {
		case ARENA:
			if (arenaarenas.size() == 0) {
				player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "There are no " + type.getType()
						+ " arena's aviable. Try again later");
				return;
			}
			break;
		case JOUST:
			if (joustarenas.size() == 0) {
				player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "There are no " + type.getType()
						+ " arena's aviable. Try again later");
				return;
			}
			break;
		case RANKED:
			if (rankedarenas.size() == 0) {
				player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "There are no " + type.getType()
						+ " arena's aviable. Try again later");
				return;
			}
			break;
		case V1:
			if (arena1v1.size() == 0) {
				player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "There are no " + type.getType()
						+ " arena's aviable. Try again later");
				return;
			}
			break;
		case SIEGE:
			if (siegearenas.size() == 0) {
				player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "There are no " + type.getType()
						+ " arena's aviable. Try again later");
				return;
			}
			break;
		default:
			break;
		}
		if (que.get(type) == null) {
			HashSet<UUID> newq = new HashSet<UUID>();
			newq.add(player.getUniqueId());
			player.sendMessage(ChatColor.AQUA
					+ "[Arena] "
					+ ChatColor.GOLD
					+ "You are now in the que. You will be notified when your match is ready. "
					+ ChatColor.RED + "Use /arena leave " + ChatColor.GOLD
					+ " to leave the que.");
			que.put(type, newq);

			plugin.info.updateall();
			return;
		}
		if (que.get(type).size() != type.getMaxplayers() - 1) {
			que.get(type).add(player.getUniqueId());
			player.sendMessage(ChatColor.AQUA
					+ "[Arena] "
					+ ChatColor.GOLD
					+ "You are now in the que. You will be notified when your match is ready. Use /arena leave to leave the que.");

			plugin.info.updateall();
		} else {
			Arena ar = null;
			switch (type) {
			case ARENA: {
				int id = typestoid.get(type).iterator().next();
				ar = arenaarenas.get(id);
				arenaarenas.remove(id);
				typestoid.get(type).remove(id);
				acarena++;
				break;
			}
			case JOUST: {
				int id = typestoid.get(type).iterator().next();
				ar = joustarenas.get(id);
				joustarenas.remove(id);
				typestoid.get(type).remove(id);
				acjoust++;
				break;
			}
			case RANKED: {
				int id = typestoid.get(type).iterator().next();
				ar = rankedarenas.get(id);
				rankedarenas.remove(id);
				typestoid.get(type).remove(id);
				acrank++;
				break;
			}
			case V1: {
				int id = typestoid.get(type).iterator().next();
				ar = arena1v1.get(id);
				arena1v1.remove(id);
				typestoid.get(type).remove(id);
				ac1v1++;
				break;
			}
			case SIEGE: {
				int id = typestoid.get(type).iterator().next();
				ar = siegearenas.get(id);
				siegearenas.remove(id);
				typestoid.get(type).remove(id);
				ac1v1++;
				break;
			}
			default:
				break;

			}
			MoY m = (MoY) Bukkit.getPluginManager().getPlugin("MistsOfYsir");
			Party party = m.party;
			HashMap<String, ArrayList<UUID>> partys = new HashMap<String, ArrayList<UUID>>();
			for (UUID qued : que.get(type)) {
				Player pqued = Bukkit.getPlayer(qued);
				pqued.setGameMode(GameMode.SURVIVAL);
				pqued.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
						+ "Your match is ready. Ready yourself.");

				if (party.Partymembers.get(qued) != null
						&& party.partymatchmaking.get(qued)) {
					if (partys.get(party.Partymembers.get(pqued)) != null) {
						ArrayList<UUID> p = partys.get(party.Partymembers
								.get(qued));
						p.add(qued);
					} else {
						ArrayList<UUID> p = new ArrayList<UUID>();
						p.add(qued);
						partys.put(party.Partymembers.get(pqued), p);
					}
				} else {
					if (partys.get(ar.toString()) != null) {
						ArrayList<UUID> p = partys.get(ar.toString());
						p.add(qued);
					} else {
						ArrayList<UUID> p = new ArrayList<UUID>();
						p.add(qued);
						partys.put(ar.toString(), p);
					}
				}

			}

			if (party.Partymembers.get(player.getUniqueId()) != null
					&& party.partymatchmaking.get(player.getUniqueId())) {
				if (partys.get(party.Partymembers.get(player.getUniqueId())) != null) {
					ArrayList<UUID> p = partys.get(party.Partymembers
							.get(player.getUniqueId()));
					p.add(player.getUniqueId());
				} else if (partys.get(ar.toString()) != null) {
					ArrayList<UUID> p = partys.get(ar.toString());
					p.add(player.getUniqueId());
				} else {
					ArrayList<UUID> p = new ArrayList<UUID>();
					p.add(player.getUniqueId());
					partys.put(ar.toString(), p);
				}
			} else {
				if (partys.get(ar.toString()) != null) {
					ArrayList<UUID> p = partys.get(ar.toString());
					p.add(player.getUniqueId());
				} else {
					ArrayList<UUID> p = new ArrayList<UUID>();
					p.add(player.getUniqueId());
					partys.put(ar.toString(), p);
				}
			}

			int blue = 0;
			int red = 0;

			for (String keys : partys.keySet()) {
				if (!keys.equals(ar.toString())) {
					if (blue + partys.get(keys).size() <= type.getMaxplayers() / 2) {
						blue += partys.get(keys).size();
						for (UUID q : partys.get(keys)) {
							Player p = Bukkit.getPlayer(q);
							p.setGameMode(GameMode.SURVIVAL);
							p.sendMessage(ChatColor.AQUA + "[Arena] "
									+ ChatColor.GOLD
									+ "Your match is ready. Ready yourself.");
							playertoarena.put(q, ar);
							ar.joinarena(p, true, "blue");
						}
					} else if (red + partys.get(keys).size() <= type
							.getMaxplayers() / 2) {
						red += partys.get(keys).size();
						for (UUID q : partys.get(keys)) {
							Player p = Bukkit.getPlayer(q);
							p.setGameMode(GameMode.SURVIVAL);
							p.sendMessage(ChatColor.AQUA + "[Arena] "
									+ ChatColor.GOLD
									+ "Your match is ready. Ready yourself.");
							playertoarena.put(q, ar);
							ar.joinarena(p, true, "red");
						}
						partys.remove(keys);
					}
				}
			}
			if (partys.get(ar.toString()) != null) {
				for (UUID q : partys.get(ar.toString())) {
					Player p = Bukkit.getPlayer(q);
					playertoarena.put(q, ar);
					ar.joinarena(p, false, null);
				}
				partys.remove(ar.toString());
			}
			if (partys.size() != 0) {
				for (String keys : partys.keySet()) {
					for (UUID q : partys.get(keys)) {
						Player p = Bukkit.getPlayer(q);
						p.setGameMode(GameMode.SURVIVAL);
						p.sendMessage(ChatColor.AQUA + "[Arena] "
								+ ChatColor.GOLD
								+ "Your match is ready. Ready yourself.");
						playertoarena.put(q, ar);
						ar.joinarena(p, false, null);
					}
				}
			}

			for (int i = 0; i <= arenas.size(); i++) {
				if (!arenas.keySet().contains(i)) {
					arenas.put(i, ar);
					break;
				}
			}
			// finally clear it
			que.get(type).clear();
			plugin.info.updateall();
		}

	}

	public void reloadmap(Arena ar) {
		ArrayList<Location> godred = ar.godspawnred;
		ArrayList<Location> godblue = ar.godspawnblue;
		Arena newar = new Arena(plugin, godred, godblue, ar.getType(),
				ar.getid());
		switch (newar.getType()) {
		case ARENA:
			if (acarena != 0) {
				acarena--;
			}
			break;
		case JOUST:
			if (acjoust != 0) {
				acjoust--;
			}
			break;
		case V1:
			if (ac1v1 != 0) {
				ac1v1--;
			}
			break;
		case RANKED:
			if (acrank != 0) {
				acrank--;
			}
			break;
		case SIEGE:
			if (acsiege != 0) {
				acsiege--;
			}
			break;
		default:
			break;
		}
		createarena(newar.getid(), newar.getType(), newar);

	}

	public Arenacheck(OdinMain instance) {
		this.plugin = instance;
	}

	private void createarena(int id, ArenaType type, Arena arena) {
		switch (arena.getType()) {

		case ARENA:
			arenaarenas.put(id, arena);
			break;
		case JOUST:
			joustarenas.put(id, arena);
			break;
		case RANKED:
			rankedarenas.put(id, arena);
			break;
		case V1:
			arena1v1.put(id, arena);
			break;
		case SIEGE:
			siegearenas.put(id, arena);
			break;
		default:
			break;
		}
		if (typestoid.get(arena.getType()) == null) {
			HashSet<Integer> numbers = new HashSet<Integer>();
			numbers.add(id);
			typestoid.put(arena.getType(), numbers);
		} else {
			typestoid.get(arena.getType()).add(id);
		}

		arenaborder.remove(id);
		arenaids.remove(id);
		arenatypes.put(id, type);
		try {
			plugin.info.updateall();
		} catch (NullPointerException ex) {

		}
	}

	private ArrayList<Location> searchminxmax(ArrayList<Location> locations) {
		ArrayList<Location> l = new ArrayList<Location>();
		Location min = locations.get(0);
		Location max = locations.get(0);
		for (Location loc : locations) {
			if (min.getX() > loc.getX() || min.getY() > loc.getY()
					|| min.getZ() > loc.getZ()) {
				min = loc;
			}
			if (max.getX() < loc.getX() || max.getY() < loc.getY()
					|| max.getZ() < loc.getZ()) {
				max = loc;
			}
		}
		l.add(min);
		l.add(max);

		return l;
	}

	private ArrayList<Location> premadelocations(ArrayList<Location> locations) {
		ArrayList<Location> l = new ArrayList<Location>();
		Location rmin = locations.get(0);
		Location rmax = locations.get(1);

		Location bmin = locations.get(2);
		Location bmax = locations.get(3);

		double rxmin = Math.min(rmin.getX(), rmax.getX());
		double rymin = Math.min(rmin.getY(), rmax.getY());
		double rzmin = Math.min(rmin.getZ(), rmax.getZ());

		double rxmax = Math.max(rmin.getX(), rmax.getX());
		double rymax = Math.max(rmin.getY(), rmax.getY());
		double rzmax = Math.max(rmin.getZ(), rmax.getZ());

		double bxmin = Math.min(bmin.getX(), bmax.getX());
		double bymin = Math.min(bmin.getY(), bmax.getY());
		double bzmin = Math.min(bmin.getZ(), bmax.getZ());

		double bxmax = Math.max(bmin.getX(), bmax.getX());
		double bymax = Math.max(bmin.getY(), bmax.getY());
		double bzmax = Math.max(bmin.getZ(), bmax.getZ());

		rmin.setX(rxmin);
		rmin.setY(rymin);
		rmin.setZ(rzmin);
		rmax.setX(rxmax);
		rmax.setY(rymax);
		rmax.setZ(rzmax);

		bmin.setX(bxmin);
		bmin.setY(bymin);
		bmin.setZ(bzmin);
		bmax.setX(bxmax);
		bmax.setY(bymax);
		bmax.setZ(bzmax);

		l.add(rmin);
		l.add(rmax);
		l.add(bmin);
		l.add(bmax);

		return l;

	}

	private Arena returnarena(ArenaType artype, int id) {
		switch (artype) {
		case ARENA:
			return arenaarenas.get(id);
		case JOUST:
			return joustarenas.get(id);
		case RANKED:
			return rankedarenas.get(id);
		case SIEGE:
			return siegearenas.get(id);
		case V1:
			return arena1v1.get(id);
		default:
			return null;
		}
	}

	private void newtypesetting(ArenaType newtype, ArenaType oldtype, int id,
			Arena a) {
		typestoid.get(oldtype).remove(id);
		if (typestoid.get(newtype) == null) {
			HashSet<Integer> ids = new HashSet<Integer>();
			ids.add(id);
			typestoid.put(newtype, ids);
		} else {
			typestoid.get(newtype).add(id);
		}

		switch (oldtype) {
		case ARENA:
			arenaarenas.remove(id);

			break;
		case JOUST:
			joustarenas.remove(id);
			break;
		case RANKED:
			rankedarenas.remove(id);

			break;
		case SIEGE:
			siegearenas.remove(id);
			break;
		case V1:
			arena1v1.remove(id);
			break;
		default:
			break;

		}

		switch (newtype) {
		case ARENA:
			arenaarenas.put(id, a);
			break;
		case JOUST:
			joustarenas.put(id, a);
			break;
		case RANKED:
			rankedarenas.put(id, a);
			break;
		case SIEGE:
			siegearenas.put(id, a);
			break;
		case V1:
			arena1v1.put(id, a);
			break;
		default:
			break;

		}

	}

}
