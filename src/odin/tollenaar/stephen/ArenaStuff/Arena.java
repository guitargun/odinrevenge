package odin.tollenaar.stephen.ArenaStuff;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.UUID;








import odin.tollenaar.stephen.Abilities.Ability;
import odin.tollenaar.stephen.Items.Actives;
import odin.tollenaar.stephen.Items.Potions;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.OdinsRevenge.Playerstats;
import odin.tollenaar.stephen.messages.InfoBar;
import odin.tollenaar.stephen.utils.ParticleEffect;
import odin.tollenaar.stephen.utils.Scheduler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Arena {
	private OdinMain plugin;
	protected Store store;
	private Arena arena = this;
	private int blueteamkills;
	private int redteamkills;
	private ArenaType type;
	private int id;
	private InfoBar info;

	
	public HashMap<UUID, Champions> players = new HashMap<UUID, Champions>();
	// spawns
	public ArrayList<Location> godspawnred = new ArrayList<Location>();
	public ArrayList<Location> godspawnblue = new ArrayList<Location>();

	private long starttime = System.currentTimeMillis();

	protected HashMap<UUID, Integer> repeater = new HashMap<UUID, Integer>();
	protected HashMap<UUID, Integer> delayer = new HashMap<UUID, Integer>();

	// EVENT HANDLER MINION/PLAYER
	public void ongoddeath(PlayerDeathEvent event) {
		Player killer = event.getEntity().getKiller();
		Player player = event.getEntity();
		Champions killed = players.get(player.getUniqueId());
		Champions kill =players.get(killer.getUniqueId());

		killed.setDeaths(player);
		kill.setKills(killer);

		if (kill.getTeam().equals("red")) {
			redteamkills++;
		} else {
			blueteamkills++;
		}
		for (UUID uuid :players.keySet()) {
			Champions champ = players.get(uuid);
			if (uuid != killer.getUniqueId() && uuid != player.getUniqueId()) {
				if (champ.getTeam().equals(kill.getTeam())) {
					info.makeMessage(champ, 2, true, blueteamkills, redteamkills);
				} else {
					info.makeMessage(champ, 1, true, blueteamkills, redteamkills);
				}
			} else if (uuid == killer.getUniqueId()) {
				info.makeMessage(champ, 4, true, blueteamkills, redteamkills);
			} else {
				info.makeMessage(champ, 3, true, blueteamkills, redteamkills);
			}
		}

		for (int i = 30; i < 36; i++) {
			if (killer.getInventory().getItem(i) != null) {
				ItemStack item = killer.getInventory().getItem(i);
				if (item.getItemMeta().getLore().contains("Passive")
						&& item.getItemMeta().getLore().contains("every kill")) {
					for (String in : item.getItemMeta().getLore()) {
						if (in.contains("Passive")) {
							for (String in2 : in.split(" ")) {
								try {
									int max = Integer.parseInt(in2);
									if (item.getAmount() < max) {
										item.setAmount(item.getAmount() + 1);
									}
									break;
								} catch (NumberFormatException ex) {
									continue;
								}
							}

							break;
						}
					}
				}
			}
		}

		if (redteamkills == getType().getMaxkill()
				|| blueteamkills == getType().getMaxkill()) {
			String team;
			if (redteamkills == getType().getMaxkill()) {
				team = "Red";
			} else {
				team = "Blue";
			}
			endgame("Kills", team);
		}

	}

	public void onstoreclick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		store.oninventoryclick(event, player, players.get(player.getUniqueId()));

	}

	public void onActive(Player player, Actives ac, int slot) {
		if (player.getInventory().getItem(slot).getAmount() == 1) {
			for (PotionEffectType eff : ac.getPotioneffect()) {
				player.addPotionEffect(new PotionEffect(eff,
						ac.getDuration() * 20, ac.getAmplifier()));
			}
			player.getInventory().getItem(slot).setAmount(ac.getCooldown());
			cooldownt(player.getInventory().getItem(slot), ac.getCooldown());
		}
	}

	private void cooldownt(final ItemStack item, final int time) {
		Scheduler.runAsyncTask(new Runnable() {
			int t = time;

			public void run() {
				if (t != 1) {
					item.setAmount(item.getAmount() - 1);
					t--;
					cooldownt(item, t);
				}
			}
		}, 1L);

	}

	// END EVENT HANDLER PLAYER

	public void baseteleport(final Player player) {
		if (!repeater.containsKey(player.getUniqueId())
				&& !delayer.containsKey(player.getUniqueId())) {
			final int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(
					plugin, new Runnable() {
						public void run() {
							for (Location loc : plugin.ap.sqlocationsw(player,
									2)) {
								ParticleEffect.SPELL_MOB.display(
										Color.RED.getRed() / 255F,
										Color.RED.getGreen() / 255F,
										Color.RED.getBlue() / 255F, 1, 0, loc,
										20);
							}
						}
					}, 0, 10L);
			repeater.put(player.getUniqueId(), id);
			int id2 = Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
					new Runnable() {
						public void run() {
							Bukkit.getScheduler().cancelTask(id);
							repeater.remove(player.getUniqueId());
							delayer.remove(player.getUniqueId());
							Random r = new Random();
							if (players.get(player.getUniqueId()).getTeam()
									.equals("red")) {
								int i = r.nextInt(godspawnred.size() - 1);

								player.teleport(godspawnred.get(i));
							} else {
								int i = r.nextInt(godspawnblue.size() - 1);
								player.teleport(godspawnblue.get(i));
							}

						}
					}, 5 * 20L);
			delayer.put(player.getUniqueId(), id2);
		}
	}

	public void potionadder(Player player, int slot) {
		Potions pot = Potions.returnpotion(player.getInventory().getItem(slot)
				.getItemMeta().getDisplayName());
		player.addPotionEffect(new PotionEffect(pot.getType(), pot
				.getDuration() * 20, pot.getModifier()));
		if (player.getInventory().getItem(slot).getAmount() - 1 == 0) {
			player.getInventory().setItem(slot, null);
		} else {
			player.getInventory()
					.getItem(slot)
					.setAmount(
							player.getInventory().getItem(slot).getAmount() - 1);
		}
	}

	// ability casting
	public void CastAbility(Player player, String type, Ability ab, int current) {
		switch (type) {
		case "line":
			plugin.ap.drawline(player, ab.getSize(), ab.getColor(), current);
			break;
		case "area":
			plugin.ap.drawareasq(player, ab.getSize(), ab.getColor(), current);
			break;
		default:
			endgame(ChatColor.RED + "INTERNAL ERROR", "none");
			break;
		}
	}

	public void makearena(Location minred, Location maxred, Location minblue,
			Location maxblue) {

		godspawnblue.clear();
		godspawnred.clear();

		godspawnblue.addAll(makespawns(minblue, maxblue));
		godspawnred.addAll(makespawns(minred, maxred));

	}

	public void onrespawn(final PlayerRespawnEvent event) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				Player player = event.getPlayer();
				String team = players.get(player.getUniqueId()).getTeam();

				if (team.equals("red")) {
					Random r = new Random();
					int index = r.nextInt(godspawnred.size());
					player.teleport(godspawnred.get(index));
				} else {
					Random r = new Random();
					int index = r.nextInt(godspawnblue.size());
					player.teleport(godspawnblue.get(index));
				}
			}
		}, 20L);

	}

	public void joinarena(Player player, boolean party, String premade) {
		plugin.fw.storeinv(player.getUniqueId(), player.getInventory());
		player.getInventory().clear();
		player.getInventory().setArmorContents(null);
		player.getInventory().setHeldItemSlot(0);
		ItemStack basic = new ItemStack(Material.STICK);
		{
			ItemMeta bm = basic.getItemMeta();
			bm.setDisplayName("Basic Attack");
			basic.setItemMeta(bm);
		}

		ItemStack shopopener = new ItemStack(Material.GOLD_INGOT);
		{
			ItemMeta bm = shopopener.getItemMeta();
			bm.setDisplayName("Shop");
			shopopener.setItemMeta(bm);
			player.getInventory().setItem(1, shopopener);
		}
		ItemStack tp = new ItemStack(Material.COMPASS);
		{
			ItemMeta im = tp.getItemMeta();
			im.setDisplayName("Base teleport");
			tp.setItemMeta(im);
			player.getInventory().setItem(2, tp);
		}

		Damageable dam = player;
		player.setHealth(dam.getMaxHealth());
		player.setGameMode(GameMode.SURVIVAL);
		player.getInventory().setItemInHand(basic);
		Champions c = new Champions(plugin, player);
		


		teammaker(c, party, premade);

		c.setGold(10000);
		players.put(player.getUniqueId(), c);
		// this goes away menu for classes open
		player.sendMessage("You have joined team " + c.getTeam());
		if (c.getTeam().equals("red")) {
			Random r = new Random();
			int index = r.nextInt(godspawnred.size() - 1);
			player.teleport(godspawnred.get(index));
		} else {
			Random r = new Random();
			int index = r.nextInt(godspawnblue.size() - 1);
			player.teleport(godspawnblue.get(index));
		}
		info.makeMessage(c, 0, false, blueteamkills, redteamkills);

	}

	public void endgame(String type, String team) {
		info.cancelAll();
		int time = (int) (System.currentTimeMillis() - starttime);
		for (UUID p : players.keySet()) {
			Player player = Bukkit.getPlayer(p);
			plugin.mov.playertoarena.remove(p);
			player.sendMessage(ChatColor.AQUA + "[Arena] " + ChatColor.GOLD
					+ "Match has ended. Type of win: " + type
					+ ". Team that won: " + team + ".");
			plugin.fw.loadinv(player);
			Damageable dam = player;
			dam.setMaxHealth(20);
			player.setHealth(dam.getMaxHealth());
			Bukkit.dispatchCommand(Bukkit.getServer().getConsoleSender(),
					plugin.getConfig().getString("endgamecommand") + " "
							+ player.getName());
			Playerstats stats = Playerstats.fromto.get(p);
			Champions ch = players.get(player.getUniqueId());
			stats.setKills(stats.getKills() + ch.getKills());
			stats.setDeaths(stats.getDeaths() + ch.getDeaths());

			if (getType() == ArenaType.RANKED) {
				if (ch.getTeam().equalsIgnoreCase(team)) {
					int totalkills;
					if (ch.equals("Blue")) {
						totalkills = blueteamkills;
					} else {
						totalkills = redteamkills;
					}
					stats.setTributes(stats.getTributes()
							+ tributecalc(totalkills, time, true));
				} else {
					int totalkills;
					if (ch.equals("Blue")) {
						totalkills = blueteamkills;
					} else {
						totalkills = redteamkills;
					}
					if (stats.getTributes()
							- tributecalc(totalkills, time, false) < 0) {
						stats.setTributes(0);
					} else {
						stats.setTributes(stats.getTributes()
								- tributecalc(totalkills, time, false));
					}
				}

				plugin.info.updateholos();
			}

		}
		players.clear();
		

		plugin.mov.reloadmap(arena);
	}

	private int tributecalc(int totalkills, int totaltime, boolean win) {
		int total = (int) ((totalkills / totaltime) * 18.60);

		if (win) {
			return total + 5;
		} else {
			return total;
		}
	}

	private void teammaker(Champions player, boolean party, String premade) {
		if (!party) {
			int reds = 0;
			int blues = 0;
			for (Champions p : players.values()) {
				if (p.getTeam().equals("red")) {
					reds++;
				} else if (p.getTeam().equals("blue")) {
					blues++;
				}
			}
			if (reds != type.getMaxplayers() / 2
					&& blues != type.getMaxplayers() / 2) {
				if (reds > blues) {
					player.setTeam("blue");
				} else if (blues > reds) {
					player.setTeam("red");
				} else {
					Random r = new Random();
					int in = r.nextInt(2);
					if (in == 0) {
						player.setTeam("red");
					} else {
						player.setTeam("blue");
					}
				}
			} else if (reds != type.getMaxplayers() / 2) {
				player.setTeam("red");
			} else {
				player.setTeam("blue");
			}

		} else {
			player.setTeam(premade);
		}
	}

	public Arena(OdinMain instance, Location minred, Location maxred,
			Location minblue, Location maxblue, ArenaType type, int id) {
		this.plugin = instance;
		store = new Store(instance, this);
		this.setType(type);
		this.id = id;
		makearena(minred, maxred, minblue, maxblue);
	}

	public int getid() {
		return id;
	}

	public Arena(OdinMain instance, ArrayList<Location> godred,
			ArrayList<Location> godblue, ArenaType type, int id) {
		this.plugin = instance;
		godspawnblue = godblue;
		godspawnred = godred;
		store = new Store(instance, this);
		this.setType(type);
		this.id = id;
		this.info = new InfoBar(instance);
	}

	public ArenaType getType() {
		return type;
	}

	public void setType(ArenaType type) {
		this.type = type;
	}

	public ArrayList<Location> makespawns(Location min, Location max) {
		ArrayList<Location> l = new ArrayList<Location>();

		for (int bx = (int) min.getX(); bx < max.getX(); bx++) {
			for (int bz = (int) min.getZ(); bz < max.getZ(); bz++) {
				Location loc = new Location(min.getWorld(), bx, min.getY(), bz);
				l.add(loc);
			}
		}
		return l;
	}
}
