package odin.tollenaar.stephen.Attacks;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import odin.tollenaar.stephen.Abilities.AbilityParticle;
import odin.tollenaar.stephen.ArenaStuff.Arenacheck;
import odin.tollenaar.stephen.Items.Area;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.utils.ParticleEffect;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class AuraBehavior {
	private OdinMain plugin;
	private Arenacheck archeck;
	private AbilityParticle ap;

	public AuraBehavior(OdinMain instance) {
		this.plugin = instance;
		this.archeck = instance.mov;
		this.ap = instance.ap;
	}

	public void InitAuras(Player player) {
		ArrayList<ItemStack> content = new ArrayList<ItemStack>();
		for (int i = 30; i <36; i++) {
			if (player.getInventory().getItem(i) != null
					&& player.getInventory().getItem(i).getType() != Material.AIR) {
				content.add(player.getInventory().getItem(i));
			}
		}
		HashMap<ItemStack, Integer> haveaura = new HashMap<ItemStack, Integer>();
		for (ItemStack in : content) {
			for (String lores : in.getItemMeta().getLore()) {
				if (lores.contains("Aura")) {
					for (String containes : lores.split(" ")) {
						try {
							int delay = Integer.parseInt(containes);
							haveaura.put(in, delay);
							break;
						} catch (NumberFormatException ex) {
							continue;
						}
					}
					break;
				}
			}
		}
		if (haveaura.size() != 0) {
			for (ItemStack item : haveaura.keySet()) {
				CastAura(player, item, haveaura.get(item));
			}
			GlowPlayer(player);
		}

	}

	private void CastAura(final Player player, final ItemStack item,
			final int delay) {
		final Area ar = Area.returnAura(item.getItemMeta().getDisplayName());
		if (Arrays.asList(player.getInventory().getContents()).contains(item)) {
			Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
					new Runnable() {
						public void run() {
							if (archeck.playertoarena.get(player.getUniqueId()) != null) {
								List<Player> area = ap.sqloc(player,
										ar.getRange());
								for (Player players : area) {
									if (ar.getAura().contains("allies")) {
										if (archeck.playertoarena.get(players
												.getUniqueId()).players
												.get(players.getUniqueId())
												.getTeam()
												.equals(archeck.playertoarena
														.get(player
																.getUniqueId()).players
														.get(player
																.getUniqueId())
														.getTeam())) {
											AuraPassive(players, ar, player);
										}
									} else {
										if (!archeck.playertoarena.get(players
												.getUniqueId()).players
												.get(players.getUniqueId())
												.getTeam()
												.equals(archeck.playertoarena
														.get(player
																.getUniqueId()).players
														.get(player
																.getUniqueId())
														.getTeam())) {
											AuraPassive(players, ar, player);
										}
									}
								}
								CastAura(player, item, delay);
							}
						}
					}, delay * 20L);
		}
	}

	private void GlowPlayer(final Player player) {
		if (archeck.playertoarena.get(player.getUniqueId()) != null) {
			for (int i = 30; i < 36; i++) {
				if (player.getInventory().getItem(i) != null
						&& player.getInventory().getItem(i).getType() != Material.AIR) {
					try {
						if (Area.returnAura(player.getInventory().getItem(i)
								.getItemMeta().getDisplayName()) != null) {
							final int red = Color.CYAN.getRed();
							final int blue = Color.CYAN.getBlue();
							final int green = Color.CYAN.getGreen();
							Bukkit.getScheduler().scheduleSyncDelayedTask(
									plugin, new Runnable() {
										public void run() {
											for (Location l : Cone(player
													.getLocation())) {
												ParticleEffect.SPELL_MOB.display(red / 255F,
														green / 255F, blue / 255F, 1, 0, l, 10);
											}
											GlowPlayer(player);
										}
									}, 20L);
						}
						break;
					} catch (NullPointerException ex) {
						break;
					}
				}
			}
		}
	}

	private void AuraPassive(Player victim, Area ar, Player attacker) {
		String passive = ar.getAura();

		// only for enemies passive
		if (passive.contains("enemies")) {
			if (passive.contains("slows")) {
				victim.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,
						(int) (1.5 * 20), 1));
			}
			if (passive.contains("weakened")) {
				victim.addPotionEffect(new PotionEffect(
						PotionEffectType.WEAKNESS, (int) (1.5 * 20), 1));
			}
			if (passive.contains("blinds")) {

				victim.addPotionEffect(new PotionEffect(
						PotionEffectType.BLINDNESS, (int) (1.5 * 20), 3));
			}
			if (passive.contains("paralyzed")) {
				victim.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,
						(int) (1.5 * 20), 100));
			}
			if (passive.contains("damage")) {
				Arrow pr = victim.getWorld().spawn(victim.getLocation(),
						Arrow.class);
				pr.setMetadata("aura",
						new FixedMetadataValue(plugin, ar.getName()));
				pr.setShooter(attacker);
				pr.setVelocity(victim.getLocation().toVector().multiply(2.9));
				scheduling(pr, victim.getLocation(), 1);
			}
		}

		// friendly passive
		if (passive.contains("allies")) {
			if (passive.contains("regen")) {
				attacker.addPotionEffect(new PotionEffect(
						PotionEffectType.REGENERATION, (int) (1.5 * 20), 2));
				victim.addPotionEffect(new PotionEffect(
						PotionEffectType.REGENERATION, (int) (1.5 * 20), 2));
			}

			if (passive.contains("resistence")) {
				attacker.addPotionEffect(new PotionEffect(
						PotionEffectType.DAMAGE_RESISTANCE, (int) (1.5 * 20), 3));
				victim.addPotionEffect(new PotionEffect(
						PotionEffectType.DAMAGE_RESISTANCE, (int) (1.5 * 20), 3));
			}

			if (passive.contains("movementspeed")) {
				attacker.addPotionEffect(new PotionEffect(
						PotionEffectType.SPEED, (int) (1.5 * 20), 1));
				victim.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,
						(int) (1.5 * 20), 1));
			}

		}
	}

	private void scheduling(final Projectile ball, final Location startpoint,
			final int maxd) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				if (ball.getLocation().distance(startpoint) >= maxd) {
					ball.remove();
				} else {
					if (ball.isOnGround()) {
						ball.remove();
					} else {
						scheduling(ball, startpoint, maxd);
					}
				}
			}
		}, 5L);
	}

	private List<Location> Cone(Location center) {
		List<Location> cone = new ArrayList<Location>();
		int amount = 6;
		double increment = (2 * Math.PI) / amount;
		for (double h = 0; h < 0.7; h += 0.1) {
			for (int i = 0; i < amount; i++) {
				double angle = i * increment;
				double x = center.getX() + (0.35 * Math.cos(angle));
				double z = center.getZ() + (0.35 * Math.sin(angle));
				cone.add(new Location(center.getWorld(), x, center.getY() + h,
						z));
			}
		}

		return cone;
	}
}
