package odin.tollenaar.stephen.Attacks;

import java.awt.Color;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import odin.tollenaar.stephen.Abilities.Ability;
import odin.tollenaar.stephen.Abilities.AbilityParticle;
import odin.tollenaar.stephen.ArenaStuff.Arena;
import odin.tollenaar.stephen.ArenaStuff.Arenacheck;
import odin.tollenaar.stephen.ArenaStuff.Champions;
import odin.tollenaar.stephen.ArenaStuff.Profession;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.utils.EntityHider;
import odin.tollenaar.stephen.utils.ParticleEffect;
import odin.tollenaar.stephen.utils.Scheduler;
import odin.tollenaar.stephen.utils.EntityHider.Policy;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class BasicAttack implements Listener {
	protected Arenacheck archeck;
	private OdinMain plugin;
	protected HashMap<UUID, Long> cooldown = new HashMap<UUID, Long>();
	private EntityHider hider;

	private void SnowballHider(Snowball ball, Set<UUID> champs) {
		for (UUID in : champs) {
			Player player = Bukkit.getPlayer(in);
			hider.toggleEntity(player, ball);
		}
	}

	@EventHandler
	public void OnBasicAttack(PlayerInteractEvent event) {

		if (event.getAction() == Action.LEFT_CLICK_AIR
				|| event.getAction() == Action.LEFT_CLICK_BLOCK) {
			Player player = event.getPlayer();
			if (archeck.playertoarena.get(player.getUniqueId()) != null) {

				if (player.getInventory().getHeldItemSlot() == 0) {
					if (archeck.playertoarena.get(player.getUniqueId()).players
							.get(player.getUniqueId()).getProf()
							.getAttacksort().equals("ranged attack")) {
						if (cooldown.get(player.getUniqueId()) == null) {
							cooldown.put(
									player.getUniqueId(),
									parseDateDiff(
											archeck.playertoarena.get(player
													.getUniqueId()).players
													.get(player.getUniqueId())
													.getAttackspeed()
													+ "s", true, System
													.currentTimeMillis()));
						} else {

							if (cooldown.get(player.getUniqueId()) <= System
									.currentTimeMillis()) {

								cooldown.put(
										player.getUniqueId(),
										parseDateDiff(
												archeck.playertoarena
														.get(player
																.getUniqueId()).players
														.get(player
																.getUniqueId())
														.getAttackspeed()
														+ "s", true, System
														.currentTimeMillis()));

							} else {

								return;
							}
						}
						Champions champ = archeck.playertoarena.get(player
								.getUniqueId()).players.get(player
								.getUniqueId());
						if (champ.getProf().getAttacksort()
								.equals("ranged attack")) {
							final Snowball snowball = player.getWorld().spawn(
									player.getEyeLocation(), Snowball.class);

							Location temp = player.getEyeLocation();
							temp.setPitch(0);
							final Location startpoint = temp;
							snowball.setShooter(player);
							snowball.setFallDistance(10F);
							snowball.setVelocity(startpoint.getDirection()
									.multiply(1.5));
							SnowballHider(snowball,
									archeck.playertoarena.get(player
											.getUniqueId()).players.keySet());
							snowball.setMetadata(
									"team",
									new FixedMetadataValue(plugin, champ
											.getTeam()));
							scheduling(snowball, startpoint, 6);
						}
					}
				} else if (player.getInventory().getHeldItemSlot() == 3
						|| player.getInventory().getHeldItemSlot() == 4) {
					if (player.getItemInHand() != null
							&& player.getItemInHand().getType() != Material.AIR
							&& player.getItemInHand().getAmount() == 1) {
						AbilityParticle ap = plugin.ap;

						Arena arena = archeck.playertoarena.get(player
								.getUniqueId());

						Ability ab = Ability
								.returnability(player.getItemInHand()
										.getItemMeta().getDisplayName());

						player.getItemInHand().setAmount(
								CalcCooldown(player.getInventory(), ab));

						cooldownt(player.getItemInHand(), player
								.getItemInHand().getAmount());

						player.getInventory().setHeldItemSlot(0);
						// different abilitie types
						switch (ab.getAttack()) {
						case "projectile": {
							Arrow ar = player.getWorld().spawn(
									player.getEyeLocation(), Arrow.class);
							Location temp = player.getEyeLocation();
							temp.setPitch(0);
							final Location startpoint = temp;
							ar.setShooter(player);
							ar.setVelocity(startpoint.getDirection().multiply(
									1.7));
							ar.setMetadata("abilitie", new FixedMetadataValue(
									plugin, ab.getName()));

							scheduling(ar, startpoint, 10);

							break;
						}
						case "explosion": {

							List<Player> players;
							if (ab.getParticle().equals("line")) {
								players = ap.linel(player, ab.getSize());
							} else {
								players = ap.sqloc(player, ab.getSize());
							}

							for (Player target : players) {

								if (!arena.players
										.get(target.getUniqueId())
										.getTeam()
										.equals(arena.players.get(
												player.getUniqueId()).getTeam())) {
									Arrow pr = player.getWorld().spawn(
											target.getLocation(), Arrow.class);
									pr.setMetadata(
											"abilitie",
											new FixedMetadataValue(plugin, ab
													.getName()));
									pr.setShooter(player);
									pr.setVelocity(target.getLocation()
											.toVector().multiply(2.9));
									scheduling(pr, target.getLocation(), 1);
								}
							}

							List<Location> alllocs;
							if (ab.getParticle().equals("line")) {
								alllocs = ap.lineloc(player, ab.getSize());
							} else {
								alllocs = ap.sqlocations(player, ab.getSize());
							}
							for (Location loc : alllocs) {
								ParticleEffect.EXPLOSION_NORMAL.display(0, 0,
										0, 0, 10, loc, 20);
							}
						}
							break;

						case "wave": {
							final List<Location> locs;
							if (ab.getParticle().equals("line")) {
								locs = ap.lineloc(player, ab.getSize());
							} else {
								locs = ap.sqlocationsw(player, ab.getSize());
							}
							waveattack(locs, 0, player, ab);
							break;
						}

						case "timed": {
							if (ab.getParticle().equals("line")) {
								Arrow ar = player.getWorld().spawn(
										player.getEyeLocation(), Arrow.class);
								Location temp = player.getEyeLocation();
								temp.setPitch(0);
								final Location startpoint = temp;
								ar.setShooter(player);
								ar.setVelocity(startpoint.getDirection()
										.multiply(1.7));
								ar.setMetadata(
										"abilitie",
										new FixedMetadataValue(plugin, ab
												.getName()));
								scheduling(ar, startpoint, 10);
							} else {
								for (Location all : ap.sqlocationsw(player,
										ab.getSize())) {

									all.setPitch(-90);
									Arrow ar = player.getWorld().spawn(all,
											Arrow.class);

									ar.setShooter(player);
									ar.setVelocity(all.getDirection().multiply(
											1.7));
									ar.setMetadata(
											"abilitie",
											new FixedMetadataValue(plugin, ab
													.getName()));
									scheduling(ar, all, 1);
								}
							}
							break;
						}

						case "wrapper": {
							List<Player> players;
							if (ab.getParticle().equals("line")) {
								players = ap.linel(player, ab.getSize());
							} else {
								players = ap.sqloc(player, ab.getSize());
							}
							for (Player victim : players) {
								if (!arena.players
										.get(victim.getUniqueId())
										.getTeam()
										.equals(arena.players.get(
												player.getUniqueId()).getTeam())) {
									victim.addPotionEffect(new PotionEffect(
											PotionEffectType.SLOW,
											(int) (3 * 20), 100));
									wrapperattack(victim, 3 * 2, ab, player);
								}
							}

							List<Location> locations;
							if (ab.getParticle().equals("line")) {
								locations = ap.lineloc(player, ab.getSize());
							} else {
								locations = ap.sqlocationsw(player,
										ab.getSize());
							}
							for (Location locs : locations) {
								ParticleEffect.EXPLOSION_NORMAL.display(0, 0,
										0, 0, 10, locs, 20);
							}
						}

						case "healexplosion": {
							List<Player> players;
							if (ab.getParticle().equals("line")) {
								players = ap.linel(player, ab.getSize());
							} else {
								players = ap.sqloc(player, ab.getSize());
							}
							for (Player victim : players) {
								if (arena.players
										.get(victim.getUniqueId())
										.getTeam()
										.equals(arena.players.get(
												player.getUniqueId()).getTeam())) {
									Arrow pr = player.getWorld().spawn(
											victim.getLocation(), Arrow.class);
									pr.setMetadata(
											"abilitie",
											new FixedMetadataValue(plugin, ab
													.getName()));
									pr.setShooter(player);
									pr.setVelocity(victim.getLocation()
											.toVector().multiply(2.9));
									scheduling(pr, victim.getLocation(), 1);
									@SuppressWarnings("deprecation")
									EntityDamageByEntityEvent e = new EntityDamageByEntityEvent(
											pr, victim,
											DamageCause.ENTITY_ATTACK, 0);
									Bukkit.getPluginManager().callEvent(e);
								}
							}

							List<Location> locations;
							if (ab.getParticle().equals("line")) {
								locations = ap.lineloc(player, ab.getSize());
							} else {
								locations = ap.sqlocationsw(player,
										ab.getSize());
							}
							for (Location locs : locations) {
								ParticleEffect.EXPLOSION_NORMAL.display(0, 0,
										0, 0, 10, locs, 20);
							}
							break;
						}

						case "healprojectile": {
							Arrow ar = player.getWorld().spawn(
									player.getEyeLocation(), Arrow.class);
							Location temp = player.getEyeLocation();
							temp.setPitch(0);
							final Location startpoint = temp;
							ar.setShooter(player);
							ar.setVelocity(startpoint.getDirection().multiply(
									1.7));
							ar.setMetadata("abilitie", new FixedMetadataValue(
									plugin, ab.getName()));
							scheduling(ar, startpoint, 10);

							break;
						}

						default:
							break;
						}

					}
				}
			}
		}
	}

	private void wrapperattack(final Player victim, final int times,
			final Ability ab, final Player attacker) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			int b = times;

			public void run() {
				Arrow pr = attacker.getWorld().spawn(victim.getLocation(),
						Arrow.class);
				pr.setMetadata("abilitie",
						new FixedMetadataValue(plugin, ab.getName()));
				pr.setShooter(attacker);
				pr.setVelocity(victim.getLocation().toVector().multiply(2.9));
				scheduling(pr, victim.getLocation(), 1);
				if (b - 1 != 0) {
					b--;
					wrapperattack(victim, b, ab, attacker);
				}
			}
		}, 10L);
	}

	private void waveattack(final List<Location> locs, final int i,
			final Player player, final Ability ab) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			int a = i;

			public void run() {
				try {
					ParticleEffect.EXPLOSION_NORMAL.display(0, 0, 0, 0, 10,
							locs.get(i), 20);
					ParticleEffect.EXPLOSION_NORMAL.display(0, 0, 0, 0, 10,
							locs.get(i + 1), 20);
					ParticleEffect.EXPLOSION_NORMAL.display(0, 0, 0, 0, 10,
							locs.get(i + 2), 20);
					for (Player target : Bukkit.getServer().getOnlinePlayers()) {
						if (target.getUniqueId() != player.getUniqueId()) {
							if (target.getLocation().distance(locs.get(i)) <= 1
									|| target.getLocation().distance(
											locs.get(i + 1)) <= 1
									|| target.getLocation().distance(
											locs.get(i + 2)) <= 1) {
								Arrow pr = player.getWorld().spawn(
										target.getLocation(), Arrow.class);
								pr.setMetadata(
										"abilitie",
										new FixedMetadataValue(plugin, ab
												.getName()));
								pr.setShooter(player);
								pr.setVelocity(target.getLocation().toVector()
										.multiply(2.9));
								scheduling(pr, target.getLocation(), 1);
								target.setVelocity(player.getEyeLocation()
										.getDirection().multiply(1.6));
							}
						}
					}
					a += 3;
					waveattack(locs, a, player, ab);
				} catch (IndexOutOfBoundsException ex) {

				}
			}
		}, 2L);
	}

	private void cooldownt(final ItemStack item, final int time) {
		Scheduler.runAsyncTask(new Runnable() {
			int t = time;

			public void run() {
				if (t != 1) {
					item.setAmount(item.getAmount() - 1);
					t--;
					cooldownt(item, t);
				}
			}
		}, 1L);

	}

	protected void scheduling(final Projectile ball, final Location startpoint,
			final int maxd) {
		Scheduler.runAsyncTask(new Runnable() {
			public void run() {
				if (ball instanceof Snowball) {
					String team = ball.getMetadata("team").get(0).asString();
					Color cl;
					if (team.equals("red")) {
						cl = Color.RED;
					} else {
						cl = Color.BLUE;
					}

					float red = cl.getRed();
					float blue = cl.getBlue();
					float green = cl.getGreen();
					ParticleEffect.SPELL_MOB.display(red / 255F, green / 255F,
							blue / 255F, 1, 0, ball.getLocation(), 20);

				}

				if (ball.getLocation().distance(startpoint) >= maxd) {
					ball.remove();
				} else {
					if (ball.isOnGround()) {
						ball.remove();
					} else {
						scheduling(ball, startpoint, maxd);
					}
				}
			}
		}, (long) 0.20);
	}

	public BasicAttack(OdinMain instance) {
		this.archeck = instance.mov;
		this.plugin = instance;
		this.hider = new EntityHider(instance, Policy.BLACKLIST);
	}

	protected static long parseDateDiff(String time, boolean future,
			long rewardedtime) {
		Pattern timePattern = Pattern
				.compile(
						"(?:([0-9]+)\\s*y[a-z]*[,\\s]*)?(?:([0-9]+)\\s*mo[a-z]*[,\\s]*)?(?:([0-9]+)\\s*w[a-z]*[,\\s]*)?(?:([0-9]+)\\s*d[a-z]*[,\\s]*)?(?:([0-9]+)\\s*h[a-z]*[,\\s]*)?(?:([0-9]+)\\s*m[a-z]*[,\\s]*)?(?:([0-9]+)\\s*(?:s[a-z]*)?)?",

						2);
		Matcher m = timePattern.matcher(time);
		int years = 0;
		int months = 0;
		int weeks = 0;
		int days = 0;
		int hours = 0;
		int minutes = 0;
		int seconds = 0;
		boolean found = false;
		while (m.find()) {
			if ((m.group() != null) && (!m.group().isEmpty())) {
				for (int i = 0; i < m.groupCount(); i++) {
					if ((m.group(i) != null) && (!m.group(i).isEmpty())) {
						found = true;
						break;
					}
				}
				if (found) {
					if ((m.group(1) != null) && (!m.group(1).isEmpty())) {
						years = Integer.parseInt(m.group(1));
					}
					if ((m.group(2) != null) && (!m.group(2).isEmpty())) {
						months = Integer.parseInt(m.group(2));
					}
					if ((m.group(3) != null) && (!m.group(3).isEmpty())) {
						weeks = Integer.parseInt(m.group(3));
					}
					if ((m.group(4) != null) && (!m.group(4).isEmpty())) {
						days = Integer.parseInt(m.group(4));
					}
					if ((m.group(5) != null) && (!m.group(5).isEmpty())) {
						hours = Integer.parseInt(m.group(5));
					}
					if ((m.group(6) != null) && (!m.group(6).isEmpty())) {
						minutes = Integer.parseInt(m.group(6));
					}
					if ((m.group(7) == null) || (m.group(7).isEmpty())) {
						break;
					}
					seconds = Integer.parseInt(m.group(7));

					break;
				}
			}
		}
		if (!found) {
			return -1L;
		}
		Calendar c = new GregorianCalendar();
		c.setTimeInMillis(rewardedtime);
		if (years > 0) {
			c.add(1, years * (future ? 1 : -1));
		}
		if (months > 0) {
			c.add(2, months * (future ? 1 : -1));
		}
		if (weeks > 0) {
			c.add(3, weeks * (future ? 1 : -1));
		}
		if (days > 0) {
			c.add(5, days * (future ? 1 : -1));
		}
		if (hours > 0) {
			c.add(11, hours * (future ? 1 : -1));
		}
		if (minutes > 0) {
			c.add(12, minutes * (future ? 1 : -1));
		}
		if (seconds > 0) {
			c.add(13, seconds * (future ? 1 : -1));
		}

		return c.getTimeInMillis();
	}

	private int CalcCooldown(Inventory invetory, Ability ab) {
		double total = 0;
		for (int i = 30; i < 36; i++) {
			if (invetory.getItem(i) != null
					&& invetory.getItem(i).getType() != Material.AIR) {
				for (String in : invetory.getItem(i).getItemMeta().getLore()) {
					if (in.contains("Cooldown")) {
						for (String t : in.split(" ")) {
							try {
								total += Double.parseDouble(t.replace("%", ""));
								break;
							} catch (NumberFormatException ex) {
								continue;
							}
						}
						break;
					}
				}
			}
		}
		int cd = ab.getCd() - (int) (ab.getCd() * (total / 100));
		return cd;

	}
}
