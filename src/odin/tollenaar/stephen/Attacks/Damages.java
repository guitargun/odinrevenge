package odin.tollenaar.stephen.Attacks;

import java.util.List;
import java.util.Random;

import odin.tollenaar.stephen.Abilities.Ability;
import odin.tollenaar.stephen.Abilities.AbilityParticle;
import odin.tollenaar.stephen.ArenaStuff.Arena;
import odin.tollenaar.stephen.ArenaStuff.Profession;
import odin.tollenaar.stephen.Items.Area;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.utils.ParticleEffect;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageModifier;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Damages extends BasicAttack {
	private OdinMain plugin;

	@EventHandler
	public void ondamage(EntityDamageByEntityEvent event) {
		Player tplayer = null;
		try{
		if (event.getEntity() instanceof Creeper) {
			tplayer = plugin.ap.sqloc(event.getEntity().getLocation(), 1)
					.get(0);
		}
		boolean pass = false;
		try {
			event.setDamage(DamageModifier.ARMOR, 0);
		} catch (UnsupportedOperationException ex) {

		}
		Player victim;
		Player attacker;
		if (event.getDamager() instanceof Player) {
			attacker = (Player) event.getDamager();
			if(plugin.mov.playertoarena.get(attacker.getUniqueId()) == null){
				return;
			}
			if (plugin.mov.playertoarena.get(attacker.getUniqueId()) != null) {
				if (plugin.mov.playertoarena.get(attacker.getUniqueId()).players
						.get(attacker.getUniqueId()).getProf() == Profession.MAGE) {
					event.setCancelled(true);
					return;
				}

			}
			if (tplayer == null) {
				victim = (Player) event.getEntity();
			} else {
				victim = tplayer;
			}

			String teama = plugin.mov.playertoarena.get(attacker.getUniqueId()).players
					.get(attacker.getUniqueId()).getTeam();

			String teamv = plugin.mov.playertoarena.get(victim.getUniqueId()).players
					.get(victim.getUniqueId()).getTeam();

			if (teama.equals(teamv)) {
				event.setCancelled(true);
				return;
			}
			if (cooldown.get(attacker.getUniqueId()) == null) {
				cooldown.put(
						attacker.getUniqueId(),
						parseDateDiff(
								archeck.playertoarena.get(attacker
										.getUniqueId()).players.get(
										attacker.getUniqueId())
										.getAttackspeed()
										+ "s", true, System.currentTimeMillis()));
			} else {
				if (cooldown.get(attacker.getUniqueId()) <= System
						.currentTimeMillis()) {
					cooldown.put(
							attacker.getUniqueId(),
							parseDateDiff(
									archeck.playertoarena.get(attacker
											.getUniqueId()).players.get(
											attacker.getUniqueId())
											.getAttackspeed()
											+ "s", true, System
											.currentTimeMillis()));
				} else {
					event.setCancelled(true);
					return;
				}
			}

		} else {
			if(!(event.getDamager() instanceof Projectile)){
				return;
			}
			if (tplayer == null) {
				victim = (Player) event.getEntity();
			} else {
				victim = tplayer;
			}
			if (event.getDamager() instanceof Projectile) {

				Projectile pro = (Projectile) event.getDamager();
				attacker = (Player) pro.getShooter();
			} else {
				attacker = null;
			}
		}
		if (attacker.getInventory().getHeldItemSlot() == 0) {
			if (plugin.mov.playertoarena.get(attacker.getUniqueId()) != null) {
				if (plugin.mov.playertoarena.get(attacker.getUniqueId()).players
						.get(attacker.getUniqueId()).getProf() == Profession.NONE) {
					plugin.mov.playertoarena.get(attacker.getUniqueId()).players
							.get(attacker.getUniqueId()).setProf(
									Profession.WARRIOR);
				}
			}
			for (Arena ar : plugin.mov.arenas.values()) {
				if (ar.players.containsKey(attacker.getUniqueId())) {
					pass = true;
					break;
				}
			}
			if (!pass) {
				return;
			}
			// crit is already calculated
			double damage = 0;

			if (event.getDamager() instanceof Projectile) {
				Projectile pr = (Projectile) event.getDamager();
				if (pr.hasMetadata("abilitie")) {
					Ability ab = Ability.returnability(pr
							.getMetadata("abilitie").get(0).asString());

					if (!ab.getAttack().equals("timed")
							|| (ab.getAttack().equals("timed") && pr
									.hasMetadata("fired"))) {
						if (!ab.getAttack().equals("healexplosion")) {
							damage = ab.getDam();
							if (damage < 0) {
								damage = -damage;
								Damageable dm = attacker;
								if (dm.getHealth() + damage < dm.getMaxHealth()) {
									dm.setHealth(dm.getHealth() + damage);
								} else {
									while (dm.getHealth() + damage > dm
											.getMaxHealth()) {
										damage--;
									}
									dm.setHealth(dm.getHealth() + damage);
									damage = -ab.getDam();
								}
							}
						} else {
							Damageable dm = victim;
							double i = -ab.getDam();
							if (dm.getHealth() + i > dm.getMaxHealth()) {
								while (dm.getHealth() + i > dm.getMaxHealth()) {
									i--;
								}
							}
							dm.setHealth(dm.getHealth() + i);
						}
						if (ab.getPassive() != null) {
							abilitypassive(victim, ab, attacker);
						}
					} else {
						countdowntimed(victim, 4, 1, attacker, ab);
					}
				} else if (pr.hasMetadata("aura")) {
					Area ar = Area.returnAura(pr.getMetadata("aura").get(0)
							.asString());
					for (String modi : ar.getModifier()) {
						if (modi.contains("Damage")) {
							for (String in : modi.split(" ")) {
								if (in.contains("+")) {
									try {
										damage += Integer.parseInt(in.replace(
												"+", ""));
										break;
									} catch (NumberFormatException ex) {
										continue;
									}
								}
							}
						}
					}
				} else if (pr.hasMetadata("area")) {
					Area ar = Area.returnAura(pr.getMetadata("area").get(0)
							.asString());
					for (String modi : ar.getModifier()) {
						if (modi.contains("Damage")) {
							for (String in : modi.split(" ")) {
								if (in.contains("+")) {
									try {
										damage += Integer.parseInt(in.replace(
												"+", ""));
										break;
									} catch (NumberFormatException ex) {
										continue;
									}
								}
							}
						}
					}
				}
				final Player playertemp = victim;
				if (event.getDamager() instanceof Arrow) {
					Bukkit.getScheduler().scheduleSyncDelayedTask(plugin,
							new Runnable() {

								public void run() {
									((CraftPlayer) playertemp.getPlayer())
											.getHandle().getDataWatcher()
											.watch(9, (byte) 0);
								}
							}, 2L);

				}
			}
			double reduction = 0;
			double truedam = 0;
			Damageable dam = attacker;
			double totallife = 0;
			for (int i = 30; i < 36; i++) {
				if (attacker.getInventory().getItem(i) != null) {
					ItemStack item = attacker.getInventory().getItem(i);
					/**
					 * passive handler currently crit to add the damage extra
					 * with the amount of stacks
					 */
					for (String in : item.getItemMeta().getLore()) {
						if (in.contains("Passive")) {
							// missed crit handler

							// passive stacked damage handler
							if (in.contains("damage") && !in.contains("true")) {
								int amout = item.getAmount();
								int multiplier = 0;
								for (String in2 : in.split(" ")) {
									if (in2.contains("+")) {
										try {
											multiplier = Integer.parseInt(in
													.replace("+", ""));
										} catch (NumberFormatException ex) {
											break;
										}
									}
								}
								damage += amout * multiplier;
							}
							if (in.contains("true")) {
								for (String in2 : in.split(" ")) {
									if (in2.contains("+")) {
										in2 = in2.replace("+", "");
										try {
											truedam += Integer.parseInt(in2);
										} catch (NumberFormatException ex) {
											continue;
										}
										break;
									}
								}
							}
							// pasive lifesteal handler
							if (in.contains("lifesteal")) {
								if (in.contains("stack")) {
									if (dam.getHealth() != dam.getMaxHealth()) {
										totallife += item.getAmount() / 4;
									}
								} else {
									for (String in2 : in.split(" ")) {
										if (in2.contains("%")) {
											in2 = in2.replace("%", "");
											try {
												totallife += Integer
														.parseInt(in2);
												break;
											} catch (NumberFormatException ex) {
												continue;
											}

										}
									}
								}
							}
						} else
						/**
						 * lifesteal handler non passive
						 */
						if (in.contains("lifesteal")) {

							// getting total lifesteal
							if (dam.getHealth() != dam.getMaxHealth()) {
								for (String in2 : in.split(" ")) {
									if (in2.contains("%")) {
										in2 = in2.replace("%", "");
										try {
											totallife += Integer.parseInt(in2);
											break;
										} catch (NumberFormatException ex) {
											continue;
										}
									}
								}
							}

						} else
						/**
						 * next is for the general power non passive
						 */
						if (in.contains("Damage") && !in.contains("Area")
								&& !in.contains("true")) {
							for (String in2 : in.split(" ")) {
								if (in2.contains("+")) {
									in2 = in2.replace("+", "");
									try {
										damage += Integer.parseInt(in2);
									} catch (NumberFormatException ex) {
										continue;
									}
									break;
								}
							}

							/**
							 * to add area damage?!
							 */
						} else if (in.contains("true")) {
							for (String in2 : in.split(" ")) {
								if (in2.contains("+")) {
									in2 = in2.replace("+", "");
									try {
										truedam += Integer.parseInt(in2);
									} catch (NumberFormatException ex) {
										continue;
									}
									break;
								}
							}
						} else if (in.contains("Area") && in.contains("Damage")) {
							if (event.getDamager() instanceof Projectile
									&& event.getDamager().hasMetadata("area")) {
								continue;
							}
							if (Area.returnAura(item.getItemMeta()
									.getDisplayName()) != null) {
								Area ar = Area.returnAura(item.getItemMeta()
										.getDisplayName());
								List<Player> inarea = plugin.ap.sqloc(
										victim.getLocation(), ar.getRange());
								for (Player players : inarea) {
									if (players.getName() != victim.getName()
											&& !plugin.mov.playertoarena
													.get(players.getUniqueId()).players
													.get(players.getUniqueId())
													.equals(plugin.mov.playertoarena.get(attacker
															.getUniqueId()).players.get(attacker
															.getUniqueId()))) {
										Arrow pr = attacker.getWorld().spawn(
												players.getLocation(),
												Arrow.class);
										pr.setMetadata("area",
												new FixedMetadataValue(plugin,
														ar.getName()));
										pr.setShooter(attacker);
										pr.setVelocity(players.getLocation()
												.toVector().multiply(2.9));
									}
								}
							}
						}
					}
				}
			}

			/**
			 * next is for all of the protection things the victim has. this
			 * include making the damage less than it already is. this wil only
			 * mean that the crit, area damage and the normal damage are being
			 * lessed the life still will remain the same however this is only
			 * for the area damage.!!
			 */
			/**
			 * normal .setdamage is before the armor calculation
			 */
			for (int i = 30; i < 36; i++) {
				if (victim.getInventory().getItem(i) != null) {
					ItemStack item = victim.getInventory().getItem(i);
					for (String in : item.getItemMeta().getLore()) {
						if (in.contains("Protect") || in.contains("protect")) {
							for (String in2 : in.split(" ")) {
								if (in2.contains("+")) {
									for (String in3 : in2.split("")) {
										try {
											if (in.contains("Passive")) {
												reduction += item.getAmount()
														* Double.parseDouble(in3);
											} else {
												reduction += Double
														.parseDouble(in3);
											}
										} catch (NumberFormatException ex) {
											continue;
										}
									}
								}
							}
						}
					}
				}
			}
			reduction = 100 / (100 + (Math.ceil(reduction / 2.0) * 10));
			if (reduction == 1) {
				reduction = 0;
			}
			reduction = -reduction;
			/**
			 * 
			 * to add true damage true damage has to deal with the armor
			 * reduction. so this is after the protecting things
			 */

			try {
				event.setDamage(DamageModifier.ARMOR, reduction);
			} catch (UnsupportedOperationException ex) {

			}
			if (damage == 0) {
				damage = 1;
			}
			Arena ar = plugin.mov.playertoarena.get(attacker.getUniqueId());
			
			if(ar.players.get(attacker.getUniqueId()).getProf().typeattack().equals(ar.players.get(victim.getUniqueId()).getProf().typeattack())){
				event.setDamage(critcalc(attacker, damage));
			}else{
				if(ar.players.get(victim.getUniqueId()).getProf().typeattack().equals("magical")){
					event.setDamage(critcalc(attacker, damage)*ar.players.get(victim.getUniqueId()).getProf().physpen());
				}else{
					event.setDamage(critcalc(attacker, damage)*ar.players.get(victim.getUniqueId()).getProf().magicpen());
				}
			}
			
			

			/**
			 * giving the lifesteal
			 */
			if (totallife != 0 && dam.getHealth() != dam.getMaxHealth()) {
				if (totallife > 60) {
					totallife = 60;
				}

				double healthb = Math.ceil((damage / totallife) / 0.4) * 0.4;
				if (healthb > 0 && healthb < attacker.getMaxHealth()) {
					attacker.setHealth(dam.getHealth() + healthb);
				}
			}

			/**
			 * check if the attack was crit for the missed crit passive
			 */

			for (int i = 30; i < 36; i++) {
				if (attacker.getInventory().getItem(i) != null) {
					ItemStack item = attacker.getInventory().getItem(i);
					for (String in : item.getItemMeta().getLore()) {
						if (in.contains("Passive")) {
							// missed crit handler
							if (in.contains("missed crit")) {
								int amount = item.getAmount();
								if (amount >= 1 && damage < event.getDamage()) {
									item.setAmount(1);
								} else if (amount < 4) {
									item.setAmount(amount + 1);
								}
								break;
							}
						}
					}
				}
			}
			if (truedam != 0) {
				Damageable v = victim;
				if (v.getHealth() - truedam >= 0) {
					victim.setHealth(v.getHealth() - truedam);
				} else {
					while (v.getHealth() - truedam < 0) {
						truedam--;
					}
					victim.setHealth(v.getHealth() - truedam);
				}
			}

		}
		}catch(Exception ex){
			return;
		}

	}

	private double critcalc(Player player, double damage) {
		double crit = damage;
		int chance = 0;
		for (int i = 30; i < 36; i++) {
			if (player.getInventory().getItem(i) != null) {
				ItemStack item = player.getInventory().getItem(i);
				for (String in : item.getItemMeta().getLore()) {
					if (in.contains("Crit")) {
						String[] inlist = in.split(" ");
						for (String k : inlist) {
							if (k.contains("%")) {
								String[] finalin = k.split("");
								for (String finalink : finalin) {
									try {
										chance += Integer.parseInt(finalink);
										break;
									} catch (NumberFormatException ex) {
										continue;
									}
								}
								break;
							}
						}
						break;
					} else if (in.contains("crit") && in.contains("missed")) {
						chance += item.getAmount() * 5;
						break;
					}
				}
			}
		}
		Random r = new Random();
		int index = r.nextInt(100);
		if (index <= chance) {
			crit = damage * 2;
		}
		return crit;
	}

	private void countdowntimed(final Player victim, final int timed,
			final int times, final Player shooter, final Ability ab) {
		Bukkit.getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			int a = timed;
			int b = times;

			public void run() {
				if (a - 1 != 0) {
					ParticleEffect.HEART.display(0, 0, 0, 0, b,
							victim.getEyeLocation(), 20);
					a--;
					b++;
					countdowntimed(victim, a, b, shooter, ab);
				} else {
					AbilityParticle ap = plugin.ap;
					List<Player> players = ap.sqloc(victim, 2);
					if (!players.contains(victim)) {
						players.add(victim);
					}
					for (Player player : players) {
						if (player.getUniqueId() != shooter.getUniqueId()) {
							Arrow pr = shooter.getWorld().spawn(
									player.getLocation(), Arrow.class);
							pr.setMetadata("name", new FixedMetadataValue(
									plugin, ab.getName()));
							pr.setMetadata("fired", new FixedMetadataValue(
									plugin, true));
							pr.setShooter(shooter);
							pr.setVelocity(player.getLocation().toVector()
									.multiply(2.9));
							scheduling(pr, player.getLocation(), 1);
						}
					}
				}
			}
		}, (20 / times));
	}

	private void abilitypassive(Player victim, Ability ab, Player attacker) {
		String passive = ab.getPassive();
		if (passive.contains("enemies")) {
			if (passive.contains("slowed")) {
				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				victim.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,
						(int) (timed * 20), 1));
			}
			if (passive.contains("weakened")) {

				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				victim.addPotionEffect(new PotionEffect(
						PotionEffectType.WEAKNESS, (int) (timed * 20), 1));
			}
			if (passive.contains("blinded")) {

				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				victim.addPotionEffect(new PotionEffect(
						PotionEffectType.BLINDNESS, (int) (timed * 20), 3));
			}
			if (passive.contains("paralyzed")) {
				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				victim.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,
						(int) (timed * 20), 100));
			}
		}
		if (passive.contains("grants")) {
			if (passive.contains("regen")) {
				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				attacker.addPotionEffect(new PotionEffect(
						PotionEffectType.REGENERATION, (int) (timed * 20), 2));
			}
			if (passive.contains("resistence")) {
				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				attacker.addPotionEffect(new PotionEffect(
						PotionEffectType.DAMAGE_RESISTANCE, (int) (timed * 20),
						3));
			}
			if (passive.contains("movementspeed")) {
				double timed = 0;
				for (String in : passive.split(" ")) {
					try {
						timed = Double.parseDouble(in);
					} catch (NumberFormatException ex) {
						continue;
					}
				}
				attacker.addPotionEffect(new PotionEffect(
						PotionEffectType.SPEED, (int) (timed * 20), 1));
			}

		}
	}

	public Damages(OdinMain instance) {
		super(instance);
		this.plugin = instance;
	}
}
