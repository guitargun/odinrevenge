package odin.tollenaar.stephen.Abilities;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import odin.tollenaar.stephen.OdinsRevenge.OdinMain;
import odin.tollenaar.stephen.utils.ParticleEffect;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.BlockIterator;

public class AbilityParticle {
	private OdinMain plugin;

	public AbilityParticle(OdinMain instance) {
		this.plugin = instance;
	}

	public void drawline(final Player player, final int size, final Color cl,
			final int current) {
		new BukkitRunnable() {

			@Override
			public void run() {
				if ((player.getItemInHand() != null && player.getItemInHand()
						.getType() != Material.AIR)
						&& (player.getInventory().getHeldItemSlot() == 4 || player
								.getInventory().getHeldItemSlot() == 3)
						&& player.getInventory().getHeldItemSlot() == current) {
					// this is for the colors

					float red = cl.getRed();
					float blue = cl.getBlue();
					float green = cl.getGreen();

					// loc.getBlock().getRelative()
					Location location = player.getLocation();
					location.setPitch(0);
					BlockIterator blocksToAdd = new BlockIterator(location, 0,
							size);
					Location blockToAdd;
					ArrayList<Player> pl = new ArrayList<Player>();
					pl.add(player);
					ArrayList<Location> alllocs = new ArrayList<Location>();
					while (blocksToAdd.hasNext()) {
						blockToAdd = blocksToAdd.next().getLocation();
						alllocs.add(blockToAdd);

						List<Location> neighbours = buildCircle(blockToAdd, 2,
								3);

						for (Location l : neighbours) {

							if (l.distance(location) < 10) {
								l.setY(location.getY());
								if (!alllocs.contains(l)) {
									alllocs.add(l);
								}
							}

						}
					}
					for (Location l : alllocs) {
						ParticleEffect.SPELL_MOB.display(red / 255F,
								green / 255F, blue / 255F, 1, 0, l, pl);
					}
					drawline(player, size, cl, current);
				}
			}
		}.runTaskLater(plugin, 5L);

	}

	public void drawareasq(final Player player, final int size, final Color cl,
			final int current) {
		new BukkitRunnable() {

			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				if ((player.getItemInHand() != null && player.getItemInHand()
						.getType() != Material.AIR)
						&& (player.getInventory().getHeldItemSlot() == 4 || player
								.getInventory().getHeldItemSlot() == 3)
						&& player.getInventory().getHeldItemSlot() == current) {
					Location playerlookloc = player.getTargetBlock((HashSet<Byte>) null, 10)
							.getLocation();
					playerlookloc.setY(player.getLocation().getY());
					List<Location> allinsloc = buildCircle(playerlookloc, size,
							50);
					ArrayList<Player> pl = new ArrayList<Player>();
					pl.add(player);

					float red = cl.getRed();
					float blue = cl.getBlue();
					float green = cl.getGreen();

					for (Location loc : allinsloc) {
						loc.setY(player.getLocation().getY());
						ParticleEffect.SPELL_MOB.display(red / 255F,
								green / 255F, blue / 255F, 1, 0, loc, pl);
					}
					drawareasq(player, size, cl, current);
				}
			}
		}.runTaskLater(plugin, 5L);
	}

	public List<Player> linel(Player player, int size) {
		List<Player> players = new ArrayList<Player>();

		Location location = player.getLocation();
		location.setPitch(0);
		BlockIterator blocksToAdd = new BlockIterator(location, 0, size);
		Location blockToAdd;
		ArrayList<Player> pl = new ArrayList<Player>();
		pl.add(player);
		ArrayList<Location> alllocs = new ArrayList<Location>();
		while (blocksToAdd.hasNext()) {
			blockToAdd = blocksToAdd.next().getLocation();
			alllocs.add(blockToAdd);

			List<Location> neighbours = buildCircle(blockToAdd, 2, 3);

			for (Location l : neighbours) {

				if (l.distance(location) < 10) {
					l.setY(location.getY());
					if (!alllocs.contains(l)) {
						alllocs.add(l);
					}
				}

			}

		}
		for (Location locs : alllocs) {
			for (Player ps : Bukkit.getOnlinePlayers()) {
				if (ps.getLocation().distance(locs) <= 1) {
					players.add(ps);
				}
			}
		}

		return players;
	}

	@SuppressWarnings("deprecation")
	public List<Player> sqloc(Player player, int size) {
		Location playerlookloc = player.getTargetBlock((HashSet<Byte>) null, 10).getLocation();
		playerlookloc.setY(player.getLocation().getY());
		List<Player> players = new ArrayList<Player>();
		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (pl.getLocation().distance(playerlookloc) <= size) {
				players.add(pl);
			}
		}

		return players;
	}

	public List<Player> sqloc(Location loc, int size) {
		List<Player> players = new ArrayList<Player>();
		for (Player pl : Bukkit.getOnlinePlayers()) {
			if (pl.getLocation().distance(loc) <= size) {
				players.add(pl);
			}
		}

		return players;
	}

	public List<Location> sqlocations(Player player, int size) {
		@SuppressWarnings("deprecation")
		Location playerlookloc = player.getTargetBlock((HashSet<Byte>) null, 10).getLocation();
		playerlookloc.setY(player.getLocation().getY());
		List<Location> allinsloc = buildCircle(playerlookloc, size, 50);

		return allinsloc;

	}

	public List<Location> sqlocationsw(Player player, int size) {
		List<Location> allinsloc = buildspiral(player.getLocation(), size, 50);
		return allinsloc;

	}

	public List<Location> lineloc(Player player, int size) {
		Location location = player.getLocation();
		location.setPitch(0);
		BlockIterator blocksToAdd = new BlockIterator(location, 0, size);
		Location blockToAdd;
		ArrayList<Player> pl = new ArrayList<Player>();
		pl.add(player);
		ArrayList<Location> alllocs = new ArrayList<Location>();
		while (blocksToAdd.hasNext()) {
			blockToAdd = blocksToAdd.next().getLocation();
			alllocs.add(blockToAdd);

			List<Location> neighbours = buildCircle(blockToAdd, 2, 3);

			for (Location l : neighbours) {

				if (l.distance(location) < 10) {
					l.setY(location.getY());
					if (!alllocs.contains(l)) {
						alllocs.add(l);
					}
				}

			}

		}
		return alllocs;
	}

	private List<Location> buildCircle(Location center, int radius, int amount) {
		World world = center.getWorld();
		double increment = (2 * Math.PI) / amount;
		ArrayList<Location> locations = new ArrayList<Location>();
		for (int i = 0; i < amount; i++) {
			double angle = i * increment;
			double x = center.getX() + (radius * Math.cos(angle));
			double z = center.getZ() + (radius * Math.sin(angle));
			locations.add(new Location(world, x, center.getY(), z));
		}
		return locations;

	}

	private List<Location> buildspiral(Location center, int radius, int amount) {
		World world = center.getWorld();
		double increment = (2 * Math.PI) / amount;
		ArrayList<Location> locations = new ArrayList<Location>();
		for (int r = 1; r < radius; r++) {
			for (int i = 0; i < amount; i++) {
				double angle = i * increment;
				double x = center.getX() + (r * Math.cos(angle));
				double z = center.getZ() + (r * Math.sin(angle));
				locations.add(new Location(world, x, center.getY(), z));
			}
		}
		return locations;
	}
}
