package odin.tollenaar.stephen.Abilities;

import java.awt.Color;
import java.util.HashSet;
import java.util.Set;

public class Ability {
	/**
	 * project is a simple 1 line arrow that shoots
	 * explosion is a explosion in that area
	 * wave is a pushing back ability that damages the players
	 * timed is a attack that has on hit a later damage, this damages players in a area
	 * wrapper paralyzes players for 3 seconds and damages them each half of second
	 * 
	 * 
	 * healexplosion heals allies and yourself if in area
	 * healprojectile heals yourself when hitting an enemy
	 */

	
	private static Set<Ability> abilities = new HashSet<Ability>();
	
	final private String particle;
	final private String attack;
	final private int size;
	final private int cd;
	final private double dam;
	final private int costs;
	final private Color color;
	final private String passive;
	final private String def;
	final private String name;
	
	public Ability(String name, String particleType, String attackType, int size, int cooldown, double damage, int price, Color color, String passive, String def){
		this.particle = particleType;
		this.attack = attackType;
		this.size = size;
		this.cd = cooldown;
		this.dam = damage;
		this.costs = price;
		this.color = color;
		this.passive = passive;
		this.def = def;
		this.name = name;
		
		
		boolean save = true;
		for(Ability in : abilities){
			if(in.getName().equals(name)){
				save = false;
				break;
			}
		}
		if(save){
			abilities.add(this);
		}
	}

	public static void Reset(){
		abilities.clear();
	}
	
	public String getParticle() {
		return particle;
	}

	public String getAttack() {
		return attack;
	}

	public int getSize() {
		return size;
	}

	public int getCd() {
		return cd;
	}

	public double getDam() {
		return dam;
	}

	public double getCosts() {
		return costs;
	}
	
	public String getName(){
		return name;
	}
	
	public static Ability returnability(String name){
		for(Ability ab : values()){
			if(ab.getName().equals(name)){
				return ab;
			}
		}
		return null;
	}

	public Color getColor() {
		return color;
	}

	public String getPassive() {
		return passive;
	}

	public String getDef() {
		return def;
	}
	
	public static Ability[] values(){
		Ability[] temp = new Ability[abilities.size()];
		for(int i = 0; i < abilities.size(); i++){
			int x = 0;
			for(Ability in : abilities){
				if(x == i){
					temp[i] = in;
					break;
				}else{
					x++;
				}
			}
		}
		return temp;
	}
}
