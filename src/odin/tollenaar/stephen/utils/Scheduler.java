package odin.tollenaar.stephen.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Scheduler {
	private static final List<ScheduledFuture<?>> executives = new ArrayList<>();
	private static ScheduledExecutorService es;

	private Scheduler() {
	}

	/**
	 * Runs a repeating asynchronous task
	 * 
	 * @since 0.1.0
	 * @version 0.1.0
	 * @author 1Rogue
	 * @param r
	 *            The runnable to execute
	 * @param startAfter
	 *            Time (in seconds) to wait before execution
	 * @param delay
	 *            Time (in seconds) between execution to wait
	 * @return The scheduled Task
	 */
	public static ScheduledFuture<?> runAsyncTaskRepeat(Runnable r,
			long startAfter, long delay) {
		ScheduledFuture<?> sch = Scheduler.getService().scheduleWithFixedDelay(
				r, startAfter, delay, TimeUnit.SECONDS);
		Scheduler.executives.add(sch);
		return sch;
	}

	/**
	 * Runs a single asynchronous task
	 * 
	 * @since 0.1.0
	 * @version 0.1.0
	 * 
	 * @param r
	 *            The runnable to execute
	 * @param delay
	 *            Time (in seconds) to wait before execution
	 * @return The scheduled Task
	 */
	public static ScheduledFuture<?> runAsyncTask(Runnable r, long delay) {
		ScheduledFuture<?> sch = Scheduler.getService().schedule(r, delay,
				TimeUnit.SECONDS);
		Scheduler.executives.add(sch);
		return sch;
	}

	/**
	 * Runs a Callable
	 * 
	 * @since 0.1.0
	 * @version 0.1.0
	 * 
	 * @param c
	 *            The callable to execute
	 * @param delay
	 *            Time (in seconds) to wait before execution
	 * @return The scheduled Task
	 */
	public static ScheduledFuture<?> runCallable(Callable<?> c, long delay) {
		ScheduledFuture<?> sch = Scheduler.getService().schedule(c, delay,
				TimeUnit.SECONDS);
		Scheduler.executives.add(sch);
		return sch;
	}

	/**
	 * Cancels all running tasks/threads and clears the cached queue.
	 * 
	 * @since 0.1.0
	 * @version 0.1.0
	 */
	public static void cancelAllTasks() {
		for (ScheduledFuture<?> s : executives) {
			if (s != null) {
				s.cancel(true);
			}
		}
		Scheduler.executives.clear();
		try {
			Scheduler.getService().awaitTermination(2, TimeUnit.SECONDS);
		} catch (InterruptedException ex) {
			System.out.println("Error while canceling task");
		}
	}

	/**
	 * Returns the underlying {@link ScheduledExecutorService} used for this
	 * utility class
	 * 
	 * @since 0.1.0
	 * @version 0.1.0
	 * 
	 * @return The underlying {@link ScheduledExecutorService}
	 */
	public static ScheduledExecutorService getService() {
		if (Scheduler.es == null || Scheduler.es.isShutdown()) {
			Scheduler.es = Executors.newScheduledThreadPool(10); // Going to
																	// find an
																	// expanding
																	// solution
																	// to this
																	// soon
		}
		return Scheduler.es;
	}
}
