package odin.tollenaar.stephen.utils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import odin.tollenaar.stephen.OdinsRevenge.OdinMain;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

public class FileWriter {
	private File inventories;
	private File signs;
	private File holos;
	private OdinMain plugin;

	public void checkfolder() {
		inventories = new File(plugin.getDataFolder(), "storedinvs");
		if (!inventories.exists()) {
			inventories.mkdir();
		}

		signs = new File(plugin.getDataFolder(), "signstores");
		if (!signs.exists()) {
			signs.mkdir();
		}

		holos = new File(plugin.getDataFolder(), "holostores");
		if (!holos.exists()) {
			holos.mkdir();
		}
	}

	public void storesign(Sign sign) {
		File file = new File(signs, sign + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		FileConfiguration config = YamlConfiguration.loadConfiguration(file);

		config.set("x", sign.getBlock().getLocation().getX());
		config.set("y", sign.getBlock().getLocation().getY());
		config.set("z", sign.getBlock().getLocation().getZ());
		config.set("World", sign.getBlock().getLocation().getWorld().getName());

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void storeholo(Hologram holo, int id) {
		File file = new File(holos, id + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		Location l = holo.getLocation();
		l.setY(l.getY()-2);
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		config.set("x", l.getX());
		config.set("y", l.getY());
		config.set("z", l.getZ());
		config.set("world", l.getWorld().getName());

		try {
			config.save(file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadholo() {
		for (File files : holos.listFiles()) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(files);
			Location l = new Location(
					Bukkit.getWorld(config.getString("world")),
					config.getDouble("x"), config.getDouble("y"),
					config.getDouble("z"));
			Hologram holo = HologramsAPI.createHologram(plugin, l);
			plugin.database.holofill(holo);

		}
		plugin.info.repeating();
	}

	public File[] returnsings() {
		return signs.listFiles();
	}

	public void storeinv(UUID playeruuid, Inventory inv) {
		File filer = new File(inventories, playeruuid.toString() + ".yml");
		if (!filer.exists()) {
			try {
				filer.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		FileConfiguration config = YamlConfiguration.loadConfiguration(filer);
		int i = 0;
		for (ItemStack content : inv.getContents()) {
			config.set(Integer.toString(i), content);
			i++;
		}
		Player player = Bukkit.getPlayer(playeruuid);
		for (char x = 'a'; x < 'e'; x++) {
			switch (x) {
			case 'a':
				config.set(Character.toString(x), player.getInventory()
						.getHelmet());
				break;
			case 'b':
				config.set(Character.toString(x), player.getInventory()
						.getChestplate());
				break;
			case 'c':
				config.set(Character.toString(x), player.getInventory()
						.getLeggings());
				break;
			case 'd':
				config.set(Character.toString(x), player.getInventory()
						.getBoots());
				break;
			}
		}
		config.set("xp", player.getExp());
		config.set("lvl", player.getLevel());
		try {
			config.save(filer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void loadinv(Player player) {
		File filer = new File(inventories, player.getUniqueId().toString()
				+ ".yml");
		FileConfiguration config = YamlConfiguration.loadConfiguration(filer);
		if (filer.exists()) {
			for (String s : config.getKeys(false)) {
				try {
					player.getInventory().setItem(Integer.parseInt(s),
							config.getItemStack(s));
				} catch (NumberFormatException ex) {
					switch (s) {
					case "a":
						player.getInventory().setHelmet(config.getItemStack(s));
						break;
					case "b":
						player.getInventory().setChestplate(
								config.getItemStack(s));
						break;
					case "c":
						player.getInventory().setLeggings(
								config.getItemStack(s));
						break;
					case "d":
						player.getInventory().setBoots(config.getItemStack(s));
						break;
					case "xp":
						player.setExp(Float.parseFloat(config.getString(s)));
						break;
					case "lvl":
						player.setLevel(config.getInt(s));
					}
				}
			}

		}

		filer.delete();
	}

	public FileWriter(OdinMain instance) {
		this.plugin = instance;
		checkfolder();
	}
}
