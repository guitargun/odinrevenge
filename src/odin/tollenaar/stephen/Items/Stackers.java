package odin.tollenaar.stephen.Items;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;

public class Stackers {
	

	
	private static Set<Stackers> items=  new HashSet<Stackers>();
	
	final private String name;
	final private Material type;
	final private String passive;
	final private List<String> modifier;
	final private int cost;
	private Stackers prereq;
	private final String def;
	
	public Stackers(String name, Material type, String passive, List<String> modifiers, int cost, Stackers prereq, String def){
		this.name = name;
		this.type = type;
		this.passive = passive;
		this.modifier = modifiers;
		this.cost = cost;
		this.prereq = prereq;
		this.def  = def;
		
		boolean save = true;
		for(Stackers in : items){
			if(in.getName().equals(name)){
				save = false;
				break;
		}
		}
		if(save){
			items.add(this);
		}
		
	}
	
	public void setPrereq(Stackers prereq){
		this.prereq = prereq;
	}
	
	public static Stackers returnstacker(String name){
		for(Stackers stacker : values()){
			if(stacker.getName().equals(name)){
				return stacker;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public Material getType() {
		return type;
	}

	public String getPassive() {
		return passive;
	}

	public List<String> getModifier() {
		return modifier;
	}

	public int getCost() {
		return cost;
	}

	public Stackers getPrereq() {
		return prereq;
	}
	
	public boolean hasPrereq(){
		if(prereq != null){
			return true;
		}else{
			return false;
		}
	}
	
	public static Stackers[] values(){
		Stackers[] temp = new Stackers[items.size()];
		for(int i = 0; i < items.size(); i++){
			int x = 0;
			for(Stackers in : items){
				if(x == i){
					temp[i] = in;
					break;
				}else{
					x++;
				}
			}
		}
		return temp;
	}
	
	public static void Reset(){
		items.clear();
	}

	public String getDef() {
		return def;
	}
}
