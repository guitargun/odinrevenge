package odin.tollenaar.stephen.Items;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.potion.PotionEffectType;


public class Potions {
	
	
	
	private static Set<Potions> items= new HashSet<Potions>();
	
	private final String name;
	private final int duration;
	private final int modifier;
	private final int costs;
	private final PotionEffectType type;
	private final String def;
	
	public Potions(String name, int duration, int modifier, int cost, PotionEffectType type, String def){
		this.name = name;
		this.duration = duration;
		this.modifier = modifier;
		this.costs = cost;
		this.type = type;
		this.def = def;
		
		boolean save = true;
		for(Potions in : items){
			if(in.getName().equals(name)){
				save = false;
			}
		}
		if(save){
			items.add(this);
		}
	}


	public String getName() {
		return name;
	}


	public int getDuration() {
		return duration;
	}


	public int getModifier() {
		return modifier;
	}
	
	public int getCosts() {
		return costs;
	}


	public PotionEffectType getType() {
		return type;
	}


	public static Potions returnpotion(String name){
		for(Potions pot : values()){
			if(pot.getName().equals(name)){
				return pot;
			}
		}
		return null;
	}

	public static Potions[] values(){
		Potions[] temp = new Potions[items.size()];
		for(int i = 0; i < items.size(); i++){
			int x = 0;
			for(Potions item : items){
				if(i == x){
					temp[i] = item;
					break;
				}else{
					x++;
				}
			}
		}
		return temp;
	}
	
	public static void Reset(){
		items.clear();
	}
	
	public String getDef() {
		return def;
	}
	
}
