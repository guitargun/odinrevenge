package odin.tollenaar.stephen.Items;

import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import odin.tollenaar.stephen.Abilities.Ability;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;

import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.potion.PotionEffectType;

public class ItemWriters extends DefaultItems {
	private OdinMain plugin;
	private File mainFolder;

	private File ArmorFolder;
	private File ActivesFolder;
	private File StackersFolder;
	private File WeaponsFolder;
	private File CrittersFolder;
	private File AreaFolder;
	private File PotionFolder;
	private File AbilitieFolder;
	
	
	public ItemWriters(OdinMain instance) {
		this.plugin = instance;

		this.mainFolder = new File(plugin.getDataFolder(), "ItemsStorage");
		if (!mainFolder.exists()) {
			mainFolder.mkdir();
		}

		this.ArmorFolder = new File(mainFolder, "ArmorItems");
		if (!ArmorFolder.exists()) {
			ArmorFolder.mkdir();
			DefaultArmor();
			for(Armor item : Armor.values()){
				SaveArmor(item);
			}
		}

		this.ActivesFolder = new File(mainFolder, "ActiveItems");
		if (!ActivesFolder.exists()) {
			ActivesFolder.mkdir();
			DefaultActive();
			for(Actives item : Actives.values()){
				SaveActives(item);
			}
		}

		this.StackersFolder = new File(mainFolder, "StackingItems");
		if (!StackersFolder.exists()) {
			StackersFolder.mkdir();
			DefaultStackers();
			for(Stackers item : Stackers.values()){
				SaveStackers(item);
			}
		}

		this.AreaFolder = new File(mainFolder, "AreaItems");
		if (!AreaFolder.exists()) {
			AreaFolder.mkdir();
			DefaultAreas();
			for(Area item : Area.values()){
				SaveArea(item);
			}
		}

		this.CrittersFolder = new File(mainFolder, "CritterItems");
		if (!CrittersFolder.exists()) {
			CrittersFolder.mkdir();
			DefaultCritters();
			for(Critters item : Critters.values()){
				SaveCritters(item);
			}
		}
		this.WeaponsFolder = new File(mainFolder, "WeaponItems");
		if (!WeaponsFolder.exists()) {
			WeaponsFolder.mkdir();
			DefaultWeapons();
			for(Weapons item : Weapons.values()){
				SaveWeapons(item);
			}
		}
		this.PotionFolder = new File(mainFolder, "PotionItems");
		if (!PotionFolder.exists()) {
			PotionFolder.mkdir();
			DefaultPotions();
			for(Potions item : Potions.values()){
				SavePotions(item);
			}
		}
		this.AbilitieFolder = new File(mainFolder, "Abilities");
		if(!AbilitieFolder.exists()){
			AbilitieFolder.mkdir();
			DefaultAbilities();
			for(Ability item : Ability.values()){
				SaveAbilities(item);
			}
		}
		
		LoadAll();
	}

	public void SaveItems() {
		for (Armor armor : Armor.values()) {
			SaveArmor(armor);
		}
		for (Critters critter : Critters.values()) {
			SaveCritters(critter);
		}
		for (Stackers item : Stackers.values()) {
			SaveStackers(item);
		}
		for (Weapons item : Weapons.values()) {
			SaveWeapons(item);
		}
		for (Area item : Area.values()) {
			SaveArea(item);
		}
		for (Actives item : Actives.values()) {
			SaveActives(item);
		}
		for (Potions item : Potions.values()) {
			SavePotions(item);
		}
		for(Ability item : Ability.values()){
			SaveAbilities(item);
		}

	}

	private void SaveAbilities(Ability item){
		File file = new File(AbilitieFolder, item.getName() + ".yml");
		if(!file.exists()){
			try {
				file.createNewFile();
				FileConfiguration config = YamlConfiguration.loadConfiguration(file);
				
				config.set("Name", item.getName());
				config.set("Particle", item.getParticle());
				config.set("Attack", item.getAttack());
				config.set("Size", item.getSize());
				config.set("Cooldown", item.getCd());
				config.set("Damage", item.getDam());
				config.set("Cost", item.getCosts());
				config.set("Color.Red", item.getColor().getRed());
				config.set("Color.Green", item.getColor().getGreen());
				config.set("Color.Blue", item.getColor().getBlue());
				config.set("Passive", item.getPassive());
				config.set("Definition", item.getDef());
				
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private void LoadAbilties(){
		for(File file : AbilitieFolder.listFiles()){
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			String name = config.getString("Name");
			String particleType = config.getString("Particle");
			String attackType = config.getString("Attack");
			int size=  config.getInt("Size");
			int cooldown = config.getInt("Cooldown");
			double damage = config.getDouble("Damage");
			int price = config.getInt("Cost");
			Color color = new Color(config.getInt("Color.Red"), config.getInt("Color.Green"), config.getInt("Color.Blue"));
			String passive = config.getString("Passive");
			String def = config.getString("Definition");
			new Ability(name, particleType, attackType, size, cooldown, damage, price, color, passive, def);
		}
	}
	
	@SuppressWarnings("deprecation")
	private void SavePotions(Potions item) {
		File file = new File(PotionFolder, item.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
				FileConfiguration config = YamlConfiguration
						.loadConfiguration(file);
				config.set("Name", item.getName());
				config.set("Duration", item.getDuration());
				config.set("Cost", item.getCosts());
				config.set("Modifier", item.getModifier());
				config.set("Type", item.getType().getId());
				config.set("Definition", item.getDef());
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void SaveActives(Actives item) {
		File file = new File(ActivesFolder, item.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
				FileConfiguration config = YamlConfiguration
						.loadConfiguration(file);

				config.set("Name", item.getName());
				config.set("Cost", item.getCost());
				config.set("Cooldown", item.getCooldown());
				config.set("Duration", item.getDuration());
				List<Integer> t = new ArrayList<Integer>();
				for (PotionEffectType type : item.getPotioneffect()) {
					t.add(type.getId());
				}
				config.set("Modifier", t);
				config.set("Lore", item.getLore());
				config.set("Amplifier", item.getAmplifier());
				if (item.getPrereq() != null) {
					config.set("Prereq", item.getPrereq().getName());
				} else {
					config.set("Prereq", null);
				}
				config.set("Definition", item.getDef());
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	@SuppressWarnings("deprecation")
	private void SaveArea(Area item) {
		File file = new File(AreaFolder, item.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
				FileConfiguration config = YamlConfiguration
						.loadConfiguration(file);
				config.set("Name", item.getName());
				config.set("Aura", item.getAura());
				config.set("Cost", item.getCost());
				config.set("Range", item.getRange());
				config.set("Modifier", item.getModifier());
				config.set("Type", item.getType().getId());
				if (item.getPrereq() != null) {
					config.set("Prereq", item.getPrereq().getName());
				} else {
					config.set("Prereq", null);
				}
				config.set("Definition", item.getDef());
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}

	@SuppressWarnings("deprecation")
	private void SaveArmor(Armor armor) {
		File file = new File(ArmorFolder, armor.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}

			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			config.set("Name", armor.getName());
			config.set("Type", armor.getType().getId());
			config.set("Passive", armor.getPassive());
			config.set("Modifier", armor.getModifier());
			config.set("Cost", armor.getCost());
			if (armor.getPrereq() != null) {
				config.set("Prereq", armor.getPrereq().getName());
			} else {
				config.set("Prereq", null);
			}
			config.set("Definition", armor.getDef());
			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void SaveCritters(Critters item) {
		File file = new File(CrittersFolder, item.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			config.set("Name", item.getName());
			config.set("Type", item.getType().getId());
			config.set("Passive", item.getPassive());
			config.set("Modifier", item.getModifier());
			config.set("Cost", item.getCost());
			if (item.getPrereq() != null) {
				config.set("Prereq", item.getPrereq().getName());
			} else {
				config.set("Prereq", null);
			}
			config.set("Definition", item.getDef());

			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@SuppressWarnings("deprecation")
	private void SaveWeapons(Weapons item) {
		File file = new File(WeaponsFolder, item.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			config.set("Name", item.getName());
			config.set("Type", item.getType().getId());
			config.set("Passive", item.getPassive());
			config.set("Modifier", item.getModifier());
			config.set("Cost", item.getCost());
			if (item.getPrereq() != null) {
				config.set("Prereq", item.getPrereq().getName());
			} else {
				config.set("Prereq", null);
			}
			config.set("Definition", item.getDef());

			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@SuppressWarnings("deprecation")
	private void SaveStackers(Stackers item) {
		File file = new File(StackersFolder, item.getName() + ".yml");
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			config.set("Name", item.getName());
			config.set("Type", item.getType().getId());
			config.set("Passive", item.getPassive());
			config.set("Modifier", item.getModifier());
			config.set("Cost", item.getCost());
			if (item.getPrereq() != null) {
				config.set("Prereq", item.getPrereq().getName());
			} else {
				config.set("Prereq", null);
			}
			config.set("Definition", item.getDef());

			try {
				config.save(file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	private void LoadAllActives() {
		Actives.Reset();
		ArrayList<File> waiting = new ArrayList<File>();
		for (File file : ActivesFolder.listFiles()) {
			if (LoadActive(file)) {
				waiting.add(file);
			}
		}
		for (File file : waiting) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Actives ac = Actives.returnactive(config.getString("Name"));
			ac.setPrereq(Actives.returnactive(config.getString("Prereq")));
		}
	}

	@SuppressWarnings("deprecation")
	private boolean LoadActive(File file) {
		boolean wait = false;
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		String name = config.getString("Name");

		int cost = config.getInt("Cost");
		Actives prereq;
		// filtering the prereq steps
		if (config.getString("Prereq") != null
				&& Actives.returnactive(config.getString("Prereq")) != null) {
			prereq = Actives.returnactive(config.getString("Prereq"));
		} else if (config.getString("Prereq") == null) {
			prereq = null;
		} else {
			prereq = null;
			wait = true;
		}
		String def = config.getString("Definition");
		int amplifier = config.getInt("Amplifier");
		int duration = config.getInt("Duration");
		String lore = config.getString("Lore");
		int cooldown = config.getInt("Cooldown");

		List<PotionEffectType> effect = new ArrayList<PotionEffectType>();
		for (int i : config.getIntegerList("Modifier")) {
			effect.add(PotionEffectType.getById(i));
		}

		new Actives(name, duration, amplifier, cost, cooldown, lore, effect,
				prereq, def);
		return wait;
	}

	private void LoadAllAreas() {
		Area.Reset();
		ArrayList<File> waiting = new ArrayList<File>();
		for (File file : AreaFolder.listFiles()) {
			if (LoadArea(file)) {
				waiting.add(file);
			}
		}
		for (File file : waiting) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Area ac = Area.returnAura(config.getString("Name"));
			ac.setPrereq(Area.returnAura(config.getString("Prereq")));
		}
	}

	private boolean LoadArea(File file) {
		boolean wait = false;
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		String name = config.getString("Name");
		@SuppressWarnings("deprecation")
		Material type = Material.getMaterial(config.getInt("Type"));
		List<String> modifiers = config.getStringList("Modifier");
		String passive = config.getString("Aura");
		int cost = config.getInt("Cost");
		Area prereq;
		// filtering the prereq steps
		if (config.getString("Prereq") != null
				&& Area.returnAura(config.getString("Prereq")) != null) {
			prereq = Area.returnAura(config.getString("Prereq"));
		} else if (config.getString("Prereq") == null) {
			prereq = null;
		} else {
			prereq = null;
			wait = true;
		}
		int range = config.getInt("Range");

		String def = config.getString("Definition");
		new Area(name, type, cost, range, passive, modifiers, prereq, def);
		return wait;
	}

	private void LoadAllCritters() {
		Critters.Reset();
		ArrayList<File> waiting = new ArrayList<File>();
		for (File file : CrittersFolder.listFiles()) {
			if (LoadCritter(file)) {
				waiting.add(file);
			}
		}
		for (File file : waiting) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Critters ac = Critters.returncritter(config.getString("Name"));
			ac.setPrereq(Critters.returncritter(config.getString("Prereq")));
		}
	}

	private boolean LoadCritter(File file) {
		boolean wait = false;
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		String name = config.getString("Name");
		@SuppressWarnings("deprecation")
		Material type = Material.getMaterial(config.getInt("Type"));
		List<String> modifiers = config.getStringList("Modifier");
		String passive = config.getString("Passive");
		int cost = config.getInt("Cost");
		Critters prereq;
		// filtering the prereq steps
		if (config.getString("Prereq") != null
				&& Critters.returncritter(config.getString("Prereq")) != null) {
			prereq = Critters.returncritter(config.getString("Prereq"));
		} else if (config.getString("Prereq") == null) {
			prereq = null;
		} else {
			prereq = null;
			wait = true;
		}
		String def = config.getString("Definition");
		new Critters(name, type, passive, modifiers, cost, prereq, def);
		return wait;
	}

	private void LoadAllPotions() {
		Potions.Reset();
		for(File file : PotionFolder.listFiles()){
			FileConfiguration config = YamlConfiguration.loadConfiguration(file);
			String name = config.getString("Name");
			int duration = config.getInt("Duration");
			int modifier = config.getInt("Modifier");
			int cost = config.getInt("Cost");
			@SuppressWarnings("deprecation")
			PotionEffectType type = PotionEffectType.getById(config.getInt("Type"));
			String def = config.getString("Definition");
			new Potions(name, duration, modifier, cost, type, def);
		}
	}

	private void LoadAllStackers() {
		Stackers.Reset();
		ArrayList<File> waiting = new ArrayList<File>();
		for (File file : StackersFolder.listFiles()) {
			if (LoadStacker(file)) {
				waiting.add(file);
			}
		}
		for (File file : waiting) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Stackers ac = Stackers.returnstacker(config.getString("Name"));
			ac.setPrereq(Stackers.returnstacker(config.getString("Prereq")));
		}
	}

	private boolean LoadStacker(File file) {
		boolean wait = false;
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		String name = config.getString("Name");
		@SuppressWarnings("deprecation")
		Material type = Material.getMaterial(config.getInt("Type"));
		List<String> modifiers = config.getStringList("Modifier");
		String passive = config.getString("Passive");
		int cost = config.getInt("Cost");
		Stackers prereq;
		// filtering the prereq steps
		if (config.getString("Prereq") != null
				&& Stackers.returnstacker(config.getString("Prereq")) != null) {
			prereq = Stackers.returnstacker(config.getString("Prereq"));
		} else if (config.getString("Prereq") == null) {
			prereq = null;
		} else {
			prereq = null;
			wait = true;
		}
		String def = config.getString("Definition");
		new Stackers(name, type, passive, modifiers, cost, prereq, def);
		return wait;
	}

	private void LoadAllWeapons() {
		Weapons.Reset();
		ArrayList<File> waiting = new ArrayList<File>();
		for (File file : WeaponsFolder.listFiles()) {
			if (LoadWeapon(file)) {
				waiting.add(file);
			}
		}
		for (File file : waiting) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Weapons ac = Weapons.returnweapon(config.getString("Name"));
			ac.setPrereq(Weapons.returnweapon(config.getString("Prereq")));
		}
	}

	private boolean LoadWeapon(File file) {
		boolean wait = false;
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		String name = config.getString("Name");
		@SuppressWarnings("deprecation")
		Material type = Material.getMaterial(config.getInt("Type"));
		List<String> modifiers = config.getStringList("Modifier");
		String passive = config.getString("Passive");
		int cost = config.getInt("Cost");
		Weapons prereq;
		// filtering the prereq steps
		if (config.getString("Prereq") != null
				&& Weapons.returnweapon(config.getString("Prereq")) != null) {
			prereq = Weapons.returnweapon(config.getString("Prereq"));
		} else if (config.getString("Prereq") == null) {
			prereq = null;
		} else {
			prereq = null;
			wait = true;
		}
		String def = config.getString("Definition");
		new Weapons(name, type, passive, modifiers, cost, prereq, def);

		return wait;
	}

	private boolean LoadArmor(File file) {
		boolean wait = false;
		FileConfiguration config = YamlConfiguration.loadConfiguration(file);
		String name = config.getString("Name");
		@SuppressWarnings("deprecation")
		Material type = Material.getMaterial(config.getInt("Type"));
		List<String> modifiers = config.getStringList("Modifier");
		String passive = config.getString("Passive");
		int cost = config.getInt("Cost");
		Armor prereq;
		// filtering the prereq steps
		if (config.getString("Prereq") != null
				&& Armor.returnarmor(config.getString("Prereq")) != null) {
			prereq = Armor.returnarmor(config.getString("Prereq"));
		} else if (config.getString("Prereq") == null) {
			prereq = null;
		} else {
			prereq = null;
			wait = true;
		}
		String def = config.getString("Definition");
		new Armor(name, type, passive, modifiers, cost, prereq, def);

		return wait;
	}

	private void LoadAllArmors() {
		Armor.Reset();
		ArrayList<File> waiting = new ArrayList<File>();
		for (File file : ArmorFolder.listFiles()) {
			if (LoadArmor(file)) {
				waiting.add(file);
			}
		}
		// this is to save add the prereqs
		for (File file : waiting) {
			FileConfiguration config = YamlConfiguration
					.loadConfiguration(file);
			Armor armor = Armor.returnarmor(config.getString("Name"));
			armor.setPrereq(Armor.returnarmor(config.getString("Prereq")));
		}
	}

	public void LoadAll() {
		LoadAllArmors();
		LoadAllActives();
		LoadAllAreas();
		LoadAllCritters();
		LoadAllPotions();
		LoadAllStackers();
		LoadAllWeapons();
		LoadAbilties();
	}

}
