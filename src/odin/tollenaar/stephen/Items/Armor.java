package odin.tollenaar.stephen.Items;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;

public class Armor {
	/**
	 * To add another item name Material the passive (null for no passive) new
	 * ArrayList<String>(Arrays.asList('modifyer') cost prereq (null for no
	 * prereq) from bottem to top (see arenaitems.jpeg) also in arraylist
	 */

	private static Set<Armor> armors = new HashSet<Armor>();

	final private String name;
	final private Material type;
	final private String passive;
	final private List<String> modifier;
	final private int cost;
	private Armor prereq;
	final private String def;

	public Armor(String name, Material type, String passive,
			List<String> modifiers, int cost, Armor prereq, String def) {
		this.name = name;
		this.type = type;
		this.passive = passive;
		this.modifier = modifiers;
		this.cost = cost;
		this.prereq = prereq;
		this.def = def;

		boolean save = true;
		for (Armor r : armors) {
			if (r != null) {
				if (r.getName().equals(name)) {
					save = false;
				}
			}
		}
		if (save) {
			armors.add(this);
		}
	}

	public void setPrereq(Armor armor) {
		this.prereq = armor;
	}

	public String getName() {
		return name;
	}

	public Material getType() {
		return type;
	}

	public String getPassive() {
		return passive;
	}

	public List<String> getModifier() {
		return modifier;
	}

	public static Armor returnarmor(String name) {
		for (Armor armor : values()) {
			if (armor.getName().equals(name)) {
				return armor;
			}
		}
		return null;
	}

	public static Armor[] values() {
		Armor[] temp = new Armor[armors.size()];
		for (int i = 0; i < armors.size(); i++) {
			int x = 0;
			for (Armor r : armors) {
				if (x == i) {
					temp[i] = r;
					break;
				} else {
					x++;
				}
			}
		}
		return temp;
	}

	public boolean hasPrereq() {
		if (prereq != null) {
			return true;
		} else {
			return false;
		}
	}

	public int getCost() {
		return cost;
	}

	public Armor getPrereq() {
		return prereq;
	}

	public String getDef() {
		return def;
	}
	
	public static void Reset(){
		armors.clear();
	}
}
