package odin.tollenaar.stephen.Items;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;

public class Weapons {
	
	/**
	 * To add another item
	 * name
	 * Material
	 * the passive (null for no passive)
	 * new ArrayList<String>(Arrays.asList('modifyer')
	 * cost	
	 * prereq (null for no prereq) from bottem to top (see .jpeg) also in arraylist
	 */

	
	private static Set<Weapons> items = new HashSet<Weapons>();
	
	final private String name;
	final private Material type;
	final private String passive;
	final private List<String> modifier;
	final private int cost;
	private Weapons prereq;
	final private String def;
	
	public Weapons(String name, Material type, String passive, List<String> modifiers, int cost, Weapons prereq, String def){
		this.name = name;
		this.type = type;
		this.passive = passive;
		this.modifier = modifiers;
		this.cost = cost;
		this.prereq = prereq;
		this.def = def;
		
		
		boolean save = true;
		for(Weapons in : items){
			if(in.getName().equals(name)){
				save = false;
				break;
			}
		}
		
		if(save){
			items.add(this);
		}
		
	}
	
	public void setPrereq(Weapons prereq){
		this.prereq = prereq;
	}
	
	public static Weapons returnweapon(String name){
		for(Weapons weapon : values()){
			if(weapon.getName().equals(name)){
				return weapon;
			}
		}
		return null;
	}

	public String getName() {
		return name;
	}

	public Material getType() {
		return type;
	}

	public String getPassive() {
		return passive;
	}

	public List<String> getModifier() {
		return modifier;
	}

	public int getCost() {
		return cost;
	}

	public Weapons getPrereq() {
		return prereq;
	}
	
	public boolean hasPrereq(){
		if(prereq != null){
			return true;
		}else{
			return false;
		}
	}
	
	public static Weapons[] values(){
		Weapons[] temp = new Weapons[items.size()];
		for(int i = 0; i < items.size(); i++){
			int x = 0;
			for(Weapons in : items){
				if(x == i){
					temp[i] = in;
					break;
				}else{
					x++;
				}
			}
		}
		return temp;
	}
	public static void Reset(){
		items.clear();
	}

	public String getDef() {
		return def;
	}
}
