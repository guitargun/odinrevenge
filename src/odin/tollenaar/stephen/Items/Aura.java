package odin.tollenaar.stephen.Items;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Material;


public enum Aura {
	
	MOLTENSTICK("Molten Stick", Material.STICK ,700, 0, null, new ArrayList<String>(Arrays.asList("Damage +3")),null),
	FORGEDSTICK("Forged Stick", Material.BLAZE_ROD, 1100, 3, "Aura deals damage to enemies every 3 seconds", new ArrayList<String>(Arrays.asList("Damage +5")), Aura.MOLTENSTICK),
	BLESSEDSTICK("Blessed Stick",Material.IRON_INGOT ,1050, 3, "Aura gives regen to allies every 10 seconds",new ArrayList<String>(Arrays.asList("Damage +4")), Aura.MOLTENSTICK),
	STICKOFHEALING("Stick Of Healing", Material.GOLD_INGOT,1200, 5, "Aura gives regen to allies every 4 seconds",new ArrayList<String>(Arrays.asList("Damage +4")), Aura.BLESSEDSTICK),
	WEAPONIZEDSTICK("Weaponized Stick", Material.BLAZE_ROD ,1350, 4, "Aura deals damage and slow to enemies every 5 seconds",new ArrayList<String>(Arrays.asList("Damage +6")), Aura.FORGEDSTICK);
	
	
	private final String name;
	private final Aura prereq;
	private final String aura;
	private final int cost;
	private final int range;
	private final ArrayList<String> modifiers;
	private final Material type;
	
	
	Aura(String name,  Material type, int cost, int range, String aura, ArrayList<String> modifiers, Aura prereq){
		this.name = name;
		this.cost = cost;
		this.aura = aura;
		this.prereq = prereq;
		this.range = range;
		this.modifiers = modifiers;
		this.type = type;
	}


	public String getName() {
		return name;
	}


	public Aura getPrereq() {
		return prereq;
	}


	public String getAura() {
		return aura;
	}




	public ArrayList<String> getModifiers() {
		return modifiers;
	}


	public int getCost() {
		return cost;
	}
	
	
	public int getRange() {
		return range;
	}


	public Material getType() {
		return type;
	}


	public static Aura returnAura(String name){
			for(Aura aura : values()){
				if(aura.getName().equals(name)){
					return aura;
				}
			}
			return null;
	}
}
