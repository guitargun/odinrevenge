package odin.tollenaar.stephen.Items;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.potion.PotionEffectType;

public class Actives {

	
	
	
	
	
	
	private static Set<Actives> actives = new HashSet<Actives>();
	
	private final String name;
	private final int cost;
	private final int duration;
	private final int amplifier;
	private final List<PotionEffectType> potioneffect;
	private Actives prereq;
	private final String lore;
	private final int cooldown;
	private final String def;
	
	public Actives(String name, int duration, int amplifier, int cost, int cooldown,String lore, List<PotionEffectType> effect, Actives prereq, String def){
		this.name = name;
		this.duration = duration;
		this.amplifier = amplifier;
		this.cost = cost;
		this.potioneffect = effect;
		this.prereq = prereq;
		this.lore = lore;
		this.cooldown = cooldown;
		this.def = def;
		boolean save = true;
		for(Actives a : actives){
			if(a != null){
				if(a.getName().equals(name)){
					save = false;
				}
			}
		}
		if(save){
			actives.add(this);
		}
	}
	
	public void setPrereq(Actives actives){
		this.prereq = actives;
	}
	
	public static Actives returnactive(String name){
		for(Actives ac : values()){
			if(ac.getName().equals(name)){
				return ac;
			}
		}
		return null;
	}


	public String getName() {
		return name;
	}


	public int getCost() {
		return cost;
	}


	public int getDuration() {
		return duration;
	}


	public int getAmplifier() {
		return amplifier;
	}


	public List<PotionEffectType> getPotioneffect() {
		return potioneffect;
	}


	public Actives getPrereq() {
		return prereq;
	}


	public String getLore() {
		return lore;
	}


	public int getCooldown() {
		return cooldown;
	}
	
	public boolean hasPrereq(){
		if(prereq != null){
			return true;
		}else{
			return false;
		}
	}


	public String getDef() {
		return def;
	}
	
	public static Actives[] values(){
		Actives[] temp = new Actives[actives.size()];
		for(int i = 0; i < actives.size();i ++){
			int x = 0;
			for(Actives a : actives){
				if(x == i){
					temp[i] = a;
					break;
				}else{
					x++;
				}
				
			}
		}
		
		return temp;
	}
	
	public static void Reset(){
		actives.clear();
	}

	
}
