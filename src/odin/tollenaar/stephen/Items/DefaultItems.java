package odin.tollenaar.stephen.Items;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;

import odin.tollenaar.stephen.Abilities.Ability;

import org.bukkit.Material;
import org.bukkit.potion.PotionEffectType;

public class DefaultItems {

	protected void DefaultAbilities(){
		new Ability("Piercing Arrow","line", "projectile", 10, 15, 7, 900, Color.ORANGE, "Passive enemies are slowed for 3 seconds", "physical");
		new Ability("Circle Of Death", "area", "explosion", 3, 26, 9, 1100, Color.BLACK, "Passive enemies are weakened for 2 seconds", "magical");
		new Ability("Pushback Wave" ,"line", "wave", 10, 14, 3, 1000, Color.BLUE, "Passive enemies are blinded for 2 seconds", "magical");
		new Ability("Time Arrow" ,"line", "timed", 10, 12, 6, 900, Color.GREEN, "Passive enemies are paralyzed for 2 seconds", "physical");
		new Ability("Pool Of Death", "area", "wave", 3, 13, 6, 1100, Color.GREEN, "Passive enemies are paralyzed and blinded for 1.7 seconds", "magical");
		new Ability("Line Explosion","line", "explosion", 10, 15, 5, 900, Color.ORANGE, "Passive enemies are weakened for 2 seconds", "physical");
		new Ability("Area Time" ,"area", "timed", 3, 15, 6, 1200, Color.CYAN, "Passive enemies are paralyzed for 2 seconds", "physical");
		new Ability("Wrapper" ,"line", "wrapper", 10, 17, 1, 1200, Color.BLACK, "Passive enemies are blinded for 2 second", "magical");
		new Ability("Healing Circle","area", "healexplosion", 3, 17, -4, 1200, Color.RED, "Passive grants allies regen for 2 seconds", "magical");
		new Ability("Healing Arrow","line", "healprojectile", 10, 15, -3, 1200, Color.PINK, "Passive grants resistence for 2 seconds", "physical");
	}
	
	protected void DefaultStackers(){
		new Stackers("Broken Spade", Material.WOOD_SPADE, null, new ArrayList<String>(Arrays.asList("Protect +1", "Health +2", "Damage +1")), 750, null, "magical");
		new Stackers("Buried Spade", Material.STONE_SPADE, "Passive protect +2 and health +1 every kill. Max 5 stacks", new ArrayList<String>(Arrays.asList("Protect +3", "Health +4")), 1250, Stackers.returnstacker("Broken Spade"), "magical");
		new Stackers("Hardened Spade", Material.GOLD_SPADE, "Passive protect +2 every kill. Max 10 stacks", new ArrayList<String>(Arrays.asList("Protect +2")), 1000, Stackers.returnstacker("Broken Spade"), "magical");
		new Stackers("Sharpened Spade", Material.IRON_SPADE, "Passive damage +3 each kill. Max 4 stacks", new ArrayList<String>(Arrays.asList("Protect +2", "Damage +3")), 1250, Stackers.returnstacker("Broken Spade"), "magical");
	}
	
	protected void DefaultPotions(){
		new Potions("Regen potion", 5, 2, 100, PotionEffectType.REGENERATION, "all");
		new Potions("Speed potion", 5, 3, 100, PotionEffectType.SPEED, "all");
		new Potions("Resistence potion", 40, 2, 3000, PotionEffectType.DAMAGE_RESISTANCE, "all");
		new Potions("Strength potion", 40, 2, 3000, PotionEffectType.INCREASE_DAMAGE, "all");
		new Potions("Extra health potion", 15, 2, 1600, PotionEffectType.ABSORPTION, "all");
		
	}
	protected void DefaultCritters(){
		new Critters("Small Sword", Material.STONE_SWORD, null, new ArrayList<String>(Arrays.asList("Damage +2", "Crit 5%")), 850, null, "physical");
		new Critters("Cutlas", Material.IRON_SWORD, null, new ArrayList<String>(Arrays.asList("Damage +4", "Crit 10%")), 1700, Critters.returncritter("Small Sword"), "physical");
		new Critters("Explosive Bow", Material.BOW, null, new ArrayList<String>(Arrays.asList("Damage +4", "Crit 5%")), 2550, Critters.returncritter("Small Sword"), "physical");
		new Critters("Long Sword", Material.DIAMOND_SWORD, "Passive damage +4 with every protect item. Max 3 stacks", new ArrayList<String>(Arrays.asList("Damage +6", "Crit 15%")), 2300, Critters.returncritter("Cutlas"), "physical");
		new Critters("Rage Axe", Material.DIAMOND_AXE, "Passive missed crit +5%. Max 4 stack (reset oncrit)", new ArrayList<String>(Arrays.asList("Damage +5", "Crit 20%")), 2125, Critters.returncritter("Cutlas"), "physical");
		new Critters("Golden Bow", Material.BOW, "Passive lifesteal +4% for every 2 kills. Max 3 stacks", new ArrayList<String>(Arrays.asList("Damage +6", "Crit 15%")), 2200,Critters.returncritter("Explosive Bow"), "physical");
	}
	protected void DefaultAreas(){
		new Area("Molten Stick", Material.STICK ,700, 0, "Aura blinds enemies every 3 seconds", new ArrayList<String>(Arrays.asList("Damage +3")),null, "magical");
		new Area("Forged Stick", Material.BLAZE_ROD, 1100, 3, "Aura slows enemies every 3 seconds", new ArrayList<String>(Arrays.asList("Damage +5")), Area.returnAura("Molten Stick"), "magical");
		new Area("Blessed Stick",Material.IRON_INGOT ,1050, 3, "Aura gives regen to allies every 10 seconds",new ArrayList<String>(Arrays.asList("Damage +4")), Area.returnAura("Molten Stick"),"magical");
		new Area("Stick Of Healing", Material.GOLD_INGOT,1200, 5, "Aura gives regen to allies every 4 seconds",new ArrayList<String>(Arrays.asList("Damage +4")), Area.returnAura("Blessed Stick"),"magical");
		new Area("Weaponized Stick", Material.BLAZE_ROD ,1350, 4, "Aura deals damage and slow to enemies every 5 seconds",new ArrayList<String>(Arrays.asList("Damage +6")), Area.returnAura("Forged Stick"),"magical");
		
		new Area("Broken Scythe", Material.WOOD_HOE, 700, 0, null, new ArrayList<String>(Arrays.asList("Damage +3", "Speed +10%")), null, "physical");
		new Area("Hastened Pickaxe", Material.WOOD_PICKAXE, 700, 0, null, new ArrayList<String>(Arrays.asList("Damage +3", "Speed + 15%")), null, "physical");
		new Area("Pure Scythe", Material.GOLD_HOE, 1100, 2, "Passive Speed +10%", new ArrayList<String>(Arrays.asList("Area Damage +4")), Area.returnAura("Broken Scythe"), "physical");
		new Area("Fatalis", Material.GOLD_PICKAXE, 1150, 3, "Passive lifesteal +3%", new ArrayList<String>(Arrays.asList("Area Damage +3", "Speed +15%")), Area.returnAura("Hastened Pickaxe"), "physical");
		new Area("God Scythe", Material.DIAMOND_HOE, 1300, 3, "Passive Speed +10%", new ArrayList<String>(Arrays.asList("Area Damage +6")), Area.returnAura("Pure Scythe"), "physical");
		new Area("Hastaned Fatalis", Material.DIAMOND_PICKAXE, 1400, 3, "Passive lifeateal +5%", new ArrayList<String>(Arrays.asList("Area Damage +3", "Speed +18%")), Area.returnAura("Fatalis"), "physical");
	}
	protected void DefaultWeapons(){
		new Weapons("Short Blade", Material.WOOD_SWORD, null, new ArrayList<String>(Arrays.asList("Damage +2")), 785, null, "all");
		new Weapons("Dagger", Material.STONE_SWORD, null, new ArrayList<String>(Arrays.asList("Damage +4")), 1570, Weapons.returnweapon("Short Blade"), "all");
		new Weapons("Hooked Blade", Material.STONE_SWORD, "Passive lifesteal 10%", new ArrayList<String>(Arrays.asList("Damage +3")),1178 , Weapons.returnweapon("Short Blade"), "all");
		new Weapons("Penetrator", Material.DIAMOND_SWORD, "Passive true damage +2", new ArrayList<String>(Arrays.asList("Damage +6")), 1300, Weapons.returnweapon("Dagger"), "all");
		new Weapons("Rapier", Material.DIAMOND_SWORD, "Passive damage +3", new ArrayList<String> (Arrays.asList("Speed +30%", "Cooldown 15%")), 1400, Weapons.returnweapon("Dagger"), "all");
		new Weapons("Sharpened Blade", Material.IRON_SWORD, "Passive lifesteal 10%", new ArrayList<String>(Arrays.asList("Damage +9")), 1600, Weapons.returnweapon("Hooked Blade"), "all");
		new Weapons("Cutter", Material.GOLD_SWORD, "Passive lifesteal 30%", new ArrayList<String>(Arrays.asList("Damage +4")), 1570, Weapons.returnweapon("Hooked Blade"), "all");
		new Weapons("Huntsmen Knife", Material.DIAMOND_SWORD, "Passive lifesteal 15%", new ArrayList<String>(Arrays.asList("Speed +80%", "Damage +3")), 1800,  Weapons.returnweapon("Hooked Blade"), "all");
	}
	
	
	protected void DefaultActive() {
		new Actives("Aegis", 5, 100, 300, 60,
				"Invincible for short duration. can't move",
				new ArrayList<PotionEffectType>(Arrays.asList(
						PotionEffectType.DAMAGE_RESISTANCE,
						PotionEffectType.SLOW)), null, "all");
		new Actives("Aegis pendant", 5, 100, 300, 60,
				"Invincible for short duration. can't move",
				new ArrayList<PotionEffectType>(Arrays.asList(
						PotionEffectType.DAMAGE_RESISTANCE,
						PotionEffectType.SLOW)), Actives.returnactive("Aegis"),
				"all");
		new Actives("Greater aegis", 5, 100, 300, 60,
				"Invincible for short duration. can't move",
				new ArrayList<PotionEffectType>(Arrays.asList(
						PotionEffectType.DAMAGE_RESISTANCE,
						PotionEffectType.SLOW)),
				Actives.returnactive("Aegis pendant"), "all");
		new Actives("Moving aegis", 3, 100, 300, 60,
				"Invincible for short duration. can move",
				new ArrayList<PotionEffectType>(Arrays
						.asList(PotionEffectType.DAMAGE_RESISTANCE)),
				Actives.returnactive("Aegis pendant"), "all");

	}

	protected void DefaultArmor() {
		new Armor("Crude Chestplate", Material.LEATHER_CHESTPLATE, null,
				new ArrayList<String>(Arrays.asList("Protect +2")), 400, null,
				"all");
		new Armor("Heavy Chestplate", Material.IRON_CHESTPLATE, null,
				new ArrayList<String>(Arrays.asList("Protect +6")), 1200,
				Armor.returnarmor("Crude Chestplate"), "all");
		new Armor("Resistence Chestplate", Material.DIAMOND_CHESTPLATE,
				"Passive area damage Max 3", new ArrayList<String>(
						Arrays.asList("Protect +7", "Health +2")), 1600,
				Armor.returnarmor("Heavy Chestplate"), "all");
		new Armor("Heavier Chestplate", Material.DIAMOND_CHESTPLATE,
				"Passive crit +15%", new ArrayList<String>(
						Arrays.asList("Protect +9")), 1800,
				Armor.returnarmor("Heavy Chestplate"), "all");
		new Armor(
				"Purifying Chestplate",
				Material.GOLD_CHESTPLATE,
				null,
				new ArrayList<String>(Arrays.asList("Protect +3", "Health +2")),
				800, Armor.returnarmor("Crude Chestplate"), "all");
		new Armor(
				"Chestplate of the Gods",
				Material.GOLD_CHESTPLATE,
				"Passive every kill protect +1. Max 4 stacks",
				new ArrayList<String>(Arrays.asList("Protect +5", "Health +4")),
				1600, Armor.returnarmor("Purifying Chestplate"), "all");
	}
}
