package odin.tollenaar.stephen.Items;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;

public class Critters {
	/**
	 * To add another item name Material the passive (null for no passive) new
	 * ArrayList<String>(Arrays.asList('modifyer') cost prereq (null for no
	 * prereq) from bottem to top (see .jpeg) also in arraylist
	 */

	private static Set<Critters> items = new HashSet<Critters>();

	final private String name;
	final private Material type;
	final private String passive;
	final private List<String> modifiers;
	final private int cost;
	private Critters prereq;
	final private String def;

	public Critters(String name, Material type, String passive,
			List<String> modifiers2, int cost, Critters prereq, String def) {
		this.name = name;
		this.type = type;
		this.passive = passive;
		this.modifiers = modifiers2;
		this.cost = cost;
		this.prereq = prereq;
		this.def = def;

		boolean save = true;
		for (Critters item : items) {
			if (item != null) {
				if (item.getName().equals(name)) {
					save = false;
					break;
				}
			}
		}
		if (save) {
			items.add(this);
		}

	}

	public static Critters returncritter(String name) {
		for (Critters critter : values()) {
			if (critter.getName().equals(name)) {
				return critter;
			}
		}
		return null;
	}

	public void setPrereq(Critters prereq) {
		this.prereq = prereq;
	}

	public String getName() {
		return name;
	}

	public Material getType() {
		return type;
	}

	public String getPassive() {
		return passive;
	}

	public List<String> getModifier() {
		return modifiers;
	}

	public int getCost() {
		return cost;
	}

	public Critters getPrereq() {
		return prereq;
	}

	public static ArrayList<String> returnmax() {
		ArrayList<String> count = new ArrayList<String>();
		for (Critters piece : values()) {
			if (piece.getPrereq() != null) {
				if (piece.getPrereq().getPrereq() != null) {
					count.add(piece.getName());
				}
			}
		}
		return count;
	}

	public static Critters[] values() {
		Critters[] temp = new Critters[items.size()];
		for (int i = 0; i < items.size(); i++) {
			int x = 0;
			for (Critters in : items) {
				if (x == i) {
					temp[i] = in;
					break;
				} else {
					x++;
				}
			}
		}
		return temp;
	}

	public static void Reset() {
		items.clear();
	}

	public boolean hasPrereq() {
		if (prereq != null) {
			return true;
		} else {
			return false;
		}
	}

	public String getDef() {
		return def;
	}
}
