package odin.tollenaar.stephen.Items;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Material;


public class Area {
	
	private static Set<Area> items = new HashSet<Area>();
	
	private final String name;
	private Area prereq;
	private final String aura;
	private final int cost;
	private final int range;
	private final List<String> modifiers;
	private final Material type;
	private final String def;
	
	public Area(String name,  Material type, int cost, int range, String aura, List<String> modifiers2, Area prereq, String def){
		this.name = name;
		this.cost = cost;
		this.aura = aura;
		this.prereq = prereq;
		this.range = range;
		this.modifiers = modifiers2;
		this.type = type;
		this.def = def;
		
		boolean save = true;
		for(Area in : items){
			if(in.getName().equals(name)){
				save = false;
			}
		}
		if(save){
			items.add(this);
		}
	}
	
	public void setPrereq(Area prereq){
		this.prereq = prereq;
	}

	public String getName() {
		return name;
	}


	public Area getPrereq() {
		return prereq;
	}


	public String getAura() {
		return aura;
	}

	public boolean hasPrereq(){
		if(prereq != null){
			return true;
		}else{
			return false;
		}
	}


	public List<String> getModifier() {
		return modifiers;
	}


	public int getCost() {
		return cost;
	}
	
	
	public int getRange() {
		return range;
	}


	public Material getType() {
		return type;
	}


	public static Area returnAura(String name){
			for(Area aura : values()){
				if(aura.getName().equals(name)){
					return aura;
				}
			}
			return null;
	}

	public static Area[] values(){
		Area[] temp = new Area[items.size()];
		for(int i = 0; i < items.size(); i++){
			int x = 0;
			for(Area in : items){
			if(x == i){
				temp[i] = in;
				break;
			}else{
				x++;
			}
			}
		}
		
		return	temp;
	}
	
	public static void Reset(){
		items.clear();
	}
	
	public String getDef() {
		return def;
	}
}
