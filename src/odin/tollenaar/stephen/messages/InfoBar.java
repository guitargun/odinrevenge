package odin.tollenaar.stephen.messages;

import java.util.HashMap;
import java.util.UUID;

import odin.tollenaar.stephen.ArenaStuff.Champions;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;

import org.bukkit.ChatColor;

public class InfoBar {

	private static HashMap<UUID, Message> playermessage = new HashMap<UUID, Message>();
	private static OdinMain plugin;

	@SuppressWarnings("static-access")
	public InfoBar(OdinMain instance) {
		this.plugin = instance;
	}

	/**
	 * 
	 * @param player
	 * @param type
	 *            0 is default message, type 1 is ally slain, type 2 is enemy,
	 *            type 3 you have been slain, type 4 you have slain an enemy
	 *            slain
	 * @param timed
	 *            if it is a timed message
	 */

	public void makeMessage(Champions player, int type, boolean timed,
			int blueteamkills, int redteamkills) {
		String message = message(type, blueteamkills, redteamkills);

		if (playermessage.containsKey(player.getPlayeruuid())) {
			playermessage.get(player.getPlayeruuid()).cancel();
			playermessage.remove(player.getPlayeruuid());
		}

		if (timed) {
			playermessage.put(player.getPlayeruuid(), new Message(player,
					message, 4, plugin, blueteamkills, redteamkills));
		} else {
			playermessage.put(player.getPlayeruuid(), new Message(player,
					message, plugin, blueteamkills, redteamkills));
		}
	}

	public void cancelAll() {
		for (UUID pl : playermessage.keySet()) {
			try {
				playermessage.get(pl).cancel();
			} catch (IllegalStateException ex) {
				continue;
			}
		}
	}

	private static String message(int type, int blueteamkills, int redteamkills) {
		String message;
		switch (type) {
		case 0:
			message = ChatColor.BLUE + "Blue: " + blueteamkills
					+ ChatColor.GRAY + "                        "
					+ ChatColor.RED + " Red: " + redteamkills;
			break;
		case 1:
			message = ChatColor.BLUE + "Blue: " + blueteamkills
					+ ChatColor.GRAY + " An ally  has been slain."
					+ ChatColor.RED + " Red: " + redteamkills;
			break;
		case 2:
			message = ChatColor.BLUE + "Blue: " + blueteamkills
					+ ChatColor.GRAY + " An enemy has been slain."
					+ ChatColor.RED + " Red: " + redteamkills;
			break;
		case 3:
			message = ChatColor.BLUE + "Blue: " + blueteamkills
					+ ChatColor.GRAY + " You   have   been slain."
					+ ChatColor.RED + " Red: " + redteamkills;
			break;
		case 4:
			message = ChatColor.BLUE + "Blue: " + blueteamkills
					+ ChatColor.GRAY + " You have slain an enemy."
					+ ChatColor.RED + " Red: " + redteamkills;
			break;
		default:
			message = ChatColor.BLUE + "Blue: " + blueteamkills
					+ "                        " + ChatColor.RED + " Red: "
					+ redteamkills;
			break;
		}
		return message;
	}

	public static void switchtimed(Champions player, int type, boolean timed, int blue, int red) {
		playermessage.get(player.getPlayeruuid()).cancel();
		playermessage.remove(player.getPlayeruuid());
		String message = message(type, blue, red);
		if(timed){
			playermessage.put(player.getPlayeruuid(), new Message(player, message, 4, plugin, blue, red));
		}else{
			playermessage.put(player.getPlayeruuid(), new Message(player, message, plugin, blue, red));
		}
	}
}
