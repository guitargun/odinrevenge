package odin.tollenaar.stephen.messages;


import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import odin.tollenaar.stephen.ArenaStuff.Champions;
import odin.tollenaar.stephen.OdinsRevenge.OdinMain;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Message extends BukkitRunnable {
	private String message;
	private boolean timed = false; 
	private int length;
	private Champions champ;
	private int blue;
	private int red;
	
	
	private int tick = 0;
	
	public Message(Champions pl, String message, OdinMain plugin, int blue, int red) {
		this.champ = pl;
		this.message = message;
		this.blue = blue;
		this.red = red;
		runTaskTimer(plugin, 10L, 10L);
	}

	public Message(Champions pl, String message, int seconds, OdinMain plugin, int blue, int red) {
		this.champ = pl;
		this.length = seconds;
		this.timed = true;
		this.message = message;
		this.blue = blue;
		this.red = red;
		
		runTaskTimer(plugin, 10L, 10L);
	}

	public Player getPlayer() {
		return Bukkit.getPlayer(champ.getPlayeruuid());
	}

	public String getMessage() {
		return message;
	}

	@Override
	public void run() {
		try{
			if (getPlayer() != null) {
				CraftPlayer p = (CraftPlayer) getPlayer();

				IChatBaseComponent cbc = ChatSerializer.a("{\"text\": \""
						+ message + "\"}");
				PacketPlayOutChat chat = new PacketPlayOutChat(cbc, (byte) 2);
				p.getHandle().playerConnection.sendPacket(chat);
				if (timed) {
					this.length--;
					if (length == 0) {
						InfoBar.switchtimed(champ, 0, false, blue, red);
					}
				}
			}
			if(tick == 2){
			champ.setGold(champ.getGold()+3);
			tick = 0;
			}
			tick++;
		}catch(NullPointerException ex){
			this.cancel();
			return;
		}
	}
}
