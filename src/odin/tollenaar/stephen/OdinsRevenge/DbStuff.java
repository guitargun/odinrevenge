package odin.tollenaar.stephen.OdinsRevenge;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.ScheduledFuture;

import odin.tollenaar.stephen.ArenaStuff.Arena;
import odin.tollenaar.stephen.ArenaStuff.ArenaType;
import odin.tollenaar.stephen.ArenaStuff.Arenacheck;
import odin.tollenaar.stephen.utils.Scheduler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;

import com.gmail.filoghost.holographicdisplays.api.Hologram;

import code.husky.mysql.MySQL;

public class DbStuff {
	private OdinMain plugin;
	private Connection con;
	public MySQL MySQl;
	private String mysqlpass;
	private String mysqluser;
	private String mysqldb;
	private String mysqlpot;
	private String mysqlhost;
	private ScheduledFuture<?> scheduler;
	private ScheduledFuture<?> dbsaver;
	private Arenacheck ar;

	public DbStuff(OdinMain instance) {
		this.plugin = instance;
		this.ar = instance.mov;
	}

	public void setcon(Connection connect) {
		con = connect;
	}

	public void holofill(Hologram holo) {
		holo.appendTextLine(ChatColor.GOLD + "RANKED TOP 10");
		String select = "SELECT * FROM Odin_players ORDER BY `tributes` DESC LIMIT 10;";
		PreparedStatement pst = null;
		Location l = holo.getLocation();
		l.setY(l.getY()+2);
		holo.teleport(l);
		int i = 1;
		try {
			pst = con.prepareStatement(select);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				holo.appendTextLine("|"
						+ ChatColor.GRAY
						+ Integer.toString(i)
						+ ChatColor.WHITE
						+ "| "
						+ ChatColor.RED
						+ plugin.database.retunname(UUID.fromString(rs
								.getString("playeruuid"))) + ChatColor.WHITE
						+ "| " + ChatColor.GREEN + rs.getInt("tributes")
						+ ChatColor.WHITE + "|");
				i++;
			}
		} catch (SQLException e) {
			this.plugin.getLogger().severe(e.getMessage());
			plugin.getLogger().info(
					"There was a error during the savings of the data to the database: "
							+ e.getMessage());
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}

		finally {
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}
	}

	public String retunname(UUID playeruuid) {
		String select = "SELECT `username` FROM Mist_playeruuid WHERE `useruuid` LIKE ?;";
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(select);
			pst.setString(1, playeruuid.toString());
			ResultSet rs = pst.executeQuery();
			if (rs.next()) {
				return rs.getString("username");
			} else {
				return null;
			}
		} catch (SQLException ex) {
			System.out.println(ex.getStackTrace());
		}

		finally {
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}
		return null;
	}
	
	public Connection GetCon(){
		return con;
	}
	public void checkcon(){
		try {
			if(con.isClosed()){
				opencon();
				intvar();
				OpenConnect();
			}
		} catch (SQLException | NullPointerException e) {
			opencon();
			intvar();
			OpenConnect();
		}
	}
	public void OpenConnect(){
		setcon(MySQl.openConnection());
	}

	public void closecon() {
		int timeout = 10;

		ScheduledFuture<?> id = Scheduler.runAsyncTaskRepeat(new Runnable() {
			public void run() {
				PreparedStatement pst;
				try {
					pst = con
							.prepareStatement("SELECT playeruuid FROM `Odin_players` LIMIT 1;");
					pst.execute();
				} catch (SQLException ex) {
					checkcon();
				}
			}
		}, 0L, timeout);
		scheduler = id;
	}

	public void opencon() {
		scheduler.cancel(true);
	}

	public void dbsaveclock() {
		int minutes = Calendar.getInstance().get(Calendar.MINUTE);
		int nearest = ((minutes) / 60) * 60 % 60;
		if (nearest == 0 || minutes > 30) {
			nearest = 60;
		}
		int needed = minutes - nearest;
		if (needed < 0) {
			needed = -needed;
		}
		ScheduledFuture<?> id = Scheduler.runAsyncTask(new Runnable() {
			public void run() {
				saveall();
				plugin.getLogger().info(ChatColor.GREEN + "Back-up complete.");
				dbsaveclock();
			}
		}, needed);
		dbsaver = id;
		dbsaveclock();
	}

	public void intvar() {
		mysqlpass = plugin.getConfig().getString("mysqlpass");
		mysqluser = plugin.getConfig().getString("mysqluser");
		mysqldb = plugin.getConfig().getString("mysqldb");
		mysqlpot = plugin.getConfig().getString("mysqlport");
		mysqlhost = plugin.getConfig().getString("mysqlhost");
		MySQl = new MySQL(plugin, mysqlhost, mysqlpot, mysqldb, mysqluser,
				mysqlpass);
	}

	public void tablecreate() {
		Statement statement;
		try {
			statement = con.createStatement();

			statement.executeUpdate("CREATE TABLE IF NOT EXISTS Odin_arena ("
					+ "id INTEGER PRIMARY KEY,"
					+ "arenatype VARCHAR(45) NOT NULL,"
					+ "redminx INTEGER NOT NULL," + "redminy INTEGER NOT NULL,"
					+ "redminz INTEGER NOT NULL," + "redmaxx INTEGER NOT NULL,"
					+ "redmaxy INTEGER NOT NULL," + "redmaxz INTEGER NOT NULL,"
					+ "blueminx INTEGER NOT NULL,"
					+ "blueminy INTEGER NOT NULL,"
					+ "blueminz INTEGER NOT NULL,"
					+ "bluemaxx INTEGER NOT NULL,"
					+ "bluemaxy INTEGER NOT NULL,"
					+ "bluemaxz INTEGER NOT NULL,"
					+ "world VARCHAR(45) NOT NULL);");

			statement.executeUpdate("CREATE TABLE IF NOT EXISTS Odin_players ("
					+ "playeruuid VARCHAR(45) PRIMARY KEY,"
					+ "totalkills INTEGER NOT NULL,"
					+ "totaldeaths INTEGER NOT NULL,"
					+ "tributes INTEGER NOT NULL);");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void savearenas() {
		String select = "SELECT `arenatype` FROM Odin_arena WHERE `id`=?;";

		String update = "UPDATE Odin_arena SET `arenatype`=?, `redminx`=?, `redminy`=?, `redminz`=?, `redmaxx`=?, `redmaxy`=?, `redmaxz`=?,"
				+ " `blueminx`=?, `blueminy`=?, `blueminz`=?, `bluemaxx`=?, `bluemaxy`=?, `bluemaxz`=?, `world`=? WHERE `id`=?;";

		String insert = "INSERT INTO Odin_arena (`id`, `arenatype`, "
				+ "`redminx`, `redminy`, `redminz`, `redmaxx`, `redmaxy`, `redmaxz`,"
				+ " `blueminx`, `blueminy`, `blueminz`, `bluemaxx`, `bluemaxy`, `bluemaxz`, `world`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";

		for (int id : ar.arenatypes.keySet()) {
			Arena a;
			switch (ar.arenatypes.get(id)) {
			case ARENA:
				a = ar.arenaarenas.get(id);
				break;
			case JOUST:
				a = ar.joustarenas.get(id);
				break;
			case RANKED:
				a = ar.rankedarenas.get(id);
				break;
			case SIEGE:
				a = ar.siegearenas.get(id);
				break;
			case V1:
				a = ar.arena1v1.get(id);
				break;
			default:
				a = null;
				break;
			}
			if(a == null){
				continue;
			}
			PreparedStatement pst = null;
			try {
				pst = con.prepareStatement(select);
				pst.setInt(1, id);
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					pst.close();
					pst = con.prepareStatement(update);
					pst.setString(1, a.getType().getType());

					pst.setInt(2, (int) a.godspawnred.get(0).getX());
					pst.setInt(3, (int) a.godspawnred.get(0).getY());
					pst.setInt(4, (int) a.godspawnred.get(0).getZ());

					pst.setInt(5, (int) a.godspawnred.get(1).getX());
					pst.setInt(6, (int) a.godspawnred.get(1).getY());
					pst.setInt(7, (int) a.godspawnred.get(1).getZ());

					pst.setInt(8, (int) a.godspawnblue.get(0).getX());
					pst.setInt(9, (int) a.godspawnblue.get(0).getY());
					pst.setInt(10, (int) a.godspawnblue.get(0).getZ());

					pst.setInt(11, (int) a.godspawnblue.get(1).getX());
					pst.setInt(12, (int) a.godspawnblue.get(1).getY());
					pst.setInt(13, (int) a.godspawnblue.get(1).getZ());
					pst.setString(14, a.godspawnblue.get(0).getWorld()
							.getName());

					pst.setInt(15, id);

					pst.execute();
				} else {
					pst.close();
					pst = con.prepareStatement(insert);
					pst.setInt(1, id);
					pst.setString(2, a.getType().getType());

					pst.setInt(3, (int) a.godspawnred.get(0).getX());
					pst.setInt(4, (int) a.godspawnred.get(0).getY());
					pst.setInt(5, (int) a.godspawnred.get(0).getZ());

					pst.setInt(6, (int) a.godspawnred.get(1).getX());
					pst.setInt(7, (int) a.godspawnred.get(1).getY());
					pst.setInt(8, (int) a.godspawnred.get(1).getZ());

					pst.setInt(9, (int) a.godspawnblue.get(0).getX());
					pst.setInt(10, (int) a.godspawnblue.get(0).getY());
					pst.setInt(11, (int) a.godspawnblue.get(0).getZ());

					pst.setInt(12, (int) a.godspawnblue.get(1).getX());
					pst.setInt(13, (int) a.godspawnblue.get(1).getY());
					pst.setInt(14, (int) a.godspawnblue.get(1).getZ());
					pst.setString(15, a.godspawnblue.get(0).getWorld()
							.getName());

					pst.execute();
				}

			} catch (SQLException e) {
				this.plugin.getLogger().severe(e.getMessage());
				plugin.getLogger().info(
						"There was a error during the savings of the data to the database: "
								+ e.getMessage());
				try {
					if (pst != null) {
						pst.close();
					}
				} catch (SQLException ex) {
					System.out.println(ex.getStackTrace());
				}
			}

			finally {
				try {
					if (pst != null) {
						pst.close();
					}
				} catch (SQLException ex) {
					System.out.println(ex.getStackTrace());
				}
			}
		}
	}

	
	public void saveplayer(Playerstats p){
		String select = "SELECT * FROM Odin_players WHERE `playeruuid`=?;";

		String update = "UPDATE Odin_players SET `totalkills`=?, `totaldeaths`=?, `tributes`=? WHERE `playeruuid`=?;";

		String insert = "INSERT INTO Odin_players (`playeruuid`, `totalkills`, `totaldeaths`, `tributes`) VALUES (?,?,?,?);";
		
		UUID player = p.getPlayeruuid();
		
		PreparedStatement pst = null;
		try{
			pst = con.prepareStatement(select);
			pst.setString(1, player.toString());
			ResultSet rs = pst.executeQuery();
			if(rs.next()){
				pst.close();
				pst = con.prepareStatement(update);
				pst.setInt(1, p.getKills());
				pst.setInt(2, p.getDeaths());
				pst.setInt(3, p.getTributes());
				pst.setString(4, player.toString());
				pst.execute();
			}else{
				pst.close();
				
				pst = con.prepareStatement(insert);
				pst.setString(1, player.toString());
				pst.setInt(2, p.getKills());
				pst.setInt(3, p.getDeaths());
				pst.setInt(4, p.getTributes());
				pst.execute();
			}
		}catch (SQLException e) {
			this.plugin.getLogger().severe(e.getMessage());
			plugin.getLogger().info(
					"There was a error during the savings of the data to the database: "
							+ e.getMessage());
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}

		finally {
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}
	}
	
	public void saveplayers() {
		String select = "SELECT * FROM Odin_players WHERE `playeruuid`=?;";

		String update = "UPDATE Odin_players SET `totalkills`=?, `totaldeaths`=?, `tributes`=? WHERE `playeruuid`=?;";

		String insert = "INSERT INTO Odin_players (`playeruuid`, `totalkills`, `totaldeaths`, `tributes`) VALUES (?,?,?,?);";
		for (UUID player : Playerstats.fromto.keySet()) {
			PreparedStatement pst = null;
			try {
				Playerstats ps = Playerstats.fromto.get(player);

				pst = con.prepareStatement(select);
				pst.setString(1, player.toString());
				ResultSet rs = pst.executeQuery();
				if (rs.next()) {
					pst.close();
					pst = con.prepareStatement(update);
					pst.setInt(1, ps.getKills());
					pst.setInt(2, ps.getDeaths());
					pst.setInt(3, ps.getTributes());
					pst.setString(4, player.toString());
					pst.execute();
				} else {
					pst.close();
					pst = con.prepareStatement(insert);
					pst.setString(1, player.toString());
					pst.setInt(2, ps.getKills());
					pst.setInt(3, ps.getDeaths());
					pst.setInt(4, ps.getTributes());
					pst.execute();
				}

			} catch (SQLException e) {
				this.plugin.getLogger().severe(e.getMessage());
				plugin.getLogger().info(
						"There was a error during the savings of the data to the database: "
								+ e.getMessage());
				try {
					if (pst != null) {
						pst.close();
					}
				} catch (SQLException ex) {
					System.out.println(ex.getStackTrace());
				}
			}

			finally {
				try {
					if (pst != null) {
						pst.close();
					}
				} catch (SQLException ex) {
					System.out.println(ex.getStackTrace());
				}
			}
		}
	}

	public void saveall() {
		checkcon();
		savearenas();
		saveplayers();
	}

	public void loadarena() {
		String load = "SELECT * FROM Odin_arena;";

		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(load);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				Location redmin = new Location(Bukkit.getWorld(rs
						.getString("world")), rs.getInt("redminx"),
						rs.getInt("redminy"), rs.getInt("redminz"));
				Location redmax = new Location(Bukkit.getWorld(rs
						.getString("world")), rs.getInt("redmaxx"),
						rs.getInt("redmaxy"), rs.getInt("redmaxz"));
				ArrayList<Location> locs = new ArrayList<Location>();
				locs.add(redmin);
				locs.add(redmax);

				Location bluemin = new Location(Bukkit.getWorld(rs
						.getString("world")), rs.getInt("blueminx"),
						rs.getInt("blueminy"), rs.getInt("blueminz"));
				Location bluemax = new Location(Bukkit.getWorld(rs
						.getString("world")), rs.getInt("bluemaxx"),
						rs.getInt("bluemaxy"), rs.getInt("bluemaxz"));
				ArrayList<Location> locs2 = new ArrayList<Location>();
				locs2.add(bluemin);
				locs2.add(bluemax);

				ArenaType type = ArenaType.returnarena(rs
						.getString("arenatype"));

				int id = rs.getInt("id");

				Arena a = new Arena(plugin, locs, locs2, type, id);

				ar.reloadmap(a);

			}

		} catch (SQLException e) {
			this.plugin.getLogger().severe(e.getMessage());
			plugin.getLogger().info(
					"There was a error during the savings of the data to the database: "
							+ e.getMessage());
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}

		finally {
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}
	}

	public void loadplayer() {
		String load = "SELECT * FROM Odin_players;";
		PreparedStatement pst = null;
		try {
			pst = con.prepareStatement(load);
			ResultSet rs = pst.executeQuery();
			while (rs.next()) {
				UUID playeruuid = UUID.fromString(rs.getString("playeruuid"));
				int kills = rs.getInt("totalkills");
				int deaths = rs.getInt("totaldeaths");
				int tributes = rs.getInt("tributes");
				Playerstats p = new Playerstats(playeruuid, plugin);
				p.setKills(kills);
				p.setDeaths(deaths);
				p.setTributes(tributes);

			}

		} catch (SQLException e) {
			this.plugin.getLogger().severe(e.getMessage());
			plugin.getLogger().info(
					"There was a error during the savings of the data to the database: "
							+ e.getMessage());
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}

		finally {
			try {
				if (pst != null) {
					pst.close();
				}
			} catch (SQLException ex) {
				System.out.println(ex.getStackTrace());
			}
		}
	}

	public void loadall() {
		loadarena();
		loadplayer();
	}
}