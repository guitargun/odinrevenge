package odin.tollenaar.stephen.OdinsRevenge;

import java.sql.Connection;

import odin.tollenaar.stephen.Abilities.AbilityParticle;
import odin.tollenaar.stephen.ArenaStuff.Arena;
import odin.tollenaar.stephen.ArenaStuff.ArenaInformations;
import odin.tollenaar.stephen.ArenaStuff.Arenacheck;
import odin.tollenaar.stephen.Attacks.AuraBehavior;
import odin.tollenaar.stephen.Attacks.Damages;
import odin.tollenaar.stephen.Items.ItemWriters;
import odin.tollenaar.stephen.utils.FileWriter;
import odin.tollenaar.stephen.utils.Scheduler;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import code.husky.mysql.MySQL;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;

public class OdinMain extends JavaPlugin {

	Connection con = null;
	OdinMain plugin;

	public FileWriter fw;
	public Arenacheck mov;
	public ArenaInformations info;
	public AuraBehavior aura;
	public AbilityParticle ap;
	public DbStuff database;
	public ItemWriters items;
	
	
	public void onEnable() {
		FileConfiguration config = this.getConfig();
		config.options().copyDefaults(true);
		saveConfig();
		plugin = this;
		fw = new FileWriter(this);
		mov = new Arenacheck(this);
		info = new ArenaInformations(this);
		
		ap = new AbilityParticle(this);
		aura = new AuraBehavior(this);
		
		database = new DbStuff(this);
		database.intvar();
		items = new ItemWriters(this);
		MySQL MySQl = database.MySQl;
		int poging = 0;
		while (con == null) {
			con = MySQl.openConnection();
			if (con == null) {
				poging++;
				getLogger()
						.info("Database connection lost. Reconection will be started");

			}
			if (poging == 2) {
				getLogger()
						.info("No Connection to Database. Plugin is deactivating. Reload server for database Connection");
				Bukkit.getPluginManager().disablePlugin(plugin);
				break;
			}

		}
		if (con != null) {
			database.setcon(con);
			getLogger().info("Databse connection has succeed");
			database.tablecreate();
			database.closecon();
			database.loadall();
		}
		fw.loadholo();
		
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(mov, this);
		pm.registerEvents(info, this);
		
		
		pm.registerEvents(new Damages(this), this);
		
		
		getCommand("arena").setExecutor(new Commandexe(this));
			

	}

	public void onDisable() {
		if (mov.arenas.size() > 0) {
			for (Arena ar : mov.arenas.values()) {
				ar.endgame("Plugin shutdown", "None");
			}
		}
		database.saveall();
		
		
		info.storeit();
		int i = 0;
		for(Hologram holo : HologramsAPI.getHolograms(plugin)){
			fw.storeholo(holo, i);
			i++;
			holo.getVisibilityManager().setVisibleByDefault(false);
			holo.delete();
		}
		Scheduler.cancelAllTasks();
		items.SaveItems();
	}

}
