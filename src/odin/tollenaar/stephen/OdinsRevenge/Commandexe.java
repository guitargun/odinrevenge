package odin.tollenaar.stephen.OdinsRevenge;

import java.util.UUID;

import odin.tollenaar.stephen.ArenaStuff.Arena;
import odin.tollenaar.stephen.ArenaStuff.ArenaType;
import odin.tollenaar.stephen.ArenaStuff.Arenacheck;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public class Commandexe implements CommandExecutor {
	private Arenacheck mov;
	private OdinMain plugin;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String commandLabel, String[] args) {
		if (sender instanceof Player) {
			Player se = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("arena")) {
				PermissionUser user = PermissionsEx.getUser(se);
				if (args.length >= 1) {
					if (user.has("Odin.Arena")) {
						if (args[0].equalsIgnoreCase("create")) {
							mov.arenamaker((Player) sender);
							return true;
						} else if (args[0].equalsIgnoreCase("edit")) {
							mov.allarenas((Player) sender);
							return true;
						}
					}
					if (args[0].equalsIgnoreCase("join")) {
						if (args.length != 2) {
							se.sendMessage(ChatColor.AQUA
									+ "[Arena] "
									+ ChatColor.RED
									+ "Not enough arguments. use /arena join <ranked/1v1/joust/arena/siege>");
						} else {
							if (ArenaType.returnarena(args[1]) != null) {
								mov.joinarena(se,
										ArenaType.returnarena(args[1]));
							} else {
								se.sendMessage(ChatColor.AQUA
										+ "[Arena] "
										+ ChatColor.RED
										+ "Wrong argument. use /arena join <ranked/1v1/joust/arena/siege>");
							}
						}

						return true;
					} else if (args[0].equalsIgnoreCase("stop") && user.has("Odin.Arena")) {
						for (Arena ar : mov.arenas.values()) {
							if (ar.players.containsKey(((Player) sender)
									.getUniqueId())) {
								for (UUID pu : ar.players.keySet()) {
									Player player = Bukkit.getPlayer(pu);
									player.sendMessage(ChatColor.AQUA
											+ "[Arena] "
											+ ChatColor.GOLD
											+ "Arena is being terminated stand by.");

								}
								ar.endgame("Arena shutdown", "None");
								break;
							}
						}
						return true;
					} else if (args[0].equalsIgnoreCase("leave")) {
						mov.leavinque(se);
						return true;
					}
				} else {
					sender.sendMessage("please use as /arena <create/edit>");
					return true;
				}
			}else if(cmd.getName().equalsIgnoreCase("test")){
				if(args.length == 1){
					if(args[0].equalsIgnoreCase("rank")){
						Playerstats.fromto.clear();
						plugin.database.loadplayer();
						plugin.info.updateholos();
						return true;
					}
				}
			}
		} else {
			sender.sendMessage("You need to be a player for this.");
			return true;
		}

		return false;
	}

	public Commandexe(OdinMain instance) {
		this.mov = instance.mov;
		this.plugin = instance;
	}
}
