package odin.tollenaar.stephen.OdinsRevenge;

import java.util.HashMap;
import java.util.UUID;

public class Playerstats {
	public static HashMap<UUID, Playerstats> fromto = new HashMap<UUID, Playerstats>();
	
	private UUID playeruuid;
	private int kills;
	private int deaths;
	private int tributes;
	private OdinMain plugin;
	
	public Playerstats(UUID player, OdinMain instance){
		this.playeruuid = player;
		kills = 0;
		deaths = 0;
		tributes = 0;
		this.plugin = instance;
		fromto.put(player, this);
	}


	public static HashMap<UUID, Playerstats> getFromto() {
		return fromto;
	}


	public static void setFromto(HashMap<UUID, Playerstats> fromto) {
		Playerstats.fromto = fromto;
	}


	public int getKills() {
		return kills;
	}


	public void setKills(int kills) {
		this.kills = kills;
	}


	public int getDeaths() {
		return deaths;
	}


	public void setDeaths(int deaths) {
		this.deaths = deaths;
	}


	public int getTributes() {
		return tributes;
	}


	public void setTributes(int tributes) {
		this.tributes = tributes;
		plugin.database.saveplayer(this);
		
	}


	public UUID getPlayeruuid() {
		return playeruuid;
	}
	
	
	
	
}
